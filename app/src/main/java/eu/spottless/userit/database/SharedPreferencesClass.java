package eu.spottless.userit.database;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesClass {
    SharedPreferences prefs;
    Context context;

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
    }

    public static void insertData(Context context, String key, String value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String retriveData(Context context, String key) {
        return getPrefs(context).getString(key, "no_data_found");
    }

    public static void deleteData(Context context, String key) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.remove(key);
        editor.apply();
    }

    public static void insertRangeStatus(Context context, String key, boolean value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static boolean retriveRangeStatus(Context context, String key) {
        return getPrefs(context).getBoolean(key, false);
    }

    public static void insertToken(Context context, String key, String value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String retriveToken(Context context, String key) {
        return getPrefs(context).getString(key, "token");
    }

    public static void deleteToken(Context context, String key) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.remove(key);
        editor.apply();
    }

    public static void insertRole(Context context, String key, String value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String retriveRole(Context context, String key) {
        return getPrefs(context).getString(key, "role");
    }

    public static void insertTaskId(Context context, String key, int value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static int retriveTaskId(Context context, String key) {
        return getPrefs(context).getInt(key, 0);
    }

    public static void insertEmpId(Context context, String key, int value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static int retriveEmpId(Context context, String key) {
        return getPrefs(context).getInt(key, 0);
    }

    public static void insertFragId(Context context, String key, String value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String retriveFragId(Context context, String key) {
        return getPrefs(context).getString(key, "fragId");
    }

    public static void insertCurrentLat(Context context, String key, float value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public static float retriveCurrentLat(Context context, String key) {
        return getPrefs(context).getFloat(key, 0);
    }

    public static void insertCurrentLong(Context context, String key, float value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public static float retriveCurrentLong(Context context, String key) {
        return getPrefs(context).getFloat(key, 0);
    }

    public static void insertClientLat(Context context, String key, float value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public static float retriveClientLat(Context context, String key) {
        return getPrefs(context).getFloat(key, 0);
    }

    public static void insertClientLong(Context context, String key, float value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public static float retriveClientLong(Context context, String key) {
        return getPrefs(context).getFloat(key, 0);
    }
}
