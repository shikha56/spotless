package eu.spottless.userit.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import eu.spottless.userit.R;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static String DB_NAME = "spottless.db";
    private static String DB_PATH = "";
    private static final int DB_VERSION = 1;

    private SQLiteDatabase mDataBase;
    private final Context mContext;
    private boolean mNeedUpdate = false;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        if (android.os.Build.VERSION.SDK_INT >= 17)
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        else
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        this.mContext = context;

        copyDataBase();

        this.getReadableDatabase();
    }

    public void updateDataBase() throws IOException {
        if (mNeedUpdate) {
            File dbFile = new File(DB_PATH + DB_NAME);
            if (dbFile.exists())
                dbFile.delete();

            copyDataBase();

            mNeedUpdate = false;
        }
    }

    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private void copyDataBase() {
        if (!checkDataBase()) {
            this.getReadableDatabase();
            this.close();
            try {
                copyDBFile();
            } catch (IOException mIOException) {
                throw new Error("ErrorCopyingDataBase");
            }
        }
    }

    private void copyDBFile() throws IOException {
        //InputStream mInput = mContext.getAssets().open(DB_NAME);
        InputStream mInput = mContext.getResources().openRawResource(R.raw.spottless);
        OutputStream mOutput = new FileOutputStream(DB_PATH + DB_NAME);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0)
            mOutput.write(mBuffer, 0, mLength);
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    public boolean openDataBase() throws SQLException {
        mDataBase = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        return mDataBase != null;
    }

    @Override
    public synchronized void close() {
        if (mDataBase != null)
            mDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion)
            mNeedUpdate = true;
    }


    public void insertTaskRow(String task_id,String name,String company_id,String client_id,String check_sheet_id,
                          String frequency,String start_date,String end_date,String status,String is_close,
                          String close_date,String created_at,String updated_at,String is_complete,String is_accept) {
        mDataBase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("task_id", task_id);
        values.put("name", name);
        values.put("company_id", company_id);
        values.put("client_id", client_id);
        values.put("check_sheet_id", check_sheet_id);
        values.put("frequency", frequency);
        values.put("start_date", start_date);
        values.put("end_date", end_date);
        values.put("status", status);
        values.put("is_close", is_close);
        values.put("close_date", close_date);
        values.put("created_at", created_at);
        values.put("updated_at", updated_at);
        values.put("is_complete", is_complete);
        values.put("is_accept", is_accept);

        mDataBase.insert("TaskList", null, values);
        mDataBase.close();
    }

    public void insertClientRow(String client_id,String name,String username,String email,
                                String phone,String company_id,String email_verified_at,String mobile_verified_at,
                                String password,String avatar,String status,String device_id,String range,
                                String device_token,String device_type,String remember_token,String force_change_password,
                                String created_at,String updated_at,String cvr,String avatar_url) {
        mDataBase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//        values.put("task_id", task_id);
        values.put("client_id", client_id);
        values.put("name", name);
        values.put("username", username);
        values.put("email", email);
        values.put("phone", phone);
        values.put("company_id", company_id);
        values.put("email_verified_at", email_verified_at);
        values.put("mobile_verified_at", mobile_verified_at);
        values.put("password", password);
        values.put("avatar", avatar);
        values.put("status", status);
        values.put("device_id", device_id);
        values.put("range", range);
        values.put("device_token", device_token);
        values.put("device_type", device_type);
        values.put("remember_token", remember_token);
        values.put("force_change_password", force_change_password);
        values.put("created_at", created_at);
        values.put("updated_at", updated_at);
        values.put("cvr", cvr);
        values.put("avatar_url", avatar_url);

        mDataBase.insert("Client", null, values);
        mDataBase.close();
    }


}
