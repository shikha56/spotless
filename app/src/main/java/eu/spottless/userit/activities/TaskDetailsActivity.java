package eu.spottless.userit.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import eu.spottless.userit.R;
import eu.spottless.userit.TinyDB;
import eu.spottless.userit.adapter.SecurityQuesAdapter;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.fragments.DetailFragment;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;
import eu.spottless.userit.responseModel.Security.QuesData;
import eu.spottless.userit.responseModel.Security.SecurityQues;
import eu.spottless.userit.responseModel.loginResponse.LoginResponse;
import eu.spottless.userit.responseModel.loginResponse.Task;
import eu.spottless.userit.responseModel.tasksResponse.ChecksheetData;
import eu.spottless.userit.responseModel.tasksResponse.HeadingData;
import eu.spottless.userit.service.GeofenceRegistrationService;
import eu.spottless.userit.utils.LocaleHelper;
import eu.spottless.userit.utils.NetworkUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Config;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class TaskDetailsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private static int[] categoryId;
    private final List<String> isHeadDone = new ArrayList<>();
    // Fragment List
    private final List<Fragment> mFragmentList = new ArrayList<>();
    // Title List
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private ChecksheetData checksheetData;
    private ChecksheetData offlinechecksheetData;
    String companyName;
    private static float clientLat, clientLong, currentLat, currentLong;
    ViewPagerAdapter adapter;
    private static final int REQUEST_LOCATION_PERMISSION_CODE = 101;

    private GoogleApiClient googleApiClient;
    private boolean isMonitoring = false;
    private MarkerOptions markerOptions;
    private Marker currentLocationMarker;
    private PendingIntent pendingIntent;
    public static final String GEOFENCE_ID_STAN_UNI = "STAN_UNI";
    public static final float GEOFENCE_RADIUS_IN_METERS = 1;
    float[] dist = new float[1];
    float range;
    boolean rangeStatus = false;
    String isCompleted, isClose, is_accept, task_name, only_view;
    static int tabPosition, checksheetId, task_id;
    static String token;
    private static ProgressDialog progressDialog;
    String ishead, role, clientId;
    private final int[] imageResId = {
            R.drawable.red_circle_shape,
            R.drawable.green_circle_shape,
            R.drawable.circle_shape
    };
    Intent intent;
    static TinyDB tinydb;
    private static boolean firstConnect = true;
    private TextView toolbar_title;
    private ImageButton btn_addtask, btn_security;
    EditText edit_comment, edit_tool, edit_file;
    // Camera activity request codes
    private static final int CAMERA_CAPTURE_AUDIO_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

    public static final int MEDIA_TYPE_AUDIO = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private Uri fileUri; // file url to store image/video

    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    public static final int request_code = 1000;

    private MediaRecorder recorder = null;
    private int currentFormat = 0;
    private int output_formats[] = {MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP};
    private String file_exts[] = {AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP};
    private String audioFilePath;
    private Button start, stop, play;
    ListView list_ques;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);

//        intent = getIntent();

        init();
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaskDetailsActivity.this.onBackPressed();
            }
        });

        tinydb = new TinyDB(this);

        LinearLayout linear_finishStatus = findViewById(R.id.linear_finishStatus);
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.pager);
        toolbar_title = findViewById(R.id.toolbar_title);
        btn_addtask = findViewById(R.id.btn_addtask);
        btn_security = findViewById(R.id.btn_security);

        setResult(RESULT_OK);

        role = SharedPreferencesClass.retriveRole(this, "role");

        companyName = getIntent().getStringExtra("companyName");
//        isCompleted = intent.getIntExtra("isCompleted",0);
//        isClose = intent.getIntExtra("isClose",0);
        isCompleted = getIntent().getStringExtra("isCompleted");
        isClose = getIntent().getStringExtra("isClose");
        is_accept = getIntent().getStringExtra("is_accept");
        task_name = getIntent().getStringExtra("task_name");
//        only_view = getIntent().getStringExtra("only_view");
        clientLat = Float.parseFloat(getIntent().getStringExtra("clientLat"));
        clientLong = Float.parseFloat(getIntent().getStringExtra("clientLong"));
        clientId = getIntent().getStringExtra("clientId");

        toolbar_title.setText(companyName);

        if (getIntent().getStringExtra("range") != null) {
            range = Float.parseFloat(getIntent().getStringExtra("range"));
            Log.d("range---------", String.valueOf(range));
        }

        initTitle();

        Log.d("isCompleted---------", String.valueOf(isCompleted));
        Log.d("clientId---------", String.valueOf(clientId));


        if (isCompleted.equals("1")) {

            if (isClose.equals("1")) {
                linear_finishStatus.setVisibility(View.GONE);
            } else {
                if(task_name.equals("declined") || task_name.equals("only_view")){
                    linear_finishStatus.setVisibility(View.GONE);
                }
                else {
                    linear_finishStatus.setVisibility(View.VISIBLE);
                    linear_finishStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TaskDetailsActivity.this);
                            alertDialogBuilder.setTitle(getText(R.string.finish_task));
                            alertDialogBuilder
                                    .setCancelable(false)
                                    .setNegativeButton(getText(R.string.no), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    })
                                    .setPositiveButton(getText(R.string.yes),
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    if (!NetworkUtils.getConnectivityStatusString(TaskDetailsActivity.this).equals(getText(R.string.not_internet))) {
                                                        finishTaskApiCall(task_id);
                                                    } else {
                                                        tinydb.putInt("taskId", task_id); //saves data in TinyDB
                                                        Toast.makeText(TaskDetailsActivity.this, getText(R.string.finish_task_success), Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                    });
                }
            }
        } else {
            linear_finishStatus.setVisibility(View.GONE);
        }

        btn_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                // custom dialog
                final Dialog dialog = new Dialog(TaskDetailsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.new_task_box);
                dialog.setCancelable(false);
                // set the custom dialog components - text, image and button
                TextView text = dialog.findViewById(R.id.label_addnewtask);
                text.setText(getText(R.string.btn_addtask));

                ImageView img_close = dialog.findViewById(R.id.img_close);
                edit_comment = dialog.findViewById(R.id.edit_comment);
                edit_tool = dialog.findViewById(R.id.edit_tool);
                edit_file = dialog.findViewById(R.id.edit_file);
                final Button btn_browse = dialog.findViewById(R.id.btn_browse);
                btn_browse.setText(getText(R.string.btn_addtask));

                TextView btn_addtask = dialog.findViewById(R.id.btn_addtask);

                btn_browse.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkRunTimePermission();
//                        selectImage();
                    }
                });

                btn_addtask.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String comment = edit_comment.getText().toString();
                        String tool = edit_tool.getText().toString();
                        String file = edit_file.getText().toString();

                        if (validate(comment, tool, file)) {
                            try {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                            } catch (Exception e) {
                                // TODO: handle exception
                            }
                            if (!NetworkUtils.getConnectivityStatusString(TaskDetailsActivity.this).equals(getString(R.string.not_internet))) {
                                dialog.dismiss();
                                addNewTaskApiCall(task_id, comment, tool, file);
                            } else {
                                Toast.makeText(TaskDetailsActivity.this, getString(R.string.internet_not), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
                img_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view12) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        btn_security.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!NetworkUtils.getConnectivityStatusString(TaskDetailsActivity.this).equals(getText(R.string.not_internet))) {

                    final Dialog dialog = new Dialog(TaskDetailsActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setContentView(R.layout.security_ques);
                    dialog.setCancelable(false);
                    // set the custom dialog components - text, image and button
                    TextView text = dialog.findViewById(R.id.label_ques);
                    text.setText(getText(R.string.security_ques));
                    ImageView img_close = dialog.findViewById(R.id.img_close);
                    list_ques = dialog.findViewById(R.id.list_ques);

                    securityQuesApiCall(clientId);

                    img_close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view12) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(TaskDetailsActivity.this, getString(R.string.internet_not), Toast.LENGTH_SHORT).show();
                }
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION_CODE);
        }

        startGeofencing();

//        Log.e("rangeStatus----", String.valueOf(rangeStatus));

        // edited , you can use multiple titles and one Fragment
        for (int i = 0; i < mFragmentTitleList.size(); i++) {
            Fragment fragment = new DetailFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("checksheetData", checksheetData);
            bundle.putInt("isCompleted", Integer.parseInt(isCompleted));
            bundle.putInt("isClose", Integer.parseInt(isClose));
            bundle.putInt("is_accept", Integer.parseInt(is_accept));
            bundle.putString("task_name", task_name);
//            bundle.putString("only_view", only_view);
            bundle.putInt("tabid", 0);
            fragment.setArguments(bundle);
            mFragmentList.add(fragment);

            ishead = isHeadDone.get(i);
            if (ishead.equals("1")) {
//                Log.d("isHeadDone---", ishead);
                tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.green));
            } else if (ishead.equals("0")) {
//                Log.d("else isHeadDone---",ishead);
                tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.darkyellow));
            }

        }

        setupViewPager(viewPager);
        viewPager.setOffscreenPageLimit(mFragmentList.size());

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
//                Log.e("isHeisHeadDonead---", String.valueOf(isHeadDone.size()));
                for (int k = 0; k < mFragmentTitleList.size(); k++) {
                    String headdone = isHeadDone.get(k);
//                        Log.e("isHead---", headdone);
                    if (headdone.equals("1")) {
//                            Log.d("if isHead---", headdone);
                        tabLayout.getTabAt(k).setIcon(getResources().getDrawable(R.drawable.green_circle_shape));
                    } else if (headdone.equals("0")) {
//                            Log.d("else isHead---",headdone);
                        tabLayout.getTabAt(k).setIcon(getResources().getDrawable(R.drawable.circle_shape));
                    }
                }
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
                Log.d("tabPosition---------", String.valueOf(categoryId[tabPosition]));
                viewPager.setCurrentItem(tab.getPosition());
                DetailFragment.newInstance(categoryId[tab.getPosition()], checksheetData);
                if (checksheetData.getHeadingData().get(tab.getPosition()).getIs_head_done().equals("1")) {
                    tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.green));
                } else {
                    tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.darkyellow));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (checksheetData.getHeadingData().get(tab.getPosition()).getIs_head_done().equals("1")) {
                    tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.green));
                } else {
                    tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.darkyellow));
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
//                Log.d("Reselected Position---", String.valueOf(categoryId[tabPosition]));
            }
        });

    }

    private void initTitle() {
        token = "Bearer " + SharedPreferencesClass.retriveToken(this, "token");
        task_id = SharedPreferencesClass.retriveTaskId(this, "task_id");
        Log.e("initTitle token-----", token + "\ntask_id---------" + task_id);

        checksheetData = (ChecksheetData) getIntent().getSerializableExtra("checksheetData");
//        Log.d("checksheetData---", new Gson().toJson(checksheetData));

        checksheetId = checksheetData.getId();
        Log.d("checksheetId---", String.valueOf(checksheetId));
        List<HeadingData> headingData = checksheetData.getHeadingData();
        categoryId = new int[headingData.size()];

        for (int i = 0; i < headingData.size(); i++) {

            String tabsName = headingData.get(i).getName();
            mFragmentTitleList.add(tabsName);
            categoryId[i] = headingData.get(i).getId();
            isHeadDone.add(headingData.get(i).getIs_head_done());
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), mFragmentList, mFragmentTitleList, tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
    }

    /**
     * ViewPagerAdapter setting
     */
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList;
        private final List<String> mFragmentTitleList;
        final int count;

        @SuppressWarnings("deprecation")
        ViewPagerAdapter(FragmentManager fm, List<Fragment> fragments, List<String> titleLists, int count) {
            super(fm);
            this.mFragmentList = fragments;
            this.mFragmentTitleList = titleLists;
            this.count = count;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList == null ? 0 : mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private boolean validate(String comment, String tool, String file) {
        if (comment == null || comment.trim().length() == 0) {
            Toast.makeText(TaskDetailsActivity.this, getText(R.string.label_comment), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (file == null || file.trim().length() == 0) {
            Toast.makeText(TaskDetailsActivity.this, getText(R.string.label_audio), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11111);
            selectImage();
        } else {
            selectImage();
        }
    }

    // Select image from camera and gallery
    private void selectImage() {
        try {
            final CharSequence[] options = {getText(R.string.upload_vedio), getText(R.string.upload_audio), getText(R.string.cancel)};
            AlertDialog.Builder builder = new AlertDialog.Builder(TaskDetailsActivity.this);
            builder.setTitle(getText(R.string.select_option));
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals(getText(R.string.upload_vedio))) {
                        dialog.dismiss();
                        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
                        // set video quality
                        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
//                        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file
                        // name
                        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);

                    } else if (options[item].equals(getText(R.string.upload_audio))) {
                        dialog.dismiss();
//                        Intent audioRecord = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
//                        startActivityForResult(audioRecord, CAMERA_CAPTURE_AUDIO_REQUEST_CODE);
                        final Dialog dialog1 = new Dialog(TaskDetailsActivity.this);
                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog1.setContentView(R.layout.audio_record);
                        dialog1.setCancelable(false);
                        // set the custom dialog components - text, image and button
                        start = dialog1.findViewById(R.id.start);
                        stop = dialog1.findViewById(R.id.stop);
//                        play =  dialog1.findViewById(R.id.play);

                        stop.setEnabled(false);
//                        play.setEnabled(false);

                        if (checkPermissionFromDevice()) {
                            recorder = new MediaRecorder();
                            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                            recorder.setOutputFormat(output_formats[currentFormat]);

                            File root = Environment.getExternalStorageDirectory();
                            File file = new File(root.getAbsolutePath() + "/Spottless/Audios");
                            if (!file.exists()) {
                                file.mkdirs();
                            }

                            audioFilePath = root.getAbsolutePath() + "/Spottless/Audios/" +
                                    String.valueOf(System.currentTimeMillis() + ".mp3");
                            Log.d("filename", audioFilePath);

                            recorder.setOutputFile(audioFilePath);
                            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                            recorder.setOnErrorListener(errorListener);
                            recorder.setOnInfoListener(infoListener);
                        } else {
                            requestPermissionFromDevice();
                        }

                        start.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startRecording();
                            }
                        });
                        stop.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stopRecording();
                                dialog1.dismiss();
                            }
                        });
//                        play.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                               play();
//                            }
//                        });
                        dialog1.show();

                    } else if (options[item].equals(getText(R.string.cancel))) {
                        dialog.dismiss();
                    }

                }
            });
            builder.show();
        } catch (Exception e) {
            Toast.makeText(TaskDetailsActivity.this, getText(R.string.camera_permision), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_AUDIO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                fileUri = data.getData();
                String filepath = getRealPathFromURI(fileUri);
                edit_file.setText(filepath);

            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled Image capture
                Toast.makeText(TaskDetailsActivity.this,
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();

            } else {
                // failed to capture image
                Toast.makeText(TaskDetailsActivity.this,
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }

        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                fileUri = data.getData();
                String filepath = getRealPathFromURI(fileUri);
                edit_file.setText(filepath);

            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled recording
                Toast.makeText(TaskDetailsActivity.this,
                        "User cancelled video recording", Toast.LENGTH_SHORT)
                        .show();

            } else {
                // failed to record video
                Toast.makeText(TaskDetailsActivity.this,
                        "Sorry! Failed to record video", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }


    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void startRecording() {

        try {
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        start.setEnabled(false);
        stop.setEnabled(true);

        Toast.makeText(this, "Recording Started", Toast.LENGTH_SHORT).show();
    }

    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            Log.e("Error: ", what + ", " + extra);
        }
    };

    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            Log.e("Warning: ", what + ", " + extra);
        }
    };

    private boolean checkPermissionFromDevice() {
        int storage_permission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int recorder_permssion = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        return storage_permission == PackageManager.PERMISSION_GRANTED && recorder_permssion == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissionFromDevice() {
        ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO},
                request_code);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case request_code: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(TaskDetailsActivity.this, "permission granted...", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(TaskDetailsActivity.this, "permission denied...", Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }

    private void stopRecording() {
        if (null != recorder) {
            recorder.stop();
            recorder.reset();
            recorder.release();
            recorder = null;

            stop.setEnabled(false);
//            play.setEnabled(true);
            Toast.makeText(this, "Audio recorded successfully", Toast.LENGTH_SHORT).show();
            edit_file.setText(audioFilePath);
        }
    }

//    public void play(){
//        MediaPlayer m = new MediaPlayer();
//        try {
//            String file = getFilename();
//            m.setDataSource(file);
//            m.prepare();
//            m.start();
//            Toast.makeText(this, "Playing Audio", Toast.LENGTH_SHORT).show();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    public void finishTaskApiCall(int taskid) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
//        Log.d("finishTask token-----", token +"\ntask_id---------"+ task_id);
        Call<LoginResponse> call = apiService.finishTask(token, String.valueOf(taskid));
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    Log.d("finishTaskApiCall---", new Gson().toJson(loginResponse));

                    if (loginResponse.getStatus().equals("success")) {
                        String message = loginResponse.getMessage();
                        tinydb.remove("taskId");
                        Toast.makeText(TaskDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                        Intent intent1 = new Intent(TaskDetailsActivity.this, Dashboard.class);
                        intent1.putExtra("upcoming", 1);
//                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent1);
                        finish();
                    } else {
                        String message = loginResponse.getMessage();
                        Toast.makeText(TaskDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                }
//                else {
//                    Toast.makeText(TaskDetailsActivity.this, "Error! Please try again!", Toast.LENGTH_SHORT).show();
//                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                progressDialog.dismiss();
                Log.d("updateTasks_error---", t.toString());
//                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void securityQuesApiCall(String clientId) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        Log.d("finishTask token-----", token + "\nclientId---------" + clientId);

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
        Call<SecurityQues> call = apiService.securityQuesList(token, clientId);
        call.enqueue(new Callback<SecurityQues>() {
            @Override
            public void onResponse(Call<SecurityQues> call, Response<SecurityQues> response) {
                //hiding progress dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    SecurityQues securityQues = response.body();
                    Log.d("securityQues---", new Gson().toJson(securityQues));

                    if (securityQues.getStatus().equals("success")) {
                        String message = securityQues.getMessage();
                        List<QuesData> quesDataList = securityQues.getQuesData();

                        SecurityQuesAdapter securityQuesAdapter = new SecurityQuesAdapter(getApplicationContext(), quesDataList);
                        list_ques.setAdapter(securityQuesAdapter);
                    } else {
                        String message = securityQues.getMessage();
                        Toast.makeText(TaskDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                progressDialog.dismiss();
                Log.d("securityQues error---", t.toString());
//                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addNewTaskApiCall(int taskid, String item_title, String tool, String proof) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
//        Log.d("finishTask token-----", token +"\ntask_id---------"+ task_id);
//        Map<String, String> params = new HashMap<>();
//        params.put("item_title", item_title);
//        params.put("tool", tool);
//        params.put("proof", proof);

        Log.e("addNewTaskApiCall-----", proof + " \ntask_id---" + taskid + " \ntoken---" + token);
        File pFile = new File(proof);
        RequestBody lRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), pFile);
        MultipartBody.Part lFile = MultipartBody.Part.createFormData("proof", pFile.getName(), lRequestBody);
        MultipartBody.Part id = MultipartBody.Part.createFormData("tool", tool);
        MultipartBody.Part type = MultipartBody.Part.createFormData("item_title", item_title);

        Call<LoginResponse> call = apiService.addNewTask(token, String.valueOf(taskid), type, id, lFile);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    Log.d("loginResponse-----", new Gson().toJson(loginResponse));
                    if (loginResponse.getStatus().equals("success")) {
                        String message = loginResponse.getMessage();
                        Toast.makeText(TaskDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
//                        EventBus.getDefault().postSticky("add task");

//                        Intent intent1 = new Intent(TaskDetailsActivity.this, Dashboard.class);
//                        intent1.putExtra("upcoming", 1);
//                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent1);
//                        finish();
                    } else {
                        String message = loginResponse.getMessage();
                        Toast.makeText(TaskDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(TaskDetailsActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                progressDialog.dismiss();
                Log.d("updateTasks_error---", t.toString());
//                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Map for storing information
     */
    public static final HashMap<String, LatLng> AREA_LANDMARKS = new HashMap<String, LatLng>();

    static {
        AREA_LANDMARKS.put(GEOFENCE_ID_STAN_UNI, new LatLng(clientLat, clientLong));
    }

    @SuppressWarnings("deprecation")
    private void startLocationMonitor() {
        Log.d("start---", "start location monitor");
        LocationRequest locationRequest = LocationRequest.create()
                .setInterval(2000)
                .setFastestInterval(1000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {

                    if (currentLocationMarker != null) {
                        currentLocationMarker.remove();
                    }

                    currentLat = (float) location.getLatitude();
                    currentLong = (float) location.getLongitude();
                    SharedPreferencesClass.insertClientLat(TaskDetailsActivity.this, "clientlat", clientLat);
                    SharedPreferencesClass.insertClientLong(TaskDetailsActivity.this, "clientlong", clientLong);
                    SharedPreferencesClass.insertCurrentLat(TaskDetailsActivity.this, "currentLat", currentLat);
                    SharedPreferencesClass.insertCurrentLong(TaskDetailsActivity.this, "currentLong", currentLong);
//                    DetailFragment.getDistance(clientLat,clientLong,location.getLatitude(),location.getLongitude());

                    Location.distanceBetween(clientLat, clientLong, location.getLatitude(), location.getLongitude(), dist);
                    Log.e("dist---", String.valueOf(dist[0]));
                    DecimalFormat df = new DecimalFormat("#.0");
//                    float distance = Float.parseFloat(df.format(dist[0]));
                    String distance = df.format(dist[0]);
                    float doubleValue = 0;
                    if (distance != null) {
                        try {
                            doubleValue = Float.parseFloat(distance.replace(',', '.'));
                        } catch (NumberFormatException e) {
                            //Error
                        }
                    }
                    Log.d("doubleValue---", String.valueOf(doubleValue));

                    if (range != 0.0) {
                        if (doubleValue <= range) {
                            Log.d("message---", "inside range");
                            rangeStatus = true;
                            if (role.equals("employee")) {
                                btn_security.setVisibility(View.GONE);
                            } else if (role.equals("supervisor")) {
                                btn_security.setVisibility(View.GONE);
                            }
                            SharedPreferencesClass.insertRangeStatus(TaskDetailsActivity.this, "rangeStatus", rangeStatus);
                            googleApiClient.disconnect();
                        } else {
                            rangeStatus = false;
                            btn_security.setVisibility(View.GONE);
                            SharedPreferencesClass.insertRangeStatus(TaskDetailsActivity.this, "rangeStatus", rangeStatus);
                        }
                    }
                }
            });
        } catch (SecurityException e) {
            Log.d("message---", e.getMessage());
        }

    }

    @SuppressWarnings("deprecation")
    private void startGeofencing() {
        Log.d("----", "Start geofencing monitoring call");
        pendingIntent = getGeofencePendingIntent();
        GeofencingRequest geofencingRequest = new GeofencingRequest.Builder()
                .setInitialTrigger(Geofence.GEOFENCE_TRANSITION_ENTER)
                .addGeofence(getGeofence())
                .build();

        if (!googleApiClient.isConnected()) {
            Log.d("api---", "Google API client not connected");
        } else {
            try {
                LocationServices.GeofencingApi.addGeofences(googleApiClient, geofencingRequest, pendingIntent).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        if (status.isSuccess()) {
                            Log.d("Successfully---", "Successfully Geofencing Connected");
                        } else {
                            Log.d("failed", "Failed to add Geofencing " + status.getStatus());
                        }
                    }
                });
            } catch (SecurityException e) {
                Log.d("error---", e.getMessage());
            }
        }
        isMonitoring = true;
        invalidateOptionsMenu();
    }

    @NonNull
    private Geofence getGeofence() {
        LatLng latLng = AREA_LANDMARKS.get(GEOFENCE_ID_STAN_UNI);
        return new Geofence.Builder()
                .setRequestId(GEOFENCE_ID_STAN_UNI)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setCircularRegion(latLng.latitude, latLng.longitude, GEOFENCE_RADIUS_IN_METERS)
                .setNotificationResponsiveness(1000)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
    }

    private PendingIntent getGeofencePendingIntent() {
        if (pendingIntent != null) {
            return pendingIntent;
        }
        Intent intent = new Intent(this, GeofenceRegistrationService.class);
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @SuppressWarnings("deprecation")
    private void stopGeoFencing() {
        pendingIntent = getGeofencePendingIntent();
        LocationServices.GeofencingApi.removeGeofences(googleApiClient, pendingIntent)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        if (status.isSuccess())
                            Log.d("stop", "Stop geofencing");
                        else
                            Log.d("not stop", "Not stop geofencing");
                    }
                });
        isMonitoring = false;
        invalidateOptionsMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();
        int response = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(TaskDetailsActivity.this);
        if (response != ConnectionResult.SUCCESS) {
            Log.d("not Available", "Google Play Service Not Available");
            GoogleApiAvailability.getInstance().getErrorDialog(TaskDetailsActivity.this, response, 1).show();
        } else {
            Log.d("Available", "Google play service available");
        }

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        googleApiClient.reconnect();
//        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.reconnect();
//        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
//        stopGeoFencing();
        googleApiClient.disconnect();
//        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LatLng latLng = AREA_LANDMARKS.get(GEOFENCE_ID_STAN_UNI);
        googleMap.addMarker(new MarkerOptions().position(latLng).title("Stanford University"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17f));

        googleMap.setMyLocationEnabled(true);

        Circle circle = googleMap.addCircle(new CircleOptions()
                .center(new LatLng(latLng.latitude, latLng.longitude))
                .radius(GEOFENCE_RADIUS_IN_METERS)
                .strokeColor(Color.RED)
                .strokeWidth(4f));

//        Log.e("circle---", String.valueOf(circle));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("ClientConnected---", "Google Api Client Connected");
        if (googleApiClient.isConnected()) {
            Log.e("--", "Google_Api_Client: It was connected on (onConnected) function, working as it should.");
        } else {
            Log.e("---", "Google_Api_Client: It was NOT connected on (onConnected) function, It is definetly bugged.");
        }
        isMonitoring = true;
        startGeofencing();
        startLocationMonitor();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("ConnectionSuspended--", "Google Connection Suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        isMonitoring = false;
        Log.e("Connection Failed:", connectionResult.getErrorMessage());
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(networkChangeReceiver);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String netstatus = NetworkUtils.getConnectivityStatusString(context);
            if (tinydb.getInt("taskId") != 0) {
                if (!NetworkUtils.getConnectivityStatusString(context).equals(context.getText(R.string.not_internet))) {
                    if (firstConnect) {
                        int taskid = tinydb.getInt("taskId"); // retrieves the object from storage
                        finishTaskApiCall(taskid);
                        progressDialog.dismiss();

                        firstConnect = false;
                    }
                } else {
                    firstConnect = true;
                }
            }
            if (role.equals("employee")) {
                if (isCompleted.equals("1")) {

                    if (isClose.equals("1")) {
                            btn_addtask.setVisibility(View.GONE);
                    } else {
                        if (!NetworkUtils.getConnectivityStatusString(context).equals(getText(R.string.not_internet))) {
                            if(task_name.equals("only_view")){
                                btn_addtask.setVisibility(View.GONE);
                            }
                            else {
                                btn_addtask.setVisibility(View.GONE);
                            }
                        } else {
                            btn_addtask.setVisibility(View.GONE);
                        }
                    }
                } else {
                    if (!NetworkUtils.getConnectivityStatusString(context).equals(getText(R.string.not_internet))) {
                        if(task_name.equals("only_view")){
                            btn_addtask.setVisibility(View.GONE);
                        }
                        else {
                            btn_addtask.setVisibility(View.GONE);
                        }
                    } else {
                        btn_addtask.setVisibility(View.GONE);
                    }
                }
            } else if (role.equals("supervisor")) {
                btn_addtask.setVisibility(View.GONE);
            }
        }
    };

    @SuppressWarnings("deprecation")
    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }

}
