package eu.spottless.userit.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import eu.spottless.userit.R;
import eu.spottless.userit.TinyDB;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;
import eu.spottless.userit.responseModel.ProfileResponse;
import eu.spottless.userit.utils.LocaleHelper;
import eu.spottless.userit.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {
    final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private ProgressDialog progressDialog;
    private String authtoken, name, email, phone, departmant, designation,
            companyname, imageurl, departmant_id, designation_id, message;
    private String encodeString = null;
    private EditText edit_username, edit_email, edit_phone,
            edit_department, edit_designation, edit_companyname;
    private ImageView img_profile, img_camera;
    private boolean aBoolean = false;
    private Menu mOptionsMenu;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String mobilePattern = "[0-9]{8}";
    TinyDB tinydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        init();
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileActivity.this.onBackPressed();
            }
        });

        aBoolean = false;

        edit_username = findViewById(R.id.edit_username);
        edit_email = findViewById(R.id.edit_email);
        edit_phone = findViewById(R.id.edit_phone);
        edit_department = findViewById(R.id.edit_department);
        edit_designation = findViewById(R.id.edit_designation);
        edit_companyname = findViewById(R.id.edit_companyname);

        LinearLayout linearDepartment = findViewById(R.id.linearDepartment);
        LinearLayout linearDesignation = findViewById(R.id.linearDesignation);

        tinydb = new TinyDB(this);

        String role = SharedPreferencesClass.retriveRole(this, "role");

        if (role.equals("employee")) {
            linearDepartment.setVisibility(View.VISIBLE);
            linearDesignation.setVisibility(View.VISIBLE);
        } else if (role.equals("supervisor")) {
            linearDepartment.setVisibility(View.GONE);
            linearDesignation.setVisibility(View.GONE);
        }

        edit_username.setEnabled(false);
        edit_username.setFocusable(false);

        edit_email.setEnabled(false);
        edit_email.setFocusable(false);

        edit_phone.setEnabled(false);
        edit_phone.setFocusable(false);

        edit_department.setEnabled(false);
        edit_department.setFocusable(false);

        edit_designation.setEnabled(false);
        edit_designation.setFocusable(false);

        edit_companyname.setEnabled(false);
        edit_companyname.setFocusable(false);

        img_profile = findViewById(R.id.img_profile);
        img_camera = findViewById(R.id.img_camera);

        img_profile.setVisibility(View.VISIBLE);
        aBoolean = false;

        authtoken = "bearer " + SharedPreferencesClass.retriveToken(this, "token");
        Log.d("token---", authtoken);

        if (!NetworkUtils.getConnectivityStatusString(this).equals(getText(R.string.not_internet))) {
            profileApiCall(authtoken);
        } else {
            getProfile();
        }
//        profileApiCall(authtoken);
    }

    private void profileApiCall(String token) {
        progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiService apiService =
                ServiceGenerator.createService(ApiService.class, token);
        Call<ProfileResponse> call = apiService.viewProfile(token);
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    ProfileResponse profileResponse = response.body();
                    Log.d("profileResponse---", new Gson().toJson(profileResponse));

                    ProfileResponse.ProfileData profile_data = profileResponse.getProfile_data();
                    Log.d("profile_data---", new Gson().toJson(profile_data));

                    tinydb.putObject("profile", profile_data); //saves the object

                    name = profile_data.getName();
                    email = profile_data.getEmail();
                    phone = profile_data.getPhone();
                    departmant_id = profile_data.getDepartment();
                    designation_id = profile_data.getDesignation();
                    departmant = profile_data.getDepartment_name();
                    designation = profile_data.getDesignation_name();
                    companyname = profile_data.getCompany_name();
                    imageurl = profile_data.getAvatar_url();

                    edit_username.setText(name);
                    edit_email.setText(email);
                    edit_phone.setText(phone);
                    edit_department.setText(departmant);
                    edit_designation.setText(designation);
                    edit_companyname.setText(companyname);
                    Picasso.with(getApplicationContext())
                            .load(imageurl)
                            .error(R.drawable.person)
                            .resize(600,600)
                            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .networkPolicy(NetworkPolicy.NO_CACHE,NetworkPolicy.NO_STORE)
                            .into(img_profile);
                } else {
                    Toast.makeText(ProfileActivity.this, getText(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("profile error---", t.toString());
                progressDialog.dismiss();
                Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getProfile(){
        if(tinydb.getObject("profile", ProfileResponse.ProfileData.class) != null){
            ProfileResponse.ProfileData tasksData = tinydb.getObject("profile", ProfileResponse.ProfileData.class); // retrieves the object from storage

            name = tasksData.getName();
            email = tasksData.getEmail();
            phone = tasksData.getPhone();
            departmant_id = tasksData.getDepartment();
            designation_id = tasksData.getDesignation();
            departmant = tasksData.getDepartment_name();
            designation = tasksData.getDesignation_name();
            companyname = tasksData.getCompany_name();
            imageurl = tasksData.getAvatar_url();

            edit_username.setText(name);
            edit_email.setText(email);
            edit_phone.setText(phone);
            edit_department.setText(departmant);
            edit_designation.setText(designation);
            edit_companyname.setText(companyname);
            Picasso.with(getApplicationContext())
                    .load(imageurl)
                    .error(R.drawable.person)
                    .resize(600,600)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .networkPolicy(NetworkPolicy.NO_CACHE,NetworkPolicy.NO_STORE)
                    .into(img_profile);

        }
        else {
            Toast.makeText(ProfileActivity.this, getText(R.string.data_not_found), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        mOptionsMenu = menu;
        menu.getItem(0).setVisible(true);
//        menu.getItem(1).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_edit) {
            toggled();
        }
        if (id == R.id.action_update) {
            toggled();
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggled() {
        aBoolean = !aBoolean;
        Log.d("toggled---", String.valueOf(this.aBoolean));

        if (aBoolean) {
            img_camera.setVisibility(View.VISIBLE);
            mOptionsMenu.getItem(1).setVisible(true);
            mOptionsMenu.getItem(0).setVisible(false);

            edit_username.setText(name);
            edit_email.setText(email);
            edit_phone.setText(phone);
            edit_department.setText(departmant);
            edit_designation.setText(designation);
            edit_companyname.setText(companyname);
            Picasso.with(getApplicationContext())
                    .load(imageurl)
                    .error(R.drawable.person)
                    .resize(600,600)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .networkPolicy(NetworkPolicy.NO_CACHE,NetworkPolicy.NO_STORE)
                    .into(img_profile);

            edit_username.setEnabled(true);
            edit_username.setFocusable(true);
            edit_username.setFocusableInTouchMode(true);

            edit_email.setEnabled(true);
            edit_email.setFocusable(true);
            edit_email.setFocusableInTouchMode(true);

            edit_phone.setEnabled(true);
            edit_phone.setFocusable(true);
            edit_phone.setFocusableInTouchMode(true);

            img_camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProfileActivity.this.checkRunTimePermission();
                }
            });
        } else {
//            img_camera.setVisibility(View.INVISIBLE);
//            mOptionsMenu.getItem(0).setVisible(true);
//            mOptionsMenu.getItem(1).setVisible(false);
//
//            edit_username.setEnabled(false);
//            edit_username.setFocusable(false);
//
//            edit_email.setEnabled(false);
//            edit_email.setFocusable(false);
//
//            edit_phone.setEnabled(false);
//            edit_phone.setFocusable(false);

            if (edit_username.getText().toString() == null || edit_username.getText().toString().trim().length() == 0) {
                aBoolean = true;
                Toast.makeText(ProfileActivity.this, getText(R.string.label_username), Toast.LENGTH_SHORT).show();
            }
            else if (edit_phone.getText().toString().length() <= 7 && edit_phone.getText().toString().length() != 0) {
                aBoolean = true;
                Toast.makeText(ProfileActivity.this, getText(R.string.valid_number), Toast.LENGTH_SHORT).show();
            }
            else if(!edit_email.getText().toString().trim().matches(emailPattern) && edit_email.getText().toString().trim().length() != 0){
                aBoolean = true;
                Toast.makeText(getApplicationContext(),getText(R.string.invalid_email), Toast.LENGTH_SHORT).show();
            }
            else{

                    img_camera.setVisibility(View.INVISIBLE);
                    mOptionsMenu.getItem(0).setVisible(true);
                    mOptionsMenu.getItem(1).setVisible(false);

                    edit_username.setEnabled(false);
                    edit_username.setFocusable(false);

                    edit_email.setEnabled(false);
                    edit_email.setFocusable(false);

                    edit_phone.setEnabled(false);
                    edit_phone.setFocusable(false);

                    updateProfileApiCall(authtoken);

            }
        }
    }

    private void updateProfileApiCall(String token) {
                progressDialog = new ProgressDialog(ProfileActivity.this);
                progressDialog.setMessage(getString(R.string.loading));
                progressDialog.setCancelable(false);
                progressDialog.show();

                ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
                //Here the json data is add to a hash map with key data
                Map<String, String> params = new HashMap<>();
                params.put("name", edit_username.getText().toString());
                params.put("email", edit_email.getText().toString());
                params.put("phone", edit_phone.getText().toString());
//        params.put("department", departmant_id);
//        params.put("designation", designation_id);

                if(encodeString != null){
                    params.put("avatar",encodeString);
                }

//        Log.d("params data---", new Gson().toJson(params));

                Call<ProfileResponse> call = apiService.updateProfile(token, params);
                call.enqueue(new Callback<ProfileResponse>() {
                    @Override
                    public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                        //hiding progress dialog
                        progressDialog.dismiss();
                        Log.d("response---",  new Gson().toJson(response));
                        if (response.isSuccessful()) {
                            ProfileResponse profileResponse = response.body();
                            Log.d("updateProfileApiCall---", new Gson().toJson(profileResponse));
                            message = profileResponse.getMessage();
//                    profileApiCall(authtoken);
                            ProfileResponse.ProfileData profile_data = profileResponse.getProfile_data();

                            name = profile_data.getName();
                            email = profile_data.getEmail();
                            phone = profile_data.getPhone();
                            departmant_id = profile_data.getDepartment();
                            designation_id = profile_data.getDesignation();
                            departmant = profile_data.getDepartment_name();
                            designation = profile_data.getDesignation_name();
                            companyname = profile_data.getCompany_name();
                            imageurl = profile_data.getAvatar_url();

                            edit_username.setText(name);
                            edit_email.setText(email);
                            edit_phone.setText(phone);
                            edit_department.setText(departmant);
                            edit_designation.setText(designation);
                            edit_companyname.setText(companyname);

                            Picasso.with(getApplicationContext())
                                    .load(imageurl)
                                    .error(R.drawable.person)
                                    .resize(600,600)
                                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                                    .networkPolicy(NetworkPolicy.NO_CACHE,NetworkPolicy.NO_STORE)
                                    .into(img_profile);

                            Toast toast = Toast.makeText(ProfileActivity.this,message, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        } else {
                            Toast toast = Toast.makeText(ProfileActivity.this,getText(R.string.email_error), Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Log.d("updateProfile_error---", new Gson().toJson(t));
//                Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11111);
            selectImage();
        } else {
            selectImage();
        }
    }

    // Select image from camera and gallery
    private void selectImage() {
        try {
            final CharSequence[] options = {getText(R.string.take_photo), getText(R.string.choose_gallery), getText(R.string.cancel)};
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getText(R.string.select_option));
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals(getText(R.string.take_photo))) {
                        dialog.dismiss();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);
                    } else if (options[item].equals(getText(R.string.choose_gallery))) {
                        dialog.dismiss();
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                    } else if (options[item].equals(getText(R.string.cancel))) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            Toast.makeText(this, getText(R.string.camera_permision), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        File destination;
        byte[] bt;
        Bitmap bitmap;
        if (requestCode == PICK_IMAGE_CAMERA) {
            if(data == null){

            }
            else {
                try {
                    bitmap = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                    bt = bytes.toByteArray();
                    encodeString = Base64.encodeToString(bt, Base64.DEFAULT);

//                    Log.e("Activity", "Pick from Camera::>>> \n " + encodeString);

//                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
//                destination = new File(Environment.getExternalStorageDirectory() + "/" +
//                        getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
//                FileOutputStream fo;
//                try {
//                    destination.createNewFile();
//                    fo = new FileOutputStream(destination);
//                    fo.write(bytes.toByteArray());
//                    fo.close();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

                    img_profile.setImageBitmap(bitmap);

                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Toast.makeText(ProfileActivity.this, getText(R.string.image_size_error), Toast.LENGTH_SHORT).show();
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            if(data == null){

            }
            else {
                Uri selectedImage = data.getData();
                try {
//                    bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));
//                    Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
//                    encodeString = ConvertBitmapToString(resizedBitmap);

                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    bt = bytes.toByteArray();
                    encodeString = Base64.encodeToString(bt, Base64.DEFAULT);
//                    Log.e("Activity", "Pick from Gallery::>>> \n " + encodeString);
                    img_profile.setImageBitmap(bitmap);

                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Toast.makeText(ProfileActivity.this, getText(R.string.image_size_error), Toast.LENGTH_SHORT).show();
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //method to convert the selected image to base64 encoded string

    public static String ConvertBitmapToString(Bitmap bitmap){
        String encodedImage = "";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        try {
            encodedImage= URLEncoder.encode(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return encodedImage;
    }

    @Override
    public void onResume() {
        super.onResume();
//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction(CONNECTIVITY_ACTION);
//        registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
//       unregisterReceiver(networkChangeReceiver);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
//    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String netstatus = NetworkUtils.getConnectivityStatusString(context);
//            if (!NetworkUtils.getConnectivityStatusString(context).equals("Not connected to Internet")) {
//                profileApiCall(authtoken);
//            } else {
//                getProfile();
//            }
//        }
//    };
}
