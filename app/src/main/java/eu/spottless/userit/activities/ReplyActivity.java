package eu.spottless.userit.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import eu.spottless.userit.R;
import eu.spottless.userit.adapter.FullScreenImageAdapter;
import eu.spottless.userit.responseModel.tasksResponse.ReplyData;
import eu.spottless.userit.utils.LocaleHelper;

import android.content.Context;
import android.os.Bundle;

import java.util.List;

public class ReplyActivity extends AppCompatActivity {

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply);

        ViewPager viewPager = findViewById(R.id.pager);

        List<ReplyData> replyData = (List<ReplyData>) getIntent().getSerializableExtra("reply");
//          replyData = getIntent().getParcelableExtra("reply");
//        Log.d("replyData---", new Gson().toJson(replyData));

        FullScreenImageAdapter adapter = new FullScreenImageAdapter(ReplyActivity.this, replyData);

        viewPager.setAdapter(adapter);

        // displaying selected image first
//        viewPager.setCurrentItem(position);

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

}
