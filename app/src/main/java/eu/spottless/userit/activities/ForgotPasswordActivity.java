package eu.spottless.userit.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import eu.spottless.userit.R;
import eu.spottless.userit.utils.LocaleHelper;
import eu.spottless.userit.utils.NetworkUtils;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edit_mobileno;
    private LinearLayout linear_reset,linear_otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        init();
    }

    private void init() {

        edit_mobileno = findViewById(R.id.edit_mobileno);
        linear_reset = findViewById(R.id.linear_reset);
        linear_otp = findViewById(R.id.linear_otp);

        linear_reset.setVisibility(View.VISIBLE);
        linear_otp.setVisibility(View.GONE);

        Button btn_send = findViewById(R.id.btn_send);
        btn_send.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String getMobileno = edit_mobileno.getText().toString();

        if (validate(getMobileno)) {
            if (!NetworkUtils.getConnectivityStatusString(this).equals(getText(R.string.not_internet))) {
                resetPassword(getMobileno);
            } else {
                Toast.makeText(this, getText(R.string.internet_not), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean validate(String mobileno) {
        if (mobileno.trim().length() == 0 || mobileno == null) {
            edit_mobileno.setError(getText(R.string.label_mobile));
            return false;
        }
//        if (!mobileno.matches("[1-9][0-9]{9}")) {
//            edit_mobileno.setError("Please enter valid mobile no.");
//            return false;
//        }
        if (mobileno.length() == 11 && mobileno.length() != 0) {
            edit_mobileno.setError(getText(R.string.enter_mobile_error));
            return false;
        }
        return true;
    }

    private void resetPassword(final String mobileno) {
//        ProgressDialog progressDialog = new ProgressDialog(ForgotPasswordActivity.this);
//        progressDialog.setMessage(getString(R.string.loading));
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        linear_reset.setVisibility(View.GONE);
        linear_otp.setVisibility(View.VISIBLE);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }
}
