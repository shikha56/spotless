package eu.spottless.userit.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import eu.spottless.userit.R;
import eu.spottless.userit.TinyDB;
import eu.spottless.userit.adapter.EmployeeListAdapter;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;
import eu.spottless.userit.responseModel.employeeResponse.EmployeeList;
import eu.spottless.userit.responseModel.employeeResponse.EmployeeListData;
import eu.spottless.userit.responseModel.employeeResponse.EmployeeResponse;
import eu.spottless.userit.utils.LocaleHelper;
import eu.spottless.userit.utils.NetworkUtils;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;


import java.util.List;

public class EmployeeListActivity extends AppCompatActivity {
    final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private EmployeeListAdapter employeeListAdapter;
    String token;

    Realm mRealm;
    EmployeeListData tinydbEmployeeData;
    List<EmployeeList> realmEmployeeList;
    TinyDB tinydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_list);

        init();
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmployeeListActivity.this.onBackPressed();
            }
        });

        tinydb = new TinyDB(this);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

//        mRealm = Realm.getDefaultInstance();

        token = "Bearer" + SharedPreferencesClass.retriveToken(this, "token");
//        Log.d("token---------", token);

//        if (!NetworkUtils.getConnectivityStatusString(this).equals("Not connected to Internet")) {
//            employeeListApiCall(token);
//        } else {
//            getAllEmployees();
//        }

    }

    public  void employeeListApiCall(String authtoken){
        progressDialog = new ProgressDialog(EmployeeListActivity.this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiService apiService = ServiceGenerator.createService(ApiService.class, authtoken);
        Call<EmployeeResponse> call = apiService.userList(authtoken);
        call.enqueue(new Callback<EmployeeResponse>() {
            @Override
            public void onResponse(Call<EmployeeResponse> call, Response<EmployeeResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    EmployeeResponse employeeResponse = response.body();
//                    Log.d("employeeResponse---", new Gson().toJson(employeeResponse));

                    EmployeeListData employeeListData = employeeResponse.getEmployeeListData();
//                    Log.d("employeeListData---", new Gson().toJson(employeeListData));

                    final List<EmployeeList> employeeLists = employeeListData.getEmployeeLists();
                    tinydb.putObject("employeeLists", employeeListData); //saves the object
                    employeeListAdapter = new EmployeeListAdapter(employeeLists,R.layout.employee_list_items, getApplicationContext());
                    recyclerView.setAdapter(employeeListAdapter);
                    employeeListAdapter.setOnItemClicklListener(new EmployeeListAdapter.EmployeeListItemClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                            int upcomingcount = Integer.parseInt(employeeLists.get(position).getTask_count().getUpcomming_task());
                            int overduecount = Integer.parseInt(employeeLists.get(position).getTask_count().getOverdue_task());
                            int pendingcount = Integer.parseInt(employeeLists.get(position).getTask_count().getPending_task());
                            int completedcount = Integer.parseInt(employeeLists.get(position).getTask_count().getComplete_task());

//                            int empId = employeeLists.get(position).getId();
//                            SharedPreferencesClass.insertEmpId(getApplicationContext(),"empId",empId);
                        Intent intent = new Intent(view.getContext(), Dashboard.class);
                        intent.putExtra("empId",employeeLists.get(position).getId());
                        intent.putExtra("upcoming", 0);
                        intent.putExtra("upcomingcount",upcomingcount);
                        intent.putExtra("overduecount",overduecount);
                        intent.putExtra("pendingcount",pendingcount);
                        intent.putExtra("completedcount",completedcount);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        }
                    });

                } else {
                    Toast.makeText(EmployeeListActivity.this, getText(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("profile error---", t.toString());
                progressDialog.dismiss();
//                Toast.makeText(EmployeeListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getEmployees(){

        if(tinydb.getObject("employeeLists", EmployeeListData.class) != null){
            tinydbEmployeeData = tinydb.getObject("employeeLists", EmployeeListData.class); // retrieves the object from storage

            realmEmployeeList = tinydbEmployeeData.getEmployeeLists();
            employeeListAdapter = new EmployeeListAdapter(realmEmployeeList,R.layout.employee_list_items, getApplicationContext());
            recyclerView.setAdapter(employeeListAdapter);
            employeeListAdapter.setOnItemClicklListener(new EmployeeListAdapter.EmployeeListItemClickListener() {
                @Override
                public void onClick(View view, int position) {
                    int upcomingcount = Integer.parseInt(realmEmployeeList.get(position).getTask_count().getUpcomming_task());
                    int overduecount = Integer.parseInt(realmEmployeeList.get(position).getTask_count().getOverdue_task());
                    int pendingcount = Integer.parseInt(realmEmployeeList.get(position).getTask_count().getPending_task());
                    int completedcount = Integer.parseInt(realmEmployeeList.get(position).getTask_count().getComplete_task());

//                            int empId = employeeLists.get(position).getId();
//                            SharedPreferencesClass.insertEmpId(getApplicationContext(),"empId",empId);
                    Intent intent = new Intent(view.getContext(), Dashboard.class);
                    intent.putExtra("empId",realmEmployeeList.get(position).getId());
                    intent.putExtra("upcoming", 0);
                    intent.putExtra("upcomingcount",upcomingcount);
                    intent.putExtra("overduecount",overduecount);
                    intent.putExtra("pendingcount",pendingcount);
                    intent.putExtra("completedcount",completedcount);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem search = menu.findItem(R.id.app_bar_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                employeeListAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        employeeListApiCall(token);
//    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(networkChangeReceiver);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String netstatus = NetworkUtils.getConnectivityStatusString(context);
//            Toast.makeText(context, netstatus, Toast.LENGTH_LONG).show();
            if (!NetworkUtils.getConnectivityStatusString(EmployeeListActivity.this).equals(context.getText(R.string.not_internet))) {
                employeeListApiCall(token);
            } else {
                getEmployees();
            }
        }
    };
}