package eu.spottless.userit.activities;

import androidx.appcompat.app.AppCompatActivity;
import eu.spottless.userit.R;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.utils.LocaleHelper;
import eu.spottless.userit.utils.MyCanvas;
import eu.spottless.userit.utils.TouchImageView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FullScreenViewActivity extends AppCompatActivity {
    TouchImageView imgDisplay;
    Button btnClose, btnEdit, btnSave;
    MyCanvas myCanvas;
    LinearLayout mDrawingPad;
    RelativeLayout main_image;
    ProgressDialog progressDialog;
    String role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_view);

        init();
    }

    public void init(){
        myCanvas = new MyCanvas(this);

        main_image = findViewById(R.id.main_image);
        imgDisplay = findViewById(R.id.imgDisplay);
        btnClose = findViewById(R.id.btnClose);
        btnEdit = findViewById(R.id.btnEdit);
        btnSave = findViewById(R.id.btnSave);
        mDrawingPad = findViewById(R.id.view_drawing_pad);

        progressDialog = new ProgressDialog(FullScreenViewActivity.this);

        role = SharedPreferencesClass.retriveRole(this, "role");

        Intent i = getIntent();
        String image = i.getStringExtra("img");
        Bundle extras = getIntent().getExtras();
        byte[] byteArray = extras.getByteArray("imageuri");
        Bitmap imageuri = null;
        Log.d("FullScreenViewActivity", "image: " + image);
//        Log.d("FullScreenViewActivity", "byteArray: " + byteArray.length);

        if (image != null && byteArray == null) {
            progressDialog.setMessage(getText(R.string.image_uploading));
            progressDialog.show();
            Picasso.with(this)
                    .load(image)
                    .error(R.drawable.ic_camera)
                    .placeholder(R.drawable.ic_camera)
                    .resize(400, 400)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            imgDisplay.setImageBitmap(bitmap);
                            //stop progressbar here
                            progressDialog.hide();
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            //stop progressbar here
                            progressDialog.hide();
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                        }
                    });
//            new DownloadImageTask(imgDisplay).execute(image);
        }
        else if (image == null && byteArray != null){
            if(byteArray.length != 0){
                imageuri = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                imgDisplay.setImageBitmap(imageuri);
            }
        }
        else if (image == null && byteArray == null) {
            imgDisplay.setImageResource(R.mipmap.ic_launcher);
        }

        if (role.equals("employee")) {
            btnEdit.setVisibility(View.GONE);
            btnSave.setVisibility(View.GONE);
        } else if (role.equals("supervisor")) {
            btnEdit.setVisibility(View.VISIBLE);
            btnSave.setVisibility(View.GONE);
        }

        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FullScreenViewActivity.this.finish();
            }
        });

        // edit button click event
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawingPad.addView(myCanvas);
                btnEdit.setClickable(false);
                btnEdit.setEnabled(false);
                btnSave.setVisibility(View.VISIBLE);
            }
        });

        // edit button click event
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                File file = saveBitMap(FullScreenViewActivity.this, main_image);
                if (file != null) {
                    btnSave.setClickable(false);
                    btnSave.setEnabled(false);
                    Toast.makeText(FullScreenViewActivity.this, getText(R.string.image_saved), Toast.LENGTH_SHORT).show();
                    FullScreenViewActivity.this.finish();
//                    Log.i("TAG", "Drawing saved to the gallery!");
                } else {
                    Toast.makeText(FullScreenViewActivity.this, getText(R.string.image_not_saved), Toast.LENGTH_SHORT).show();
                    Log.i("TAG", "Oops! Image could not be saved.");
                }

            }
        });
    }

    private File saveBitMap(Context context, View drawView){
        File pictureFileDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"Spottless");
        if (!pictureFileDir.exists()) {
            boolean isDirectoryCreated = pictureFileDir.mkdirs();
            if(!isDirectoryCreated)
                Log.i("TAG", "Can't create directory to save the image");
        }
        String filename = pictureFileDir.getPath() +File.separator+ System.currentTimeMillis()+".jpg";
        File pictureFile = new File(filename);
        Bitmap bitmap = getBitmapFromView(drawView);
        try {
            pictureFile.createNewFile();
            FileOutputStream oStream = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, oStream);
            oStream.flush();
            oStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("TAG", "There was an issue saving the image.");
        }
        scanGallery( context,pictureFile.getAbsolutePath());
        return pictureFile;
    }

    private Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        }   else{
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    private void scanGallery(Context cntx, String path) {
        try {
            MediaScannerConnection.scanFile(cntx, new String[]{path}, null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("TAG", "There was an issue scanning gallery.");
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        private ProgressDialog pd = new ProgressDialog(FullScreenViewActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage(getText(R.string.image_uploading));
            pd.show();
        }

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bmp = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bmp = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bmp;
        }
        protected void onPostExecute(Bitmap result) {
            pd.hide();
            pd.dismiss();
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }
}
