package eu.spottless.userit.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import eu.spottless.userit.R;
import eu.spottless.userit.adapter.NotificationAdapter;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;
import eu.spottless.userit.responseModel.notificationResponse.MainData;
import eu.spottless.userit.responseModel.notificationResponse.NotificationList;
import eu.spottless.userit.responseModel.notificationResponse.NotificationResponse;
import eu.spottless.userit.utils.LocaleHelper;
import eu.spottless.userit.utils.NetworkUtils;
import eu.spottless.userit.utils.PaginationScrollListener;
import eu.spottless.userit.viewmodel.TaskViewModel;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;

public class NotificationsActivity extends AppCompatActivity {
    final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefresh;
    private TaskViewModel taskViewModel;
    private NotificationAdapter notificationAdapter;
    private ProgressBar progress_bar;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int page;
    private String nextPage;
    LinearLayoutManager linearLayoutManager;
    String token;
    RealmList<NotificationList> notificationList;

    Realm mRealm;
    RealmResults<MainData> realmMainData;
    RealmList<NotificationList> realmNotificationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("notification.realm")
                .schemaVersion(1)
                .build();

        Realm.setDefaultConfiguration(config);

        init();
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationsActivity.this.onBackPressed();
            }
        });

        token = "Bearer" + SharedPreferencesClass.retriveToken(this, "token");
//        Log.d("token---------", token);
        taskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
        swipeRefresh = findViewById(R.id.swiperefresh);
        progress_bar = findViewById(R.id.progress_bar);
        recyclerView = findViewById(R.id.recycler_view);

        mRealm = Realm.getDefaultInstance();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!NetworkUtils.getConnectivityStatusString(getApplicationContext()).equals(getText(R.string.not_internet))) {
                    notificationListApiCall(token);
                } else {
                    getNotificationCount();
                }
            }
        });
    }

    public void notificationListApiCall(String token){
        swipeRefresh.setRefreshing(true);
        taskViewModel.getNotifications(token).observe(this, new Observer<MainData>() {
            @Override
            public void onChanged(@Nullable MainData mainData) {
                swipeRefresh.setRefreshing(false);
                if(mainData.getNotificationList() != null){
                     notificationList = mainData.getNotificationList();
//                    Log.d("notificationLists---", new Gson().toJson(notificationList));
                    addDataToRealm(mainData);
                    prepareRecyclerView(notificationList);
                }
            }
        });
    }

    private void prepareRecyclerView(final RealmList<NotificationList> notificationLists) {

        notificationAdapter = new NotificationAdapter(notificationLists,R.layout.notification_items,getApplicationContext());
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            linearLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(linearLayoutManager);
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(this, 4));

        }

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(notificationAdapter);
        recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                if (!isLastPage) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            notificationPaginationApiCall(token,page);
                        }
                    }, 200);
                }
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        notificationAdapter.notifyDataSetChanged();
    }

    private void addDataToRealm(final MainData mainData) {
        mRealm.beginTransaction();
//        try {
//            mRealm.deleteAll();
//        } catch (Exception ex){
//            throw ex;
//        }
        MainData mainData1 = new MainData();
        mainData1.setType("notificationList");
        mainData1.setTotal(mainData.getTotal());
        mainData1.setNotificationList(mainData.getNotificationList());

        mRealm.copyToRealm(mainData1); // Persist unmanaged objects
        mRealm.commitTransaction();

    }

    private void getNotificationCount() {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmMainData = realm.where(MainData.class).equalTo("type", "notificationList").findAll();
                swipeRefresh.setRefreshing(false);
                if(realmMainData != null) {
                    for (MainData mainData : realmMainData) {
//                        Log.e("realmMainData ---", String.valueOf(mainData.getTotal()));
                         realmNotificationList = mainData.getNotificationList();
                    }
                    notificationAdapter = new NotificationAdapter(realmNotificationList,R.layout.notification_items,getApplicationContext());
                    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                        linearLayoutManager = new LinearLayoutManager(NotificationsActivity.this);
                        recyclerView.setLayoutManager(linearLayoutManager);
                    } else {
                        recyclerView.setLayoutManager(new GridLayoutManager(NotificationsActivity.this, 4));

                    }

                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(notificationAdapter);
                    recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
                        @Override
                        protected void loadMoreItems() {
                            isLoading = true;
                            if (!isLastPage) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        notificationPaginationApiCall(token,page);
                                    }
                                }, 200);
                            }
                        }

                        @Override
                        public boolean isLastPage() {
                            return isLastPage;
                        }

                        @Override
                        public boolean isLoading() {
                            return isLoading;
                        }
                    });
                    notificationAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    public void notificationPaginationApiCall(String token,int pageno) {
//        progress_bar.setVisibility(View.VISIBLE);

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
        Call<NotificationResponse> call = apiService.notificationsPagination(token,pageno);
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                //hiding progress dialog
                if (response.isSuccessful()) {
                    swipeRefresh.setRefreshing(false);
//                    progress_bar.setVisibility(View.VISIBLE);
                    NotificationResponse notificationResponse = response.body();
                    if (notificationResponse.getMainData() != null){
                        Log.e("notificationResponse---", new Gson().toJson(notificationResponse));
                        resultAction(notificationResponse.getMainData());
                    }
                } else {
                    Toast.makeText(NotificationsActivity.this, getText(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("notification error---", new Gson().toJson(t.getCause()));
            }
        });
    }

    private void resultAction(MainData mainData) {
//        notificationList.clear();
        progress_bar.setVisibility(View.INVISIBLE);
        swipeRefresh.setRefreshing(false);
        isLoading = false;
        if (mainData != null) {
            notificationList = mainData.getNotificationList();
            notificationAdapter.addItems(notificationList);
//            notificationAdapter = new NotificationAdapter(mainData.getNotificationList(),R.layout.notification_items,getApplicationContext());
//            recyclerView.setAdapter(notificationAdapter);
            if (mainData.getTo() == mainData.getTotal()) {
                swipeRefresh.setRefreshing(false);
                notificationAdapter.setItems(notificationList);
                isLastPage = true;
//                page = mainData.getLast_page();
            } else {
                notificationAdapter.setItems(notificationList);
                page = mainData.getCurrent_page() + 1;
            }
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(networkChangeReceiver);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String netstatus = NetworkUtils.getConnectivityStatusString(context);
            if (!NetworkUtils.getConnectivityStatusString(context).equals(context.getText(R.string.not_internet))) {
                notificationListApiCall(token);
            } else {
                getNotificationCount();
            }
//            Toast.makeText(context, netstatus, Toast.LENGTH_LONG).show();
//            Log.d("app","Network connectivity change");
        }
    };
}
