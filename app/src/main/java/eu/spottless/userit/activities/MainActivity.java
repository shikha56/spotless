package eu.spottless.userit.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import eu.spottless.userit.R;
import eu.spottless.userit.TinyDB;
import eu.spottless.userit.adapter.AyalmaExpandAdapter;
import eu.spottless.userit.adapter.CustomAdapter;
import eu.spottless.userit.adapter.RecycleViewAdapter;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;
import eu.spottless.userit.responseModel.countResponse.Data;
import eu.spottless.userit.responseModel.loginResponse.LoginResponse;
import eu.spottless.userit.responseModel.notificationResponse.MainData;
import eu.spottless.userit.responseModel.notificationResponse.NotificationResponse;
import eu.spottless.userit.responseModel.countResponse.TaskCountResponse;
import eu.spottless.userit.utils.ConnectivityReceiver;
import eu.spottless.userit.utils.LocaleHelper;
import eu.spottless.userit.utils.NetworkUtils;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private ProgressDialog progressDialog;
    private Toolbar toolbar;
    private TextView txt_notificationCount;
    private int upcomingcount, overduecount, pendingcount, completedcount, employeecount, declinedcount;
    int notificationCount;
    private TextView txt_upcomingno, txt_overdueno, txt_pendingno, txt_completedno, txt_employeecount,txt_declinedno;
    private TextView txt_upcoming, txt_overdue, txt_pending, txt_completed, txt_employeeList,txt_declined;
    Realm mRealm;
    RealmResults<MainData> realmMainData;
    RealmResults<Data> realmData;
    TinyDB tinydb;
    String authtoken, role, message, languageCode, logout_title, logout_message, yes, no;
    String[] languageNames = {"English", "Danish"};
    int flags[] = {R.drawable.english, R.drawable.danish};
    private Menu mOptionsMenu;
    AlertDialog.Builder alertDialogBuilder;
    String netstatus;
//    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("count.realm")
                .schemaVersion(3)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(config);

        init();
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        authtoken = SharedPreferencesClass.retriveToken(this, "token");
        role = SharedPreferencesClass.retriveRole(this, "role");
//        SharedPreferencesClass.insertToken(MainActivity.this, "token", authtoken);

        Log.e("authtoken-----", authtoken);
//        Log.e("role-----", role);

        mRealm = Realm.getDefaultInstance();
        tinydb = new TinyDB(this);

        languageCode = tinydb.getString("languageCode");

        if (role.equals("employee")) {
            toolbar.setTitle(getText(R.string.empDashboard));
        } else if (role.equals("supervisor")) {
            toolbar.setTitle(getText(R.string.superDashboard));
        }

        upcomingcount = getIntent().getIntExtra("upcomingcount", 0);
        overduecount = getIntent().getIntExtra("overduecount", 0);
        pendingcount = getIntent().getIntExtra("pendingcount", 0);
        completedcount = getIntent().getIntExtra("completedcount", 0);
        employeecount = getIntent().getIntExtra("employeecount", 0);
        declinedcount = getIntent().getIntExtra("declinedcount", 0);

        LinearLayout linear_upcoming = findViewById(R.id.linear_upcoming);
        LinearLayout linear_overdue = findViewById(R.id.linear_overdue);
        LinearLayout linear_pending = findViewById(R.id.linear_pending);
        LinearLayout linear_completed = findViewById(R.id.linear_completed);
        CardView linear_declined = findViewById(R.id.linear_declined);
        CardView linear_employeeList = findViewById(R.id.linear_employeeList);

        txt_upcoming = findViewById(R.id.txt_upcoming);
        txt_overdue = findViewById(R.id.txt_overdue);
        txt_pending = findViewById(R.id.txt_pending);
        txt_completed = findViewById(R.id.txt_completed);
        txt_employeeList = findViewById(R.id.txt_employeeList);
        txt_declined = findViewById(R.id.txt_declined);

        txt_upcoming.setText(getText(R.string.upcomingtask));
        txt_overdue.setText(getText(R.string.overduetask));
        txt_pending.setText(getText(R.string.pendingtask));
        txt_completed.setText(getText(R.string.completedtask));
        txt_employeeList.setText(getText(R.string.employeelist));
        txt_declined.setText(getText(R.string.declinedTask));

        txt_upcomingno = findViewById(R.id.txt_upcomingno);
        txt_overdueno = findViewById(R.id.txt_overdueno);
        txt_pendingno = findViewById(R.id.txt_pendingno);
        txt_completedno = findViewById(R.id.txt_completedno);
        txt_employeecount = findViewById(R.id.txt_employeecount);
        txt_declinedno = findViewById(R.id.txt_declinedno);

        txt_upcomingno.setText(String.valueOf(upcomingcount));
        txt_overdueno.setText(String.valueOf(overduecount));
        txt_pendingno.setText(String.valueOf(pendingcount));
        txt_completedno.setText(String.valueOf(completedcount));
        txt_employeecount.setText(String.valueOf(employeecount));
        txt_declinedno.setText(String.valueOf(declinedcount));

        if (role.equals("employee")) {
            linear_employeeList.setVisibility(View.GONE);
            linear_declined.setVisibility(View.GONE);
        } else if (role.equals("supervisor")) {
            linear_employeeList.setVisibility(View.VISIBLE);
            linear_declined.setVisibility(View.VISIBLE);
        }

        linear_upcoming.setOnClickListener(this);
        linear_overdue.setOnClickListener(this);
        linear_pending.setOnClickListener(this);
        linear_completed.setOnClickListener(this);
        linear_declined.setOnClickListener(this);
        linear_employeeList.setOnClickListener(this);

//        updateViews(languageCode);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    public void customLanguageDialog() {
        // custom dialog
        final Dialog dialog1 = new Dialog(this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog1.setContentView(R.layout.language_dialog_box);
        dialog1.setCancelable(false);
        ImageView img_cancle = dialog1.findViewById(R.id.img_close);
        RecyclerView recycler_lang = dialog1.findViewById(R.id.recycler_lang);
        recycler_lang.setLayoutManager(new LinearLayoutManager(this));
        CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), flags, languageNames);
        recycler_lang.setAdapter(customAdapter);
        customAdapter.setOnItemClicklListener(new CustomAdapter.LangItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (languageNames[position].equals("English")) {
                    dialog1.dismiss();
                    tinydb.putString("languageCode", "en");
                    tinydb.putString("language", "en");
                    //restart Activity

                    Log.e("english netstatus-----", netstatus);

                    updateViews("en");
                    String token = "Bearer " + authtoken;

                    if (!NetworkUtils.getConnectivityStatusString(MainActivity.this).equals("Not connected to Internet")) {
                        changeLanguage(token, "en");
                    } else if (!NetworkUtils.getConnectivityStatusString(MainActivity.this).equals("Ikke tilsluttet internettet")) {
                        changeLanguage(token, "en");
                    }

                } else if (languageNames[position].equals("Danish")) {
                    dialog1.dismiss();
                    tinydb.putString("languageCode", "da");
                    tinydb.putString("language", "dk");
                    //restart Activity
                    Log.e("danish netstatus-----", netstatus);

                    updateViews("da");
                    String token = "Bearer " + authtoken;

                    if (!NetworkUtils.getConnectivityStatusString(MainActivity.this).equals("Not connected to Internet")) {
                        changeLanguage(token, "dk");
                    } else if (!NetworkUtils.getConnectivityStatusString(MainActivity.this).equals("Ikke tilsluttet internettet")) {
                        changeLanguage(token, "dk");
                    }                }
            }
        });
        img_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view12) {
                dialog1.dismiss();
//                Toast.makeText(MainActivity.this, "Please select language", Toast.LENGTH_SHORT).show();
            }
        });
        dialog1.show();
    }

    private void updateViews(String languageCode) {
        Context context = LocaleHelper.setLocale(this, languageCode);
        Resources resources = context.getResources();

        txt_upcoming.setText(resources.getText(R.string.upcomingtask));
        txt_overdue.setText(resources.getText(R.string.overduetask));
        txt_pending.setText(resources.getText(R.string.pendingtask));
        txt_completed.setText(resources.getText(R.string.completedtask));
        txt_employeeList.setText(resources.getText(R.string.employeelist));
        txt_declined.setText(resources.getText(R.string.declinedTask));

        if (role.equals("employee")) {
            toolbar.setTitle(resources.getText(R.string.empDashboard));
        } else if (role.equals("supervisor")) {
            toolbar.setTitle(resources.getText(R.string.superDashboard));
        }

//        mOptionsMenu.findItem(R.id.action_changeLang).setTitle(resources.getString(R.string.change_lang));
        mOptionsMenu.findItem(R.id.action_logout).setTitle(resources.getString(R.string.logout));
        mOptionsMenu.findItem(R.id.action_profile).setTitle(resources.getString(R.string.profile));
        resources.getString(R.string.logout_title);
        resources.getString(R.string.logout_message);
        resources.getString(R.string.yes);
        resources.getString(R.string.no);
        resources.getString(R.string.internet_not);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mOptionsMenu = menu;
        final MenuItem menuItem = menu.findItem(R.id.action_notifications);

        View actionView = menuItem.getActionView();
        txt_notificationCount = actionView.findViewById(R.id.cart_badge);

        setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_profile) {
            Intent i = new Intent(MainActivity.this, ProfileActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
        }
//        if (id == R.id.action_changeLang) {
//            customLanguageDialog();
//        }
        if (id == R.id.action_logout) {
            // do something
            String token = "Bearer" + authtoken;
            logoutApiCall(token);
        }
        if (id == R.id.action_notifications) {
            // do something
            Intent i = new Intent(MainActivity.this, NotificationsActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupBadge() {
        if (!NetworkUtils.getConnectivityStatusString(MainActivity.this).equals(getString(R.string.not_internet))) {
            notificationListApiCall("Bearer " + authtoken);
        } else {
            getNotificationCount();
        }

//        if (!NetworkUtils.getConnectivityStatusString(this).equals("Not connected to Internet")) {
//            notificationListApiCall("Bearer " + authtoken);
//        } else if (!NetworkUtils.getConnectivityStatusString(this).equals("Ikke tilsluttet internettet")) {
//            notificationListApiCall("Bearer " + authtoken);
//        } else {
//            getNotificationCount();
//        }
    }

    public void notificationListApiCall(String token) {

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
        Call<NotificationResponse> call = apiService.notificationsList(token);
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                //hiding progress dialog
                if (response.isSuccessful()) {
                    NotificationResponse notificationResponse = response.body();
                    if (notificationResponse.getMainData() != null) {
                        addDataToRealm(notificationResponse.getMainData());
                        notificationCount = notificationResponse.getMainData().getTotal();
                        txt_notificationCount.setText(String.valueOf(notificationCount));
                        Log.e("notificationCount---", String.valueOf(notificationCount));
                    }
                } else {
                    Toast.makeText(MainActivity.this, getText(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("notification error---", new Gson().toJson(t.getCause()));
            }
        });
    }

    public void changeLanguage(String token, String lang) {
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
        Call<LoginResponse> call = apiService.changeLanguage(token, lang);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
//                Log.d("loginApiCall---", new Gson().toJson(response));
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    Log.d("changeLanguage---", new Gson().toJson(loginResponse));
                    if (loginResponse.getStatus().equals("success")) {
                        String message = loginResponse.getMessage();
                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
//                        txt_forgotpassword.setVisibility(View.GONE);
                    } else {
                        String message = loginResponse.getMessage();
                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                progressDialog.dismiss();
                Log.d("changeLanguage error---", new Gson().toJson(t.getMessage()));
            }
        });
    }

    public void taskCountApiCall(String token) {

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
        Call<TaskCountResponse> call = apiService.getCount(token);
        call.enqueue(new Callback<TaskCountResponse>() {
            @Override
            public void onResponse(Call<TaskCountResponse> call, Response<TaskCountResponse> response) {
                //hiding progress dialog
//                Log.d("taskCount response---", new Gson().toJson(response));
                if (response.isSuccessful()) {
                    TaskCountResponse taskCountResponse = response.body();
                    if (taskCountResponse.getData() != null) {
                        addCountDataToRealm(taskCountResponse.getData());
                        upcomingcount = taskCountResponse.getData().getTaskTotal().getUpcomming_task();
                        overduecount = taskCountResponse.getData().getTaskTotal().getOverdue_task();
                        pendingcount = taskCountResponse.getData().getTaskTotal().getPending_task();
                        completedcount = taskCountResponse.getData().getTaskTotal().getComplete_task();
                        employeecount = taskCountResponse.getData().getEmployee_count();
                        declinedcount = taskCountResponse.getData().getTaskTotal().getDecline_task();

                        txt_upcomingno.setText(String.valueOf(upcomingcount));
                        txt_overdueno.setText(String.valueOf(overduecount));
                        txt_pendingno.setText(String.valueOf(pendingcount));
                        txt_completedno.setText(String.valueOf(completedcount));
                        txt_employeecount.setText(String.valueOf(employeecount));
                        txt_declinedno.setText(String.valueOf(declinedcount));
                    }
                } else {
                    Toast.makeText(MainActivity.this, getText(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("taskCount error---", new Gson().toJson(t.getCause()));
            }
        });
    }

    private void addDataToRealm(final MainData mainData) {
        mRealm.beginTransaction();
        MainData mainData1 = new MainData();
        mainData1.setType("notification");
        mainData1.setTotal(mainData.getTotal());
        mainData1.setNotificationList(mainData.getNotificationList());

        mRealm.copyToRealm(mainData1); // Persist unmanaged objects
        mRealm.commitTransaction();

    }

    private void addCountDataToRealm(final Data data) {
        mRealm.beginTransaction();
        Data countData = new Data();
        countData.setType("taskcount");
        countData.setEmployee_count(data.getEmployee_count());
        countData.setTaskTotal(data.getTaskTotal());

        mRealm.copyToRealm(countData); // Persist unmanaged objects
        mRealm.commitTransaction();

    }

    private void getCount() {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmData = realm.where(Data.class).equalTo("type", "taskcount").findAll();
//                Log.e("realmMainData ---",realmMainData.toString());
                if (realmData != null) {
                    for (Data data : realmData) {
                        upcomingcount = data.getTaskTotal().getUpcomming_task();
                        overduecount = data.getTaskTotal().getOverdue_task();
                        pendingcount = data.getTaskTotal().getPending_task();
                        completedcount = data.getTaskTotal().getComplete_task();
                        declinedcount = data.getTaskTotal().getDecline_task();
                        employeecount = data.getEmployee_count();
//                        Log.e("offline taskcount---", String.valueOf(upcomingcount));
                        txt_upcomingno.setText(String.valueOf(upcomingcount));
                        txt_overdueno.setText(String.valueOf(overduecount));
                        txt_pendingno.setText(String.valueOf(pendingcount));
                        txt_completedno.setText(String.valueOf(completedcount));
                        txt_employeecount.setText(String.valueOf(employeecount));
                        txt_declinedno.setText(String.valueOf(declinedcount));
                    }
                }
            }
        });
    }

    private void getNotificationCount() {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmMainData = realm.where(MainData.class).equalTo("type", "notification").findAll();
//                Log.e("realmMainData ---",realmMainData.toString());
                if (realmMainData != null) {
                    for (MainData mainData : realmMainData) {
                        notificationCount = mainData.getTotal();
                        txt_notificationCount.setText(String.valueOf(notificationCount));
                        Log.e("offline notification---", String.valueOf(notificationCount));
                    }
                }
            }
        });
    }

    private void logoutApiCall(String token) {
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiService apiService =
                ServiceGenerator.createService(ApiService.class, token);
        Call<LoginResponse> call = apiService.logout(token);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    Log.e("logout---", new Gson().toJson(loginResponse));
                    message = loginResponse.getMessage();
                    tinydb.remove("taskid");
                    tinydb.remove("taskId");
                    tinydb.remove("taskID");
                    tinydb.remove("idTask");
                    tinydb.remove("tokens");
                    tinydb.remove("acceptId");
                    tinydb.remove("reason");
                    tinydb.remove("images");
                    tinydb.remove("comment");
                    tinydb.remove("remarkId");
                    tinydb.remove("validateId");
                    tinydb.remove("checklistId");
                    tinydb.remove("completed");
                    tinydb.remove("empCompleted");
                    tinydb.remove("upcoming");
                    tinydb.remove("empupcoming");
                    tinydb.remove("overdue");
                    tinydb.remove("empOverdue");
                    tinydb.remove("pending");
                    tinydb.remove("empPending");
                    tinydb.remove("declined");
                    tinydb.remove("empDeclined");
                    tinydb.remove("profile");
                    tinydb.remove("employeeLists");
                    tinydb.remove("taskLists");
                    RecycleViewAdapter.imagesList(null, null, null, null, null);
                    AyalmaExpandAdapter.imagesList(null, null, null);
                    SharedPreferences settings = getSharedPreferences("prefs", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.clear();
                    editor.commit();
                    finish();
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    if (Build.VERSION.SDK_INT >= 11) {
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    } else {
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    }
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(MainActivity.this, getText(R.string.no_network), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("logout error---", t.toString());
//                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        // your code.
        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getBaseContext().getString(R.string.logout_title));
        alertDialogBuilder
                .setMessage(getBaseContext().getString(R.string.logout_message))
                .setCancelable(false)
                .setPositiveButton(getBaseContext().getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                MainActivity.this.moveTaskToBack(true);
                                android.os.Process.killProcess(android.os.Process.myPid());
                                System.exit(1);
                            }
                        })

                .setNegativeButton(getBaseContext().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
//        Intent intent;
        switch (id) {
            case R.id.linear_upcoming:
                Intent intent = new Intent(MainActivity.this, Dashboard.class);
                intent.putExtra("upcoming", 3);
                intent.putExtra("upcomingcount", upcomingcount);
                intent.putExtra("overduecount", overduecount);
                intent.putExtra("pendingcount", pendingcount);
                intent.putExtra("completedcount", completedcount);
                intent.putExtra("declinedcount", declinedcount);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                break;

            case R.id.linear_overdue:
                Intent intent1 = new Intent(MainActivity.this, Dashboard.class);
                intent1.putExtra("upcoming", 2);
                intent1.putExtra("upcomingcount", upcomingcount);
                intent1.putExtra("overduecount", overduecount);
                intent1.putExtra("pendingcount", pendingcount);
                intent1.putExtra("completedcount", completedcount);
                intent1.putExtra("declinedcount", declinedcount);
                intent1.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent1);
                break;

            case R.id.linear_pending:
                Intent intent2 = new Intent(MainActivity.this, Dashboard.class);
                intent2.putExtra("upcoming", 0);
                intent2.putExtra("upcomingcount", upcomingcount);
                intent2.putExtra("overduecount", overduecount);
                intent2.putExtra("pendingcount", pendingcount);
                intent2.putExtra("completedcount", completedcount);
                intent2.putExtra("declinedcount", declinedcount);
                intent2.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent2);
                break;

            case R.id.linear_completed:
                Intent intent3 = new Intent(MainActivity.this, Dashboard.class);
                intent3.putExtra("upcoming", 1);
                intent3.putExtra("upcomingcount", upcomingcount);
                intent3.putExtra("overduecount", overduecount);
                intent3.putExtra("pendingcount", pendingcount);
                intent3.putExtra("completedcount", completedcount);
                intent3.putExtra("declinedcount", declinedcount);
                intent3.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent3);
                break;

            case R.id.linear_declined:
                Intent intent4 = new Intent(MainActivity.this, Dashboard.class);
                intent4.putExtra("upcoming", 4);
                intent4.putExtra("upcomingcount", upcomingcount);
                intent4.putExtra("overduecount", overduecount);
                intent4.putExtra("pendingcount", pendingcount);
                intent4.putExtra("completedcount", completedcount);
                intent4.putExtra("declinedcount", declinedcount);
                intent4.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent4);
                break;
            case R.id.linear_employeeList:
                Intent intent5 = new Intent(MainActivity.this, EmployeeListActivity.class);
                intent5.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent5);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(networkChangeReceiver);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            netstatus = NetworkUtils.getConnectivityStatusString(context);
            Toast.makeText(context, netstatus, Toast.LENGTH_LONG).show();
            Log.e("netstatus---",netstatus);

            if (!NetworkUtils.getConnectivityStatusString(MainActivity.this).equals(context.getString(R.string.not_internet))) {
                taskCountApiCall("Bearer" + authtoken);
                notificationListApiCall("Bearer" + authtoken);

            } else {
                getCount();
                getNotificationCount();
            }

//            if (tinydb.getString("language") != null){
//                if (NetworkUtils.getConnectivityStatusString(MainActivity.this).equals("Not connected to Internet")) {
//                    getCount();
//                    getNotificationCount();
//
//                } else if (NetworkUtils.getConnectivityStatusString(MainActivity.this).equals("Ikke tilsluttet internettet")) {
//                    getCount();
//                    getNotificationCount();
//                }
//                else {
//                    taskCountApiCall("Bearer" + authtoken);
//                    notificationListApiCall("Bearer" + authtoken);
//
//                    languageCode = tinydb.getString("language");
//                    Log.e("else if lang---",languageCode);
//                    String token = "Bearer " + authtoken;
//                    changeLanguage(token,languageCode);
//
//                }
//            }else {
//                if (NetworkUtils.getConnectivityStatusString(MainActivity.this).equals("Not connected to Internet")) {
//                    getCount();
//                    getNotificationCount();
//
//                } else if (NetworkUtils.getConnectivityStatusString(MainActivity.this).equals("Ikke tilsluttet internettet")) {
//                    getCount();
//                    getNotificationCount();
//
//                } else {
//                    languageCode = "en";
//                    Log.e("else lang---",languageCode);
//                    String token = "Bearer " + authtoken;
//                    changeLanguage(token,languageCode);
//                }
//            }
        }
    };
}
