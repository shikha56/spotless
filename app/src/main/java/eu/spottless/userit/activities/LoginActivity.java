package eu.spottless.userit.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import eu.spottless.userit.R;
import eu.spottless.userit.TinyDB;
import eu.spottless.userit.adapter.CustomAdapter;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;
import eu.spottless.userit.responseModel.ErrorRes;
import eu.spottless.userit.responseModel.loginResponse.LoginResponse;
import eu.spottless.userit.utils.LocaleHelper;
import eu.spottless.userit.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.io.File;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private EditText edit_username, edit_password;
    private TextView txt_forgotpassword, txt_login, txt_changeLanguage;
    private ProgressDialog progressDialog;
    private Button btn_login;
    private String newToken, deviceId;
    String netstatus, errorMessage, getUsername,languageCode, langCode,getPassword,
            label_username="Please enter username",label_password = "Please enter password",
            error_userpassword, loading = "Please wait…", internet_not, not_internet, attempts;
    TinyDB tinydb;
    String[] languageNames = {"English", "Danish"};
    int flags[] = {R.drawable.english, R.drawable.danish};
    boolean dialog_close = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("NEW_TOKEN", newToken);
            }
        });

        deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.e("deviceId", deviceId);

        init();
    }

    private void init() {
        tinydb = new TinyDB(this);

        edit_username = findViewById(R.id.edit_username);
        edit_password = findViewById(R.id.edit_password);

        txt_login = findViewById(R.id.txt_login);

        if(tinydb.getString("username") != null){
            edit_username.setText(tinydb.getString("username"));
        }

        edit_username.setSelection(edit_username.getText().length());
        edit_password.setSelection(edit_password.getText().length());

        txt_forgotpassword = (TextView) findViewById(R.id.txt_forgotpassword);
        txt_forgotpassword.setOnClickListener(this);

        txt_changeLanguage = (TextView) findViewById(R.id.txt_changeLanguage);
        txt_changeLanguage.setOnClickListener(this);

        if(isFirstTime()){
            customLanguageDialog(false);
        }

        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);

    }

    public void customLanguageDialog(final boolean dialogClose){
        // custom dialog
        final Dialog dialog1 = new Dialog(this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog1.setContentView(R.layout.language_dialog_box);
        dialog1.setCancelable(false);
        ImageView img_cancle = dialog1.findViewById(R.id.img_close);
        RecyclerView recycler_lang = dialog1.findViewById(R.id.recycler_lang);
        recycler_lang.setLayoutManager(new LinearLayoutManager(this));
        CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), flags, languageNames);
        recycler_lang.setAdapter(customAdapter);
        customAdapter.setOnItemClicklListener(new CustomAdapter.LangItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (languageNames[position].equals("English")) {
                    dialog1.dismiss();
                    tinydb.putString("languageCode","en");
                    tinydb.putString("language","en");
//                    finish();
//                    startActivity(getIntent());
                    updateViews("en");
                } else if (languageNames[position].equals("Danish")) {
                    dialog1.dismiss();
                    tinydb.putString("languageCode","da");
                    tinydb.putString("language","dk");
//                    finish();
//                    startActivity(getIntent());
                    updateViews("da");
                }
            }
        });
        img_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view12) {
//                dialog1.dismiss();
                if(dialogClose == false){
                    Toast.makeText(LoginActivity.this, "Please select language", Toast.LENGTH_SHORT).show();
                }
                else {
                    dialog1.dismiss();
                }
            }
        });
        dialog1.show();
    }

    private boolean isFirstTime()
    {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        if (!ranBefore) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
        }
        return !ranBefore;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }


    private void updateViews(String languageCode) {
        Context context = LocaleHelper.setLocale(this, languageCode);
        Resources resources = context.getResources();

        txt_login.setText(resources.getString(R.string.login));
        edit_username.setHint(resources.getString(R.string.username));
        edit_password.setHint(resources.getString(R.string.password));
        btn_login.setText(resources.getString(R.string.login));
        txt_forgotpassword.setText(resources.getString(R.string.reset_password));
        txt_changeLanguage.setText(resources.getString(R.string.change_lang));

        label_username = resources.getString(R.string.label_username);
        label_password = resources.getString(R.string.label_password);
        error_userpassword = resources.getString(R.string.error_userpassword);
        loading = resources.getString(R.string.loading);
        internet_not = resources.getString(R.string.internet_not);
        not_internet = resources.getString(R.string.not_internet);
        attempts = resources.getString(R.string.attempts);
    }

    @Override
    public void onClick(View view) {
        int v = view.getId();

        if (v == R.id.btn_login) {


            getUsername = edit_username.getText().toString();
            getPassword = edit_password.getText().toString();

            //validate form
            if (validateLogin(getUsername, getPassword)) {
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    // TODO: handle exception
                }
                if (!NetworkUtils.getConnectivityStatusString(this).equals(getString(R.string.not_internet))) {
                    Log.e("getUsername---", getUsername);
                    Log.e("getPassword---", getPassword);
                    tinydb.remove("taskLists");
                    if (tinydb.getString("language") != null){
                        langCode = tinydb.getString("language");
                        Log.e("if lang---",langCode);
                    }
                    else {
                        langCode = "en";
                        Log.e("else lang---",langCode);
                    }
                    doLogin(getUsername, getPassword,langCode);
                } else {
                    Toast.makeText(this, internet_not, Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if (v == R.id.txt_forgotpassword) {
            Log.e("resetpasword username--", getUsername);
              resetPassword(getUsername);
//            Intent i = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
//            startActivity(i);
        }
        else if (v == R.id.txt_changeLanguage) {
//            tinydb.putBoolean("isFirstTime",isFirstTime);
            customLanguageDialog(true);
        }

    }

    private boolean validateLogin(String username, String password) {
//        label_username = "Please enter username";
//        label_password = "Please enter password";

        if (username == null || username.trim().length() == 0) {
            Toast.makeText(LoginActivity.this,label_username, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password == null || password.trim().length() == 0) {
            Toast.makeText(LoginActivity.this,label_password, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void doLogin(final String username, final String password,final String lang) {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage(loading);
        progressDialog.setCancelable(false);
        progressDialog.show();

//        Log.d("loginApiCall---" ,"parameter\n"+"username : "+username +"\npassword :"+ password +"\ndeviceId :"+ deviceId
//                +"\nnewToken :"+ newToken+"\nlang :"+ lang);

        ApiService apiService =
                ServiceGenerator.createService(ApiService.class, username, password);
        Call<LoginResponse> call = apiService.basicLogin(username, password, deviceId, newToken,lang);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
//                Log.d("loginApiCall---", new Gson().toJson(response));
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    Log.d("loginApiCalljjhn---", new Gson().toJson(loginResponse));

                    if (loginResponse.getStatus().equals("success")) {
                        String message = loginResponse.getMessage();
                        String authtoken = loginResponse.getToken();
                        String role = loginResponse.getRole();
                        int upcomingcount = loginResponse.getTask().getUpcomming_task();
                        int overduecount = loginResponse.getTask().getOverdue_task();
                        int pendingcount = loginResponse.getTask().getPending_task();
                        int completedcount = loginResponse.getTask().getComplete_task();
                        int employeecount = loginResponse.getEmployee_count();
                        int declinedcount = loginResponse.getTask().getDecline_task();
                        SharedPreferencesClass.insertToken(LoginActivity.this, "token", authtoken);
                        SharedPreferencesClass.insertRole(LoginActivity.this, "role", role);

                        tinydb.putString("username",username);

                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

                        //login start main activity
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("upcomingcount", upcomingcount);
                        intent.putExtra("overduecount", overduecount);
                        intent.putExtra("pendingcount", pendingcount);
                        intent.putExtra("completedcount", completedcount);
                        intent.putExtra("employeecount", employeecount);
                        intent.putExtra("declinedcount", declinedcount);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        String message = loginResponse.getMessage();
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        if(message.equals(attempts)){
                            txt_forgotpassword.setVisibility(View.VISIBLE);
                        }
                        else if(message.equals("Your Login attempts are exceeded contact your administrator!")){
                            txt_forgotpassword.setVisibility(View.VISIBLE);
                        }
                        else if(message.equals("Kontakt dine administratorer med dine loginforsøg!")){
                            txt_forgotpassword.setVisibility(View.VISIBLE);
                        }
                        else {
                            txt_forgotpassword.setVisibility(View.GONE);
                        }
//                    Toast.makeText(LoginActivity.this, error_userpassword, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                progressDialog.dismiss();
                Log.d("loginResponse error---", new Gson().toJson(t.getCause()));
//                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void resetPassword(final String username) {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiService apiService =
                ServiceGenerator.createService(ApiService.class, username,"");
        Call<LoginResponse> call = apiService.resetPassword(username);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
//                Log.d("loginApiCall---", new Gson().toJson(response));
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    Log.d("reset---", new Gson().toJson(loginResponse));

                    if (loginResponse.getStatus().equals("success")) {
                        String message = loginResponse.getMessage();
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
//                        txt_forgotpassword.setVisibility(View.GONE);
                    }
                    else {
                        String message = loginResponse.getMessage();
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                progressDialog.dismiss();
                Log.d("reset error---", new Gson().toJson(t.getCause()));
//                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void changeLanguage(final String language) {
//        progressDialog = new ProgressDialog(LoginActivity.this);
//        progressDialog.setMessage(getString(R.string.loading));
//        progressDialog.setCancelable(false);
//        progressDialog.show();

        ApiService apiService =
                ServiceGenerator.createService(ApiService.class, "","");
        Call<LoginResponse> call = apiService.changeLanguage("",language);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //hiding progress dialog
//                progressDialog.dismiss();
//                Log.d("loginApiCall---", new Gson().toJson(response));
                if (response.isSuccessful()) {
//                    LoginResponse loginResponse = response.body();
                    Log.d("changeLanguage---", "change successfully");

//                    if (loginResponse.getStatus().equals("success")) {
//                        String message = loginResponse.getMessage();
//                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
////                        txt_forgotpassword.setVisibility(View.GONE);
//                    }
//                    else {
//                        String message = loginResponse.getMessage();
//                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
//                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
//                progressDialog.dismiss();
                Log.d("reset error---", new Gson().toJson(t.getCause()));
//                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(networkChangeReceiver);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            netstatus = NetworkUtils.getConnectivityStatusString(context);
            Toast.makeText(context, netstatus, Toast.LENGTH_LONG).show();
//            Log.d("app","Network connectivity change");
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            trimCache(this);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }

}
