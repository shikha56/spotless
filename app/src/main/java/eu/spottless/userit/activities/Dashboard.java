package eu.spottless.userit.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import eu.spottless.userit.R;
import eu.spottless.userit.TinyDB;
import eu.spottless.userit.adapter.PagerAdapter;
import eu.spottless.userit.callBacks.MainCallbacks;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.fragments.CompletedTask;
import eu.spottless.userit.fragments.DeclinedTask;
import eu.spottless.userit.fragments.OverdueTask;
import eu.spottless.userit.fragments.PendingTask;
import eu.spottless.userit.fragments.UpcomingTask;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;
import eu.spottless.userit.responseModel.countResponse.TaskCountResponse;
import eu.spottless.userit.utils.LocaleHelper;
import eu.spottless.userit.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Field;

public class Dashboard extends AppCompatActivity implements MainCallbacks {
    final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private SearchView searchView = null;
    private int upcomingcount, overduecount, pendingcount, completedcount,empId, declinedcount;
    int previousposition;
    private String authtoken, languageCode, role;
    TinyDB tinydb;
    PagerAdapter adapter;
    int upcoming;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        init();
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dashboard.this.onBackPressed();
            }
        });

        tinydb = new TinyDB(this);

        languageCode = tinydb.getString("languageCode");

        authtoken = "Bearer " + SharedPreferencesClass.retriveToken(this, "token");
        role = SharedPreferencesClass.retriveRole(this,"role");
        upcoming = getIntent().getIntExtra("upcoming", 0);
//        Log.e("tab1----", String.valueOf(upcoming));
//        taskCountApiCall(authtoken);
//        tinydb.remove("taskId");
        tinydb.remove("idTask");
        tinydb.remove("tokens");
        tinydb.remove("acceptId");
        tinydb.remove("reason");
        tinydb.remove("taskLists");

        upcomingcount = getIntent().getIntExtra("upcomingcount",0);
        overduecount = getIntent().getIntExtra("overduecount",0);
        pendingcount = getIntent().getIntExtra("pendingcount",0);
        completedcount = getIntent().getIntExtra("completedcount",0);
        declinedcount = getIntent().getIntExtra("declinedcount",0);
        empId = getIntent().getIntExtra("empId",0);

//        Log.e("empId----", String.valueOf(empId));

        tabLayout = findViewById(R.id.tab_layout);
        if(upcoming == 0){
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.darkyellow));
        }
        if(upcoming == 1){
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.green));
        }
        if(upcoming == 2){
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.red));
        }
        if(upcoming == 3){
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.grey));
        }
        if(upcoming == 4){
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.grey));
        }
        viewPager = findViewById(R.id.pager);
        adapter = new PagerAdapter(getSupportFragmentManager(),getBaseContext(),upcomingcount,overduecount,pendingcount,completedcount,empId,declinedcount);
        viewPager.setAdapter(adapter);
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });
        makeActionOverflowMenuShown();
        selectTabIndex(upcoming);
        // Attach the page change listener inside the activity
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {
//                Toast.makeText(getApplicationContext(), "Selected page position: " + position, Toast.LENGTH_SHORT).show();
                previousposition = position;
                if(position == 0){
                    viewPager.setCurrentItem(position);
                    tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.darkyellow));
                }
                if(position == 1){
                    viewPager.setCurrentItem(position);
                    tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.green));
                }
                if(position == 2){
                    viewPager.setCurrentItem(position);
                    tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.red));
                }
                if(position == 3){
                    viewPager.setCurrentItem(position);
                    tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.grey));
                }
                if(position == 4){
                    viewPager.setCurrentItem(position);
                    tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.grey));
                }
                if (searchView != null && !searchView.isIconified()) {
                    //searchView.onActionViewExpanded();
                    searchView.setIconified(true);
                    searchView.setIconified(true);

                }
            }
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                Log.d("Dashboard:","onPageScrolled-- "+position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
//                Log.e("Dashboard:","onPageScrollStateChanged-- "+state);
            }
        });

    }

    private void selectTabIndex(final int index) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                Log.e("Dashboard:","index-- "+index);
                tabLayout.setScrollPosition(index, 0, true);
                viewPager.setCurrentItem(index);
            }
        }, 100);

    }

    private void makeActionOverflowMenuShown() {
        //devices with hardware menu button (e.g. Samsung Note) don't show action overflow menu
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            Log.e("", e.getLocalizedMessage());
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        MenuItem mSearchMenuItem = menu.findItem(R.id.app_bar_search);
        searchView = (SearchView)mSearchMenuItem.getActionView();

        if (mSearchMenuItem != null) {
            searchView = (SearchView) mSearchMenuItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
        }
        searchView.setIconifiedByDefault(true);
        MenuItemCompat.expandActionView(mSearchMenuItem);

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String query) {
//                 this is your adapter that will be filtered
                PagerAdapter pagerAdapter = (PagerAdapter) viewPager.getAdapter();
                for (int i = 0; i < pagerAdapter.getCount(); i++) {

                    Fragment viewPagerFragment = (Fragment) viewPager.getAdapter().instantiateItem(viewPager, i);
                    if (viewPagerFragment != null && viewPagerFragment.isAdded()) {

                        if (viewPagerFragment instanceof PendingTask) {
                            PendingTask pendingTask= (PendingTask) viewPagerFragment;
                            if (pendingTask != null) {
                                pendingTask.beginSearch(query);
                            }
                        }else if (viewPagerFragment instanceof CompletedTask) {
                            CompletedTask completedTask= (CompletedTask) viewPagerFragment;
                            if (completedTask != null) {
                                completedTask.beginSearch(query);
                            }
                        }else if (viewPagerFragment instanceof OverdueTask) {
                            OverdueTask overdueTask= (OverdueTask) viewPagerFragment;
                            if (overdueTask != null) {
                                overdueTask.beginSearch(query);
                            }
                        }else if (viewPagerFragment instanceof UpcomingTask) {
                            UpcomingTask upcomingTask = (UpcomingTask) viewPagerFragment;
                            if (upcomingTask != null) {
                                upcomingTask.beginSearch(query);
                            }
                        }else if (viewPagerFragment instanceof DeclinedTask) {
                            DeclinedTask declinedTask = (DeclinedTask) viewPagerFragment;
                            if (declinedTask != null) {
                                declinedTask.beginSearch(query);
                            }
                        }
                    }
                }

                return true;

            }

            public boolean onQueryTextSubmit(String query) {
                // Here u can get the value "query" which is entered in the
                return false;

            }
        };
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onMsgFromFragToMain(String sender) {
        if (sender.equals("UPCOMING‐FRAG")) {
            Log.e("UPCOMING‐FRAG--", "UPCOMING");
            taskCountApiCall(authtoken);
        }
        else if (sender.equals("PENDING‐FRAG")) {
            Log.e("PENDING‐FRAG--", "PENDING");
            taskCountApiCall(authtoken);
        }
        else if (sender.equals("COMPLETED‐FRAG")) {
            Log.e("COMPLETED‐FRAG--", "COMPLETED");
            taskCountApiCall(authtoken);
        }

        else if (sender.equals("OVERDUE‐FRAG")) {
            Log.e("OVERDUE‐FRAG--", "OVERDUE");
            taskCountApiCall(authtoken);
        }
        else if (sender.equals("DECLINED‐FRAG")) {
            Log.e("DECLINED‐FRAG--", "DECLINED");
            taskCountApiCall(authtoken);
        }
    }

    public void taskCountApiCall(String token) {

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
        Call<TaskCountResponse> call = apiService.getCount(token);
        call.enqueue(new Callback<TaskCountResponse>() {
            @Override
            public void onResponse(Call<TaskCountResponse> call, Response<TaskCountResponse> response) {
                //hiding progress dialog
                if (response.isSuccessful()) {
                    TaskCountResponse taskCountResponse = response.body();
                    if (taskCountResponse.getData() != null){
                        upcomingcount = taskCountResponse.getData().getTaskTotal().getUpcomming_task();
                        overduecount = taskCountResponse.getData().getTaskTotal().getOverdue_task();
                        pendingcount = taskCountResponse.getData().getTaskTotal().getPending_task();
                        completedcount = taskCountResponse.getData().getTaskTotal().getComplete_task();
                        Log.e("taskCountApiCall:","pendingcount-- " + pendingcount);
                        if(previousposition == 0){
                            adapter = new PagerAdapter(getSupportFragmentManager(),getBaseContext(),upcomingcount,overduecount,pendingcount,completedcount,empId,declinedcount);
                            viewPager.setAdapter(adapter);
                            tabLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    tabLayout.setupWithViewPager(viewPager);
                                }
                            });
                            makeActionOverflowMenuShown();
                            selectTabIndex(0);
                        }
                        if(previousposition == 1){
                            adapter = new PagerAdapter(getSupportFragmentManager(),getBaseContext(),upcomingcount,overduecount,pendingcount,completedcount,empId,declinedcount);
                            viewPager.setAdapter(adapter);
                            tabLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    tabLayout.setupWithViewPager(viewPager);
                                }
                            });
                            makeActionOverflowMenuShown();
                            selectTabIndex(1);
                        }
                        if(previousposition == 2){
                            adapter = new PagerAdapter(getSupportFragmentManager(),getBaseContext(),upcomingcount,overduecount,pendingcount,completedcount,empId,declinedcount);
                            viewPager.setAdapter(adapter);
                            tabLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    tabLayout.setupWithViewPager(viewPager);
                                }
                            });
                            makeActionOverflowMenuShown();
                            selectTabIndex(2);
                        }
                        if(previousposition == 3){
                            adapter = new PagerAdapter(getSupportFragmentManager(),getBaseContext(),upcomingcount,overduecount,pendingcount,completedcount,empId,declinedcount);
                            viewPager.setAdapter(adapter);
                            tabLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    tabLayout.setupWithViewPager(viewPager);
                                }
                            });
                            makeActionOverflowMenuShown();
                            selectTabIndex(3);
                        }
                        if(previousposition == 4){
                            adapter = new PagerAdapter(getSupportFragmentManager(),getBaseContext(),upcomingcount,overduecount,pendingcount,completedcount,empId,declinedcount);
                            viewPager.setAdapter(adapter);
                            tabLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    tabLayout.setupWithViewPager(viewPager);
                                }
                            });
                            makeActionOverflowMenuShown();
                            selectTabIndex(4);
                        }
                    }
                } else {
                    Toast.makeText(Dashboard.this, getText(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("taskCount error---", new Gson().toJson(t.getCause()));
            }
        });
    }

//    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
//    public void onResultReceived(String result) {
//        Log.e("onResultReceived--",result);
//        taskCountApiCall(authtoken);
//    }

    @Override
    protected void onStart() {
        super.onStart();
//        EventBus.getDefault().register(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("onRestart---", "onRestart");
        if(previousposition == 0){
            try {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutpending, new PendingTask());
                fragmentTransaction.commit();
            } catch( IllegalStateException e ) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutpending, new PendingTask());
                fragmentTransaction.commitAllowingStateLoss();
            }
            tinydb.remove("taskLists");
            selectTabIndex(0);
        }
        if(previousposition == 1){
            try {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutCompleted, new CompletedTask());
                fragmentTransaction.commit();
            } catch( IllegalStateException e ) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutCompleted, new CompletedTask());
                fragmentTransaction.commitAllowingStateLoss();
            }
            tinydb.remove("taskLists");
            selectTabIndex(1);
        }
        if(previousposition == 2){
            try {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutOverdue, new OverdueTask());
                fragmentTransaction.commit();
            } catch( IllegalStateException e ) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutOverdue, new OverdueTask());
                fragmentTransaction.commitAllowingStateLoss();
            }
            tinydb.remove("taskLists");
            selectTabIndex(2);
        }
        if(previousposition == 3){
            try {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutupcoming, new UpcomingTask());
                fragmentTransaction.commit();
            } catch( IllegalStateException e ) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutupcoming, new UpcomingTask());
                fragmentTransaction.commitAllowingStateLoss();
            }
            tinydb.remove("taskLists");
            selectTabIndex(3);
        }
        if(previousposition == 4){
            try {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutDeclined, new DeclinedTask());
                fragmentTransaction.commit();
            } catch( IllegalStateException e ) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameLayoutDeclined, new DeclinedTask());
                fragmentTransaction.commitAllowingStateLoss();
            }
            tinydb.remove("taskLists");
            selectTabIndex(4);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(networkChangeReceiver);
//        EventBus.getDefault().unregister(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String netstatus = NetworkUtils.getConnectivityStatusString(context);
            if (!NetworkUtils.getConnectivityStatusString(Dashboard.this).equals(context.getString(R.string.not_internet))) {
                if(empId == 0){
                    taskCountApiCall(authtoken);
                }
            }
            else {
                Toast.makeText(Dashboard.this, context.getString(R.string.no_network), Toast.LENGTH_SHORT).show();
            }
        }
    };

//    @Override
//    public void onBackPressed() {
//        if (viewPager.getCurrentItem() == 0) {
//            // If the user is currently looking at the first step, allow the system to handle the
//            // Back button. This calls finish() on this activity and pops the back stack.
//            super.onBackPressed();
//        } else {
//            // Otherwise, select the previous step.
//            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
//        }
//    }
}
