package eu.spottless.userit.activities;

import androidx.appcompat.app.AppCompatActivity;
import eu.spottless.userit.R;
import eu.spottless.userit.TinyDB;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    SplashActivity.this.startActivity(i);
                    SplashActivity.this.finish();
            }
        }, 5000);
    }
}
