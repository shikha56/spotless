package eu.spottless.userit.remote;

import android.os.Build;

import java.io.IOException;

import androidx.annotation.RequiresApi;
import eu.spottless.userit.utils.ConnectivityReceiver;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


class AuthenticationInterceptor implements Interceptor {

    private final String authToken;

    public AuthenticationInterceptor(String token) {
        this.authToken = token;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder builder = original.newBuilder()
                .header("Authorization", authToken);

        Request request = builder.build();

// Add Cache Control only for GET methods
        if (request.method().equals("GET")) {
            if (!ConnectivityReceiver.isConnected()) {
                // 1 day
                request = request.newBuilder()
                        .header("Cache-Control", "only-if-cached")
                        .build();
            } else {
                // 4 weeks stale
                request = request.newBuilder()
                        .header("Cache-Control", "public, max-stale=2419200")
                        .build();
            }
        }

        Response originalResponse = chain.proceed(request);
        return originalResponse.newBuilder()
                .header("Cache-Control", "max-age=600")
                .build();
//        return chain.proceed(request);
    }
}
