package eu.spottless.userit.remote;

import eu.spottless.userit.responseModel.Security.SecurityQues;
import eu.spottless.userit.responseModel.tasksResponse.TasksResponse;
import io.reactivex.Observable;

import java.util.Map;

import eu.spottless.userit.responseModel.employeeResponse.EmployeeResponse;
import eu.spottless.userit.responseModel.loginResponse.LoginResponse;
import eu.spottless.userit.responseModel.notificationResponse.NotificationResponse;
import eu.spottless.userit.responseModel.ProfileResponse;
import eu.spottless.userit.responseModel.countResponse.TaskCountResponse;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> basicLogin(@Field("username") String username, @Field("password") String password, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token, @Field("lang") String lang);

    @GET("my-profile")
    Call<ProfileResponse> viewProfile(@Header("Authorization") String authHeader);

    @GET("reset-password/{USER_NAME}")
    Call<LoginResponse> resetPassword(@Path("USER_NAME") String username);

    @GET("change-lang/{language}")
    Call<LoginResponse> changeLanguage(@Header("Authorization") String authHeader,@Path("language") String language);

//    @Headers({"Content-Type: multipart/form-data","Content-Type: text/plain"})
//    @FormUrlEncoded
//    @Headers("Content-Type: multipart/form-data")
    @Multipart
    @POST("add-task/{id}")
    Call<LoginResponse> addNewTask(@Header("Authorization") String authHeader, @Path("id") String id, @Part MultipartBody.Part item_title, @Part MultipartBody.Part tool, @Part MultipartBody.Part proof);

//    @FormUrlEncoded
//    @Multipart
//    @POST("add-task/{id}")
//    Call<LoginResponse> addNewTask(@Header("Authorization") String authHeader, @Path("id") String id, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("user/update")
    Call<ProfileResponse> updateProfile(@Header("Authorization") String authHeader, @FieldMap Map<String, String> params);

    @GET("user/logout")
    Call<LoginResponse> logout(@Header("Authorization") String authHeader);

    @GET("user-list")
    Call<EmployeeResponse> userList(@Header("Authorization") String authHeader);

    @GET("task-list")
    Call<TasksResponse> tasksListUpdateUI(@Header("Authorization") String authHeader);

    @GET("task-list/{id}")
    Observable<TasksResponse> tasksList(@Header("Authorization") String authHeader, @Path("id") String id);

    @GET("security-question/{id}")
    Call<SecurityQues> securityQuesList(@Header("Authorization") String authHeader, @Path("id") String id);

    @FormUrlEncoded
    @PUT("update-task/{id}")
    Call<TasksResponse> updateTasks(@Header("Authorization") String authHeader, @Path("id") String id, @FieldMap Map<String, String> params);

    @GET("emp-task-list/{empid}/{id}")
    Call<TasksResponse> employeetasksList(@Header("Authorization") String authHeader, @Path("empid") String empid, @Path("id") String id);

    @FormUrlEncoded
    @POST("accept-task")
    Call<LoginResponse> acceptTask(@Header("Authorization") String authHeader, @Field("task_id") String task_id, @Field("is_accept") String is_accept, @Field("reason") String reason);


    @FormUrlEncoded
    @POST("notification-send")
    Call<LoginResponse> notificationSend(@Header("Authorization") String authHeader, @Field("receiver_id") String receiver_id, @Field("title") String title, @Field("body") String body);

    @GET("notifications")
    Call<NotificationResponse> notificationsList(@Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST("finish-task")
    Call<LoginResponse> finishTask(@Header("Authorization") String authHeader, @Field("task_id") String task_id);

    @GET("notifications")
    Call<NotificationResponse> notificationsPagination(@Header("Authorization") String authHeader,@Query("page") int pageno);

    @GET("getcount")
    Call<TaskCountResponse> getCount(@Header("Authorization") String authHeader);

}



