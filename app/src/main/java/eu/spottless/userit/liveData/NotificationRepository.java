package eu.spottless.userit.liveData;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;
import eu.spottless.userit.responseModel.notificationResponse.MainData;
import eu.spottless.userit.responseModel.notificationResponse.NotificationList;
import eu.spottless.userit.responseModel.notificationResponse.NotificationResponse;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationRepository {

    private RealmList<NotificationList> notificationLists = new RealmList<>();
    private MutableLiveData<MainData> mutableLiveData = new MutableLiveData<>();
    private String authtoken,id;

    public NotificationRepository(Application application) {
    }

    public MutableLiveData<MainData> getMutableLiveData(String token) {
        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);

        Call<NotificationResponse> call = apiService.notificationsList(token);

        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                NotificationResponse notificationResponse = response.body();
//                Log.d("tasksResponse---", new Gson().toJson(tasksResponse));

                if (notificationResponse != null && notificationResponse.getMainData() != null) {

                    MainData mainData = notificationResponse.getMainData();
                    notificationLists = notificationResponse.getMainData().getNotificationList();
                    mutableLiveData.setValue(mainData);
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                Log.e("onFailure-----",t.getMessage());
            }
        });
        return mutableLiveData;
    }
}
