package eu.spottless.userit.liveData;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.MutableLiveData;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;

import eu.spottless.userit.responseModel.tasksResponse.TasksList;
import eu.spottless.userit.responseModel.tasksResponse.TasksListData;
import eu.spottless.userit.responseModel.tasksResponse.TasksResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.reactivex.Observable;
import io.reactivex.Observer;

public class TaskListRepository {
    private List<TasksList> taskLists = new ArrayList<>();
//    private ArrayList<TasksResponse.TasksListData.TasksList> taskLists = new ArrayList<>();
    private MutableLiveData<TasksListData> mutableLiveData = new MutableLiveData<>();
    private String authtoken,id;
    Realm mRealm = null;

    public TaskListRepository(Application application) {
        mRealm = Realm.getDefaultInstance();
    }

    public MutableLiveData<TasksListData> getMutableLiveData(String token,String frag_id) {
        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);

        Observable<TasksResponse> observable = apiService.tasksList(token,frag_id);
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<TasksResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(TasksResponse value) {
//                        Log.d("tasksResponse---", new Gson().toJson(value.getTasksListData().getData()));
//                        taskLists = value.getTasksListData().getData();
                        mutableLiveData.setValue(value.getTasksListData());
                    }

                    @Override
                    public void onError(Throwable e) {
//                        Log.e("task onFailure-----",e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });

//        Call<TasksResponse> call = apiService.tasksList(token,frag_id);
//
//        call.enqueue(new Callback<TasksResponse>() {
//            @Override
//            public void onResponse(Call<TasksResponse> call, Response<TasksResponse> response) {
//                TasksResponse tasksResponse = response.body();
////                Log.d("tasksResponse---", new Gson().toJson(tasksResponse));
//
//                if (tasksResponse != null && tasksResponse.getTasksListData().getData() != null) {
//                    taskLists = (ArrayList<TasksResponse.TasksListData.TasksList>)tasksResponse.getTasksListData().getData();
//                    mutableLiveData.setValue(taskLists);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<TasksResponse> call, Throwable t) {
//                Log.e("task onFailure-----",t.getMessage());
//            }
//        });
        return mutableLiveData;
    }

}
