package eu.spottless.userit.liveData;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;

import eu.spottless.userit.responseModel.tasksResponse.TasksListData;
import eu.spottless.userit.responseModel.tasksResponse.TasksResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeTaskRepository {

//    private RealmList<TasksList> taskLists = new RealmList<>();
    private MutableLiveData<TasksListData> mutableLiveData = new MutableLiveData<>();

    public EmployeeTaskRepository(Application application) {
    }

    public MutableLiveData<TasksListData> getMutableLiveData(String token,String frag_id,String empId) {
        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);

        Call<TasksResponse> call = apiService.employeetasksList(token,empId,frag_id);

        call.enqueue(new Callback<TasksResponse>() {
            @Override
            public void onResponse(Call<TasksResponse> call, Response<TasksResponse> response) {
                TasksResponse tasksResponse = response.body();
//                Log.d("tasksResponse---", new Gson().toJson(tasksResponse));

                if (tasksResponse != null && tasksResponse.getTasksListData().getData() != null) {
//                    taskLists = tasksResponse.getTasksListData().getData();
                    mutableLiveData.setValue(tasksResponse.getTasksListData());
                }
            }

            @Override
            public void onFailure(Call<TasksResponse> call, Throwable t) {
                Log.e("employee onFailure-----",t.getMessage());
            }
        });
        return mutableLiveData;
    }
}
