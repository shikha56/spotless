package eu.spottless.userit.responseModel.countResponse;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.Index;

public class Data extends RealmObject {
    @Index
    private String type;

    @SerializedName("employee_count")
    private int employee_count;
    @SerializedName("task")
    private TaskTotal taskTotal;

    public Data(){

    }

    public int getEmployee_count() {
        return employee_count;
    }

    public void setEmployee_count(int employee_count) {
        this.employee_count = employee_count;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public TaskTotal getTaskTotal() {
        return taskTotal;
    }

    public void setTaskTotal(TaskTotal taskTotal) {
        this.taskTotal = taskTotal;
    }
}
