package eu.spottless.userit.responseModel.loginResponse;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("access_token")
    private String token;
    @SerializedName("token_type")
    private String token_type;
    @SerializedName("expires_in")
    private String expires_in;
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("company_name")
    private String company_name;
    @SerializedName("role")
    private String role;
    @SerializedName("profile_img")
    private String profile_img;
    @SerializedName("task")
    private Task task;
    @SerializedName("employee_count")
    private int employee_count;
//    @SerializedName("errors")
//    private Errorss errors;

    public LoginResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String authtoken) {
        this.token = authtoken;
    }

    public String getTokenType() {
        return token_type;
    }

    public void setTokenType(String tokentype) {
        this.token_type = tokentype;
    }

    public String getExpiresin() {
        return expires_in;
    }

    public void setExpiresin(String expiresin) {
        this.expires_in = expiresin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProfile_img() {
        return profile_img;
    }

    public void setProfile_img(String profile_img) {
        this.profile_img = profile_img;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public int getEmployee_count() {
        return employee_count;
    }

    public void setEmployee_count(int employee_count) {
        this.employee_count = employee_count;
    }

//    public Errorss getErrors() {
//        return errors;
//    }
//
//    public void setErrors(Errorss errors) {
//        this.errors = errors;
//    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
