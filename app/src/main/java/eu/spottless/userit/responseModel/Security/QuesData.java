package eu.spottless.userit.responseModel.Security;

import com.google.gson.annotations.SerializedName;

public class QuesData {

    @SerializedName("name")
    private String name;
    @SerializedName("answer")
    private String answer;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
