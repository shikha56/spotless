package eu.spottless.userit.responseModel.tasksResponse;

import com.google.gson.annotations.SerializedName;


public class TasksResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;

//    @PrimaryKey
//    @Required
    @SerializedName("data")
    private TasksListData tasksListData;

    public TasksResponse(){

    }

    public TasksResponse(String status, String message, TasksListData tasksListData) {

        this.status = status;
        this.message = message;
        this.tasksListData = tasksListData;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TasksListData getTasksListData() {
        return tasksListData;
    }

    public void setTasksListData(TasksListData tasksListData) {
        this.tasksListData = tasksListData;
    }
}
