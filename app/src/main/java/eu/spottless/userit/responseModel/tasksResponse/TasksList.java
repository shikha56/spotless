package eu.spottless.userit.responseModel.tasksResponse;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


//@Parcel
public class TasksList implements Serializable {
    public static final String PROPERTY_ID = "id";

//            @PrimaryKey
//            @Required
            @SerializedName("id")
            private int id;

            @SerializedName("name")
            private String name;


            @SerializedName("company_id")
            private int company_id;

            @SerializedName("client_id")
            private int client_id;

            @SerializedName("check_sheet_id")
            private int check_sheet_id;

            @SerializedName("frequency")
            private String frequency;

//            @SerializedName("frequency_data")
//            private List<String> frequency_data;

            @SerializedName("start_date")
            private String start_date;

            @SerializedName("end_date")
            private String end_date;

            @SerializedName("status")
            private String status;

            @SerializedName("deadline")
            private String deadline;

            @SerializedName("is_close")
            private int is_close;

            @SerializedName("close_date")
            private String close_date;

            @SerializedName("created_at")
            private String created_at;

            @SerializedName("updated_at")
            private String updated_at;

            @SerializedName("case_number")
            private String case_number;

            @SerializedName("is_complete")
            private int is_complete;

            @SerializedName("is_accept")
            private int is_accept;

            @SerializedName("client")
            private ClientData clientData;

            @SerializedName("checksheet")
            private ChecksheetData checksheetData;

            @SerializedName("assign")
            private AssignData assignData;


            public TasksList(){

            }

            public TasksList(int id,String name,int company_id,int client_id,
                             int check_sheet_id,String frequency,String start_date,
                             int is_accept, String end_date,String status,String created_at,String updated_at,
                             int is_complete,
                             ClientData clientData,ChecksheetData checksheetData, AssignData assignData,int is_close) {
                this.id = id;
                this.name = name;
                this.company_id = company_id;
                this.client_id = client_id;
                this.check_sheet_id = check_sheet_id;
                this.frequency = frequency;
//                this.frequency_data = frequency_data;
                this.start_date = start_date;
                this.end_date = end_date;
                this.status = status;
                this.created_at = created_at;
                this.updated_at = updated_at;
                this.is_complete = is_complete;
                this.is_accept = is_accept;
                this.clientData = clientData;
                this.checksheetData = checksheetData;
                this.assignData = assignData;
                this.is_close = is_close;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getCompany_id() {
                return company_id;
            }

            public void setCompany_id(int company_id) {
                this.company_id = company_id;
            }

            public int getClient_id() {
                return client_id;
            }

            public void setClient_id(int client_id) {
                this.client_id = client_id;
            }

            public int getCheck_sheet_id() {
                return check_sheet_id;
            }

            public void setCheck_sheet_id(int check_sheet_id) {
                this.check_sheet_id = check_sheet_id;
            }

            public String getFrequency() {
                return frequency;
            }

            public void setFrequency(String frequency) {
                this.frequency = frequency;
            }

            public String getStart_date() {
                return start_date;
            }

            public void setStart_date(String start_date) {
                this.start_date = start_date;
            }

            public String getEnd_date() {
                return end_date;
            }

            public void setEnd_date(String end_date) {
                this.end_date = end_date;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public ClientData getClientData() {
                return clientData;
            }

            public void setClientData(ClientData clientData) {
                this.clientData = clientData;
            }

            public ChecksheetData getChecksheetData() {
                return checksheetData;
            }

            public void setChecksheetData(ChecksheetData checksheetData) {
                this.checksheetData = checksheetData;
            }

            public int getIs_complete() {
                return is_complete;
            }

            public void setIs_complete(int is_complete) {
                this.is_complete = is_complete;
            }

            public int getIs_accept() {
                return is_accept;
            }

            public void setIs_accept(int is_accept) {
                this.is_accept = is_accept;
            }

            public AssignData getAssignData() {
                return assignData;
            }

            public void setAssignData(AssignData assignData) {
                this.assignData = assignData;
            }

//            public List<String> getFrequency_data() {
//                return frequency_data;
//            }
//
//            public void setFrequency_data(List<String> frequency_data) {
//                this.frequency_data = frequency_data;
//            }

            public int getIs_close() {
                return is_close;
            }

            public void setIs_close(int is_close) {
                this.is_close = is_close;
            }

            public String getClose_date() {
                return close_date;
            }

            public void setClose_date(String close_date) {
                this.close_date = close_date;
            }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getCase_number() {
        return case_number;
    }

    public void setCase_number(String case_number) {
        this.case_number = case_number;
    }
}
