package eu.spottless.userit.responseModel.loginResponse;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Task extends RealmObject {
    @SerializedName("complete_task")
    private int complete_task;
    @SerializedName("pending_task")
    private int pending_task;
    @SerializedName("upcomming_task")
    private int upcomming_task;
    @SerializedName("overdue_task")
    private int overdue_task;
    @SerializedName("decline_task")
    private int decline_task;

    public int getComplete_task() {
        return complete_task;
    }

    public void setComplete_task(int complete_task) {
        this.complete_task = complete_task;
    }

    public int getPending_task() {
        return pending_task;
    }

    public void setPending_task(int pending_task) {
        this.pending_task = pending_task;
    }

    public int getUpcomming_task() {
        return upcomming_task;
    }

    public void setUpcomming_task(int upcomming_task) {
        this.upcomming_task = upcomming_task;
    }

    public int getOverdue_task() {
        return overdue_task;
    }

    public void setOverdue_task(int overdue_task) {
        this.overdue_task = overdue_task;
    }

    public int getDecline_task() {
        return decline_task;
    }

    public void setDecline_task(int decline_task) {
        this.decline_task = decline_task;
    }
}