package eu.spottless.userit.responseModel;

import com.google.gson.annotations.SerializedName;

public class ProfileResponse {

    @SerializedName("company_id")
    private int company_id;
    @SerializedName("id")
    private int id;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private ProfileData profile_data;
    @SerializedName("status")
    private String status;

    public ProfileResponse(int company_id, int id, String message, ProfileData profile_data, String status) {

        this.company_id = company_id;
        this.id = id;
        this.message = message;
        this.profile_data = profile_data;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProfileData getProfile_data() {
        return profile_data;
    }

    public void setProfile_data(ProfileData profile_data) {
        this.profile_data = profile_data;
    }


    public class ProfileData{
        @SerializedName("id")
        private int id;
        @SerializedName("name")
        private String name;
        @SerializedName("userid")
        private String userid;
        @SerializedName("email")
        private String email;
        @SerializedName("phone")
        private String phone;
        @SerializedName("department")
        private String department;
        @SerializedName("designation")
        private String designation;
        @SerializedName("company_id")
        private int company_id;
        @SerializedName("user_type")
        private String user_type;
        @SerializedName("status")
        private String status;
        @SerializedName("avatar")
        private String avatar;
        @SerializedName("avatar_url")
        private String avatar_url;
        @SerializedName("department_name")
        private String department_name;
        @SerializedName("designation_name")
        private String designation_name;
        @SerializedName("company_name")
        private String company_name;
        @SerializedName("task_count")
        private TaskCount task_count;

        public ProfileData(int id,String name,String userid,String email,String phone,
                               String department,String designation,int company_id,
                               String user_type,String status,String avatar,String avatar_url,String department_name,
                               String designation_name,String company_name,TaskCount task_count) {
            this.id = id;
            this.name = name;
            this.userid = userid;
            this.email = email;
            this.phone = phone;
            this.department = department;
            this.designation = designation;
            this.company_id = company_id;
            this.user_type = user_type;
            this.status = status;
            this.avatar_url = avatar_url;
            this.department_name = department_name;
            this.designation_name = designation_name;
            this.company_name = company_name;
            this.avatar = avatar;
            this.task_count = task_count;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public int getCompany_id() {
            return company_id;
        }

        public void setCompany_id(int company_id) {
            this.company_id = company_id;
        }

        public String getUser_type() {
            return user_type;
        }

        public void setUser_type(String user_type) {
            this.user_type = user_type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAvatar_url() {
            return avatar_url;
        }

        public void setAvatar_url(String avatar_url) {
            this.avatar_url = avatar_url;
        }

        public String getDepartment_name() {
            return department_name;
        }

        public void setDepartment_name(String department_name) {
            this.department_name = department_name;
        }

        public String getDesignation_name() {
            return designation_name;
        }

        public void setDesignation_name(String designation_name) {
            this.designation_name = designation_name;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public TaskCount getTask_count() {
            return task_count;
        }

        public void setTask_count(TaskCount task_count) {
            this.task_count = task_count;
        }

        public class TaskCount {
            @SerializedName("complete_task")
            private String complete_task;
            @SerializedName("pending_task")
            private String pending_task;
            @SerializedName("upcomming_task")
            private String upcomming_task;
            @SerializedName("overdue_task")
            private String overdue_task;

            public String getComplete_task() {
                return complete_task;
            }

            public void setComplete_task(String complete_task) {
                this.complete_task = complete_task;
            }

            public String getPending_task() {
                return pending_task;
            }

            public void setPending_task(String pending_task) {
                this.pending_task = pending_task;
            }

            public String getUpcomming_task() {
                return upcomming_task;
            }

            public void setUpcomming_task(String upcomming_task) {
                this.upcomming_task = upcomming_task;
            }

            public String getOverdue_task() {
                return overdue_task;
            }

            public void setOverdue_task(String overdue_task) {
                this.overdue_task = overdue_task;
            }
        }
    }
}

