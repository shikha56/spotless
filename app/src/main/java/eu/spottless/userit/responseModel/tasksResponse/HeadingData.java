package eu.spottless.userit.responseModel.tasksResponse;

import com.google.gson.annotations.SerializedName;



import java.io.Serializable;
import java.util.List;


//@Parcel
public class HeadingData implements Serializable {
//    @PrimaryKey
//    @Required
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("tool")
    private String tool;
    @SerializedName("check_sheet_id")
    private int check_sheet_id;
    @SerializedName("status")
    private String status;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;
    @SerializedName("is_head_done")
    private String is_head_done;

//    @ParcelPropertyConverter(ChecklistParcelConverter.class)
    @SerializedName("checklist")
    private List<ChecklistData> checklistData;

    public HeadingData(){

    }

    HeadingData(int id, String name, String tool, int check_sheet_id, String status,
                String created_at, String updated_at,String is_head_done, List<ChecklistData> checklistData) {
        this.id = id;
        this.name = name;
        this.tool = tool;
        this.check_sheet_id = check_sheet_id;
        this.status = status;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.is_head_done = is_head_done;
        this.checklistData = checklistData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTool() {
        return tool;
    }

    public void setTool(String tool) {
        this.tool = tool;
    }

    public int getCheck_sheet_id() {
        return check_sheet_id;
    }

    public void setCheck_sheet_id(int check_sheet_id) {
        this.check_sheet_id = check_sheet_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public List<ChecklistData> getChecklistData() {
        return checklistData;
    }

    public void setChecklistData(List<ChecklistData> checklistData) {
        this.checklistData = checklistData;
    }


    public String getIs_head_done() {
        return is_head_done;
    }

    public void setIs_head_done(String is_head_done) {
        this.is_head_done = is_head_done;
    }
}
