package eu.spottless.userit.responseModel.tasksResponse;

import com.google.gson.annotations.SerializedName;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//@Parcel
public class ChecksheetData implements Serializable {
//    @PrimaryKey
//    @Required
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("checklist_no")
    private String checklist_no;
    @SerializedName("comment")
    private String comment;
    @SerializedName("company_id")
    private int company_id;
    @SerializedName("industry_id")
    private int industry_id;
    @SerializedName("checksheet_type")
    private int checksheet_type;
    @SerializedName("client_id")
    private int client_id;
    @SerializedName("client_frequency")
    private String client_frequency;
    @SerializedName("days")
    private String days;
    @SerializedName("start_date")
    private String start_date;
    @SerializedName("end_date")
    private String end_date;
    @SerializedName("status")
    private String status;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;

    @SerializedName("heading")
    private List<HeadingData> headingData;


    public ChecksheetData(){

    }


    public ChecksheetData(int id,String name,String checklist_no,String comment,int company_id,int industry_id,
                          int checksheet_type,int client_id,String client_frequency,String days,String start_date,
                          String end_date,String status,String created_at,String updated_at,List<HeadingData> headingData) {
        this.id = id;
        this.name = name;
        this.checklist_no = checklist_no;
        this.comment = comment;
        this.company_id = company_id;
        this.industry_id = industry_id;
        this.checksheet_type = checksheet_type;
        this.client_id = client_id;
        this.client_frequency = client_frequency;
        this.days = days;
        this.start_date = start_date;
        this.end_date = end_date;
        this.status = status;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.headingData = headingData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChecklist_no() {
        return checklist_no;
    }

    public void setChecklist_no(String checklist_no) {
        this.checklist_no = checklist_no;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public int getIndustry_id() {
        return industry_id;
    }

    public void setIndustry_id(int industry_id) {
        this.industry_id = industry_id;
    }

    public int getChecksheet_type() {
        return checksheet_type;
    }

    public void setChecksheet_type(int checksheet_type) {
        this.checksheet_type = checksheet_type;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public String getClient_frequency() {
        return client_frequency;
    }

    public void setClient_frequency(String client_frequency) {
        this.client_frequency = client_frequency;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public List<HeadingData> getHeadingData() {
        return headingData;
    }

    public void setHeadingData(ArrayList<HeadingData> headingData) {
        this.headingData = headingData;
    }


}
