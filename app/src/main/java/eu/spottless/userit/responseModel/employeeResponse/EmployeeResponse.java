package eu.spottless.userit.responseModel.employeeResponse;

import com.google.gson.annotations.SerializedName;

public class EmployeeResponse{

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private EmployeeListData employeeListData;

    public EmployeeResponse(){

    }

    public EmployeeResponse(String status, String message, EmployeeListData employeeListData) {

        this.status = status;
        this.message = message;
        this.employeeListData = employeeListData;

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public EmployeeListData getEmployeeListData() {
        return employeeListData;
    }

    public void setEmployeeListData(EmployeeListData employeeListData) {
        this.employeeListData = employeeListData;
    }

 }



