package eu.spottless.userit.responseModel.tasksResponse;

import com.google.gson.annotations.SerializedName;


import java.io.Serializable;
import java.util.List;


public class ChecklistData implements Serializable {
//    @PrimaryKey
//    @Required
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("frequency")
    private String frequency;
    @SerializedName("check_list_heading_id")
    private int check_list_heading_id;
    @SerializedName("status")
    private String status;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;
    @SerializedName("is_done")
    private int is_done;
    @SerializedName("is_checklist_validate")
    private int is_checklist_validate;
//    @ParcelPropertyConverter(RemarkParcelConverter.class)
    @SerializedName("remarks")
    private List<RemarksData> remarksData;


    public ChecklistData(){

    }

    ChecklistData(int id, String name, String frequency, int check_list_heading_id, String status,
                  String created_at, String updated_at, int is_done) {
        this.id = id;
        this.name = name;
        this.frequency = frequency;
        this.check_list_heading_id = check_list_heading_id;
        this.status = status;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.is_done = is_done;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public int getCheck_list_heading_id() {
        return check_list_heading_id;
    }

    public void setCheck_list_heading_id(int check_list_heading_id) {
        this.check_list_heading_id = check_list_heading_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getIs_done() {
        return is_done;
    }

    public void setIs_done(int is_done) {
        this.is_done = is_done;
    }

    public List<RemarksData> getRemarksData() {
        return remarksData;
    }

    public void setRemarksData(List<RemarksData> remarksData) {
        this.remarksData = remarksData;
    }


    public int getIs_checklist_validate() {
        return is_checklist_validate;
    }

    public void setIs_checklist_validate(int is_checklist_validate) {
        this.is_checklist_validate = is_checklist_validate;
    }
}
