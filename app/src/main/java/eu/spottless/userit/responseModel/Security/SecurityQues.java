package eu.spottless.userit.responseModel.Security;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import eu.spottless.userit.responseModel.countResponse.Data;

public class SecurityQues {

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<QuesData> quesData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public List<QuesData> getQuesData() {
        return quesData;
    }

    public void setQuesData(List<QuesData> quesData) {
        this.quesData = quesData;
    }
}
