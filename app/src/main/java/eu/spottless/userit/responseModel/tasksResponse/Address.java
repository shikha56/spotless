package eu.spottless.userit.responseModel.tasksResponse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Address implements Serializable {
//    @PrimaryKey
//    @Required
    @SerializedName("id")
    private int id;
    @SerializedName("client_id")
    private int client_id;
    @SerializedName("address")
    private String address;
    @SerializedName("postal_code")
    private String postal_code;
    @SerializedName("postal_district")
    private String postal_district;
    @SerializedName("city")
    private String city;
    @SerializedName("landmark")
    private String landmark;
    @SerializedName("lat")
    private String lat;
    @SerializedName("long")
    private String longitude;
    @SerializedName("status")
    private String status;
    @SerializedName("is_primary")
    private int is_primary;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;
    @SerializedName("range")
    private String range;

    public Address(){

    }

    public Address(int id,int client_id,String address,String postal_code,String postal_district,
                   String city,String landmark,String lat,String longitude,String status,int is_primary,
                   String created_at,String updated_at, String range) {
        this.id = id;
        this.client_id = client_id;
        this.address = address;
        this.postal_code = postal_code;
        this.postal_district = postal_district;
        this.city = city;
        this.landmark = landmark;
        this.lat = lat;
        this.longitude = longitude;
        this.status = status;
        this.is_primary = is_primary;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.range = range;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getPostal_district() {
        return postal_district;
    }

    public void setPostal_district(String postal_district) {
        this.postal_district = postal_district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIs_primary() {
        return is_primary;
    }

    public void setIs_primary(int is_primary) {
        this.is_primary = is_primary;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }
}
