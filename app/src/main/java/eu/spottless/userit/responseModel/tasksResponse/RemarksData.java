package eu.spottless.userit.responseModel.tasksResponse;

import com.google.gson.annotations.SerializedName;


import java.io.Serializable;
import java.util.List;


public class RemarksData implements Serializable {
//    @PrimaryKey
//    @Required
    @SerializedName("id")
    private int id;
    @SerializedName("parent")
    private String parent;
    @SerializedName("task_id")
    private int task_id;
    @SerializedName("check_sheet_id")
    private long check_sheet_id;
    @SerializedName("check_sheet_list_id")
    private int check_sheet_list_id;
    @SerializedName("is_done")
    private int is_done;
    @SerializedName("is_validate")
    private int is_validate;
    @SerializedName("user_type")
    private String user_type;
    @SerializedName("update_by")
    private int update_by;
    @SerializedName("img")
    private String img;
    @SerializedName("remark")
    private String remark;
    @SerializedName("status")
    private String status;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;
    @SerializedName("user_name")
    private String user_name;

//    @ParcelPropertyConverter(ReplyParcelConverter.class)
    @SerializedName("reply")
    private List<ReplyData> reply;

    public RemarksData(){

    }

    RemarksData(int id, int task_id, long check_sheet_id, int check_sheet_list_id, int is_done,
                String user_type,String parent, String user_name, int update_by, String img,
                String remark, String created_at, String updated_at, String status,List<ReplyData> reply) {
        this.id = id;
        this.parent = parent;
        this.task_id = task_id;
        this.check_sheet_id = check_sheet_id;
        this.check_sheet_list_id = check_sheet_list_id;
        this.is_done = is_done;
        this.user_type = user_type;
        this.update_by = update_by;
        this.img = img;
        this.remark = remark;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.status = status;
        this.user_name = user_name;
        this.reply = reply;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTask_id() {
        return task_id;
    }

    public void setTask_id(int task_id) {
        this.task_id = task_id;
    }

    public long getCheck_sheet_id() {
        return check_sheet_id;
    }

    public void setCheck_sheet_id(long check_sheet_id) {
        this.check_sheet_id = check_sheet_id;
    }

    public int getCheck_sheet_list_id() {
        return check_sheet_list_id;
    }

    public void setCheck_sheet_list_id(int check_sheet_list_id) {
        this.check_sheet_list_id = check_sheet_list_id;
    }

    public int getIs_done() {
        return is_done;
    }

    public void setIs_done(int is_done) {
        this.is_done = is_done;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public int getUpdate_by() {
        return update_by;
    }

    public void setUpdate_by(int update_by) {
        this.update_by = update_by;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public List<ReplyData> getReply() {
        return reply;
    }

    public void setReply(List<ReplyData> reply) {
        this.reply = reply;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }


    public int getIs_validate() {
        return is_validate;
    }

    public void setIs_validate(int is_validate) {
        this.is_validate = is_validate;
    }
}
