package eu.spottless.userit.responseModel;

import com.google.gson.annotations.SerializedName;

public class ErrorRes {
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("errors")
    private Errors errors;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Errors getErrors() {
        return errors;
    }

    public void setErrors(Errors errors) {
        this.errors = errors;
    }

    public class Errors {
        @SerializedName("password")
        private String password;

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}
