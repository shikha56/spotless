package eu.spottless.userit.responseModel.tasksResponse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class ClientData implements Serializable {
//    @PrimaryKey
//    @Required
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("company_id")
    private int company_id;
    @SerializedName("email_verified_at")
    private String email_verified_at;
    @SerializedName("mobile_verified_at")
    private String mobile_verified_at;
    @SerializedName("password")
    private String password;
    @SerializedName("status")
    private String status;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("device_id")
    private String device_id;
    @SerializedName("range")
    private String range;
    @SerializedName("device_token")
    private String device_token;
    @SerializedName("device_type")
    private String device_type;
    @SerializedName("remember_token")
    private String remember_token;
    @SerializedName("force_change_password")
    private int force_change_password;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;
    @SerializedName("cvr")
    private String cvr;
    @SerializedName("avatar_url")
    private String avatar_url;

    @SerializedName("address")
    private Address address = new Address();


    public ClientData(){

    }

    public ClientData(int id,String name,String username,String email,
                      String phone,int company_id,String email_verified_at,String mobile_verified_at,
                      String password,String status,String avatar,String remember_token,String device_id,
                      String range,String device_token,String device_type,int force_change_password,
                      String created_at,String updated_at,String cvr,String avatar_url,Address address) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.company_id = company_id;
        this.email_verified_at = email_verified_at;
        this.mobile_verified_at = mobile_verified_at;
        this.password = password;
        this.status = status;
        this.avatar = avatar;
        this.device_id = device_id;
        this.range = range;
        this.device_token = device_token;
        this.device_type = device_type;
        this.remember_token = remember_token;
        this.force_change_password = force_change_password;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.cvr = cvr;
        this.avatar_url = avatar_url;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getEmail_verified_at() {
        return email_verified_at;
    }

    public void setEmail_verified_at(String email_verified_at) {
        this.email_verified_at = email_verified_at;
    }

    public String getMobile_verified_at() {
        return mobile_verified_at;
    }

    public void setMobile_verified_at(String mobile_verified_at) {
        this.mobile_verified_at = mobile_verified_at;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public int getForce_change_password() {
        return force_change_password;
    }

    public void setForce_change_password(int force_change_password) {
        this.force_change_password = force_change_password;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCvr() {
        return cvr;
    }

    public void setCvr(String cvr) {
        this.cvr = cvr;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

}
