package eu.spottless.userit.responseModel.employeeResponse;

import com.google.gson.annotations.SerializedName;

public class TaskCount{
    @SerializedName("complete_task")
    private String complete_task;
    @SerializedName("pending_task")
    private String pending_task;
    @SerializedName("upcomming_task")
    private String upcomming_task;
    @SerializedName("overdue_task")
    private String overdue_task;

    public TaskCount(){

    }

    public String getComplete_task() {
        return complete_task;
    }

    public void setComplete_task(String complete_task) {
        this.complete_task = complete_task;
    }

    public String getPending_task() {
        return pending_task;
    }

    public void setPending_task(String pending_task) {
        this.pending_task = pending_task;
    }

    public String getUpcomming_task() {
        return upcomming_task;
    }

    public void setUpcomming_task(String upcomming_task) {
        this.upcomming_task = upcomming_task;
    }

    public String getOverdue_task() {
        return overdue_task;
    }

    public void setOverdue_task(String overdue_task) {
        this.overdue_task = overdue_task;
    }
}
