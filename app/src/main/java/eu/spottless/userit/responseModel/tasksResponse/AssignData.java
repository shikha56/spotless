package eu.spottless.userit.responseModel.tasksResponse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class AssignData implements Serializable {
//    @PrimaryKey
//    @Required
    @SerializedName("id")
    private int id;
    @SerializedName("employee_id")
    private String employee_id;
    @SerializedName("task_id")
    private String task_id;
    @SerializedName("status")
    private String status;
    @SerializedName("is_accept")
    private String is_accept;
    @SerializedName("reason")
    private String reason;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;


    public AssignData(){

    }

    AssignData(int id, String employee_id,String task_id,String status,
               String is_accept, String reason, String created_at, String updated_at) {
        this.id = id;
        this.employee_id = employee_id;
        this.task_id = task_id;
        this.status = status;
        this.is_accept = is_accept;
        this.reason = reason;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIs_accept() {
        return is_accept;
    }

    public void setIs_accept(String is_accept) {
        this.is_accept = is_accept;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

}
