package eu.spottless.userit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import eu.spottless.userit.R;
import eu.spottless.userit.responseModel.notificationResponse.NotificationList;
import io.realm.RealmList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>{

    private int rowLayout;
    private RealmList<NotificationList> notificationLists = new RealmList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder{
        final TextView txt_title;
        final TextView txt_time;
        final TextView txt_body;


        MyViewHolder(View v) {
            super(v);
            txt_title = v.findViewById(R.id.txt_title);
            txt_time = v.findViewById(R.id.txt_time);
            txt_body = v.findViewById(R.id.txt_body);

        }
    }

    public NotificationAdapter(Context context) {
        notificationLists = new RealmList<>();
        context = context;
    }

    public NotificationAdapter(RealmList<NotificationList> notificationList, int rowLayout, Context context) {
        notificationLists = new RealmList<>();
        this.notificationLists = notificationList;
        this.rowLayout = rowLayout;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.txt_title.setText(notificationLists.get(position).getTitle().substring(0, 1).toUpperCase()+ notificationLists.get(position).getTitle().substring(1));
        holder.txt_body.setText(notificationLists.get(position).getBody().substring(0, 1).toUpperCase()+ notificationLists.get(position).getBody().substring(1));

        Date oneWayTripDate = null;
        String str = null;

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy HH:mm aa", Locale.US);
        try {
            oneWayTripDate = df.parse(notificationLists.get(position).getCreated_at());
            str = outputFormat.format(oneWayTripDate);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        holder.txt_time.setText(str);
    }

    @Override
    public int getItemCount() {
        return notificationLists.size();
    }

    public void setItems(RealmList<NotificationList> notificationList){
        notificationLists = notificationList;
        notifyDataSetChanged();
    }

    public void addItems(RealmList<NotificationList> notificationList){
        notificationLists.clear();
        notificationLists.addAll(notificationList);
        notifyDataSetChanged();
    }
}
