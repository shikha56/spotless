package eu.spottless.userit.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import eu.spottless.userit.R;
import eu.spottless.userit.activities.TaskDetailsActivity;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.fragments.DetailFragment;
import eu.spottless.userit.responseModel.tasksResponse.ChecklistData;
import eu.spottless.userit.responseModel.tasksResponse.RemarksData;

public class AyalmaExpandAdapter extends RecyclerView.Adapter<AyalmaExpandAdapter.ViewHolder> {
    private static final int UNSELECTED = -1;
    private RecyclerView recyclerView;
    private int selectedItem = UNSELECTED;

    private Context context;
    private List<ChecklistData> checklistData;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    ArrayList<Bitmap> bitmaps;
    private static Uri bitmap;
    static String encodedImage,encodedImages;
    private boolean rangeStatus;
    int checkedId,isdone;
    int isCompleted,isClose;
    static int selectedPos,adapterposition,checklist_id;
    ArrayList<Uri> uris;
    static Bitmap bitmapImage;
    String currentPhotoPath, task_name;
    RecyclerView.SmoothScroller smoothScroller;

    public AyalmaExpandAdapter(ArrayList<Uri> uris,RecyclerView recyclerView, List<ChecklistData> checklistData, int isCompleted, int isClose, String task_name,Context context) {
        this.recyclerView = recyclerView;
        this.checklistData = checklistData;
        this.isCompleted = isCompleted;
        this.isClose = isClose;
        this.context = context;
        this.uris = uris;
        this.task_name = task_name;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.parent_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return checklistData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, ExpandableLayout.OnExpansionUpdateListener {
        private ExpandableLayout expandableLayout;
        private TextView txt_taskname;
        private LinearLayout linearLayout;
        private ImageView img_up, img_down;
        private CardView cardview;
        private CheckBox checkbox;

        private RecyclerView child_recyclerview;
        private LinearLayout linear_updateStatus;
        private ImageView img_remark;
        private EditText edit_comment;
        String role;
        int isDone,is_Done,is_checklist_validate;


        public ViewHolder(View itemView) {
            super(itemView);

            txt_taskname = itemView.findViewById(R.id.txt_taskname);
            linearLayout = itemView.findViewById(R.id.linearLayout);
            cardview = itemView.findViewById(R.id.cardview);
            checkbox = itemView.findViewById(R.id.checkbox);
            img_up = itemView.findViewById(R.id.img_up);
            img_down = itemView.findViewById(R.id.img_down);
            img_down.setVisibility(View.VISIBLE);
            img_up.setVisibility(View.GONE);

            role = SharedPreferencesClass.retriveRole(context,"role");
            child_recyclerview = itemView.findViewById(R.id.child_recyclerview);
            child_recyclerview.setLayoutManager(new LinearLayoutManager(context));
            img_remark = itemView.findViewById(R.id.img_remark);
            edit_comment = itemView.findViewById(R.id.edit_comment);
            linear_updateStatus = itemView.findViewById(R.id.linear_updateStatus);

            expandableLayout = itemView.findViewById(R.id.expandable_layout);
            expandableLayout.setInterpolator(new OvershootInterpolator());
            expandableLayout.setOnExpansionUpdateListener(this);

            linearLayout.setOnClickListener(this);
            img_remark.setOnClickListener(this);

            if(task_name.equals("only_view")){
                linear_updateStatus.setVisibility(View.GONE);
            }
            else {
                linear_updateStatus.setOnClickListener(this);
            }

            if(role.equals("employee")){
                if(task_name.equals("only_view")){
                    checkbox.setEnabled(false);
                }
                else {
                    checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                        Log.e("super boolean b-----", String.valueOf(b));
                            if (!b) {
                                checkedId = checklistData.get(ViewHolder.this.getAdapterPosition()).getId();
                                isdone = 0;
//                            Log.e("super if isdone-----", String.valueOf(isdone));
                            } else {

                                checkedId = checklistData.get(getAdapterPosition()).getId();
                                Log.e("super else isdone-----", String.valueOf(isdone));
                                if (checklistData.get(getAdapterPosition()).getIs_done() == 1) {
                                    isdone = 0;
//                                Log.e("employee if isdone-----", String.valueOf(isdone));
                                } else {
//                                isdone = 1;
//                                if (edit_comment.getText().toString().equals("")) {
//                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//                                    alertDialogBuilder.setTitle(context.getText(R.string.checkbox_error));
//                                    alertDialogBuilder
//                                            .setCancelable(false)
//                                            .setPositiveButton("OK",
//                                                    new DialogInterface.OnClickListener() {
//                                                        @Override
//                                                        public void onClick(DialogInterface dialog, int id) {
//                                                            dialog.cancel();
//                                                            checkbox.setChecked(false);
//                                                        }
//                                                    });
//                                    AlertDialog alertDialog = alertDialogBuilder.create();
//                                    alertDialog.show();
//                                }
//                                else {
                                    isdone = 1;
//                                    Log.e("employee else isdone---", String.valueOf(isdone));
//                                }
                                }
//
                            }
                        }
                    });
                }
            }
            else if(role.equals("supervisor")){
                if(task_name.equals("declined")){
                    checkbox.setEnabled(false);
                }
                else {
                    checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (!b) {
                                checkedId = checklistData.get(ViewHolder.this.getAdapterPosition()).getId();
//                            Log.e("super if checkedId-----", String.valueOf(checkedId));
                                isdone = 0;
                            } else {
                                checkedId = checklistData.get(getAdapterPosition()).getId();
//                            Log.e("super else checkedId---", String.valueOf(checkedId));
                                if (checklistData.get(getAdapterPosition()).getIs_done() == 1) {
                                    isdone = 1;
                                } else {
                                    isdone = 1;
                                }
//
                            }
                        }
                    });
                }
            }
        }

        public void bind() {
//            Log.d("bind()-------", String.valueOf(getAdapterPosition()));
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;
            img_down.setVisibility(View.VISIBLE);
            img_up.setVisibility(View.GONE);

            if(role.equals("employee")){
                isDone = checklistData.get(position).getIs_done();
//                Log.d("isDone-------", String.valueOf(isDone));
                if(task_name.equals("only_view")){
                    checkbox.setClickable(false);
                    checkbox.setEnabled(false);
                }
                else {
                    if (isDone == 1 || isCompleted == 1) {
                        checkbox.setClickable(false);
                        checkbox.setChecked(true);
                    } else {
                        checkbox.setChecked(false);
                    }
                }
            }
            else if(role.equals("supervisor")){
                isDone = checklistData.get(position).getIs_done();
                is_checklist_validate = checklistData.get(position).getIs_checklist_validate();
//                Log.d("isDone-------", String.valueOf(isDone));
                if(task_name.equals("declined")){
                    checkbox.setClickable(false);
                    checkbox.setEnabled(false);
                }
                else {
                    if(isDone == 1 || isCompleted == 1){
                        if(isClose == 1 || is_checklist_validate == 1){
                            checkbox.setClickable(false);
                            checkbox.setChecked(true);
                        }
                        else {
                            is_Done = 1;
                            checkbox.setChecked(true);
                        }

                    }else {
                        if(is_checklist_validate == 1){
                            checkbox.setClickable(false);
                            checkbox.setChecked(true);
                        }
                        else {
                            is_Done = 0;
                            checkbox.setChecked(false);
                        }
                    }
                }
            }
            txt_taskname.setText(checklistData.get(position).getName().substring(0, 1).toUpperCase()+ checklistData.get(position).getName().substring(1));

            if(adapterposition == position) {
//                linearLayout.setSelected(isSelected);
                rangeStatus = SharedPreferencesClass.retriveRangeStatus(context,"rangeStatus");
                Log.e("rangeStatus---", String.valueOf(rangeStatus));
                expandableLayout.setExpanded(isSelected, true);
                expandableLayout.expand();

                if(bitmap == null || encodedImage == null || bitmapImage == null){
                    encodedImage = null;
                }
                else {
                    if(checklist_id == checklistData.get(position).getId()) {
                        encodedImages = encodedImage;
                        Picasso.with(context)
                                .load(bitmap)
                                .error(R.drawable.ic_camera)
                                .placeholder(R.drawable.ic_camera)
                                .resize(600, 600)
                                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                                .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                                .into(img_remark);
                    }
                    else {
                        encodedImages = null;
                        img_remark.setImageResource(R.drawable.ic_camera);
                    }
                }
                List<RemarksData> remarksDataList = checklistData.get(position).getRemarksData();
                isDone = checklistData.get(position).getIs_done();

                if(role.equals("employee")){
                    if(isDone == 1){
                        cardview.setVisibility(View.GONE);
                    }
                    else {
                        cardview.setVisibility(View.VISIBLE);
                    }
                }
                else if(role.equals("supervisor")){
                    cardview.setVisibility(View.GONE);
                }

                RecycleViewAdapter recycleViewAdapter = new RecycleViewAdapter(remarksDataList, checkedId, isDone, isdone, isCompleted, isClose, R.layout.remark_list, task_name, context);
                child_recyclerview.setAdapter(recycleViewAdapter);
                selectedItem = position;

            }
            else{
                linearLayout.setSelected(isSelected);
                expandableLayout.setExpanded(isSelected, false);
                expandableLayout.collapse();
            }
//            linearLayout.setSelected(isSelected);
//            expandableLayout.setExpanded(isSelected, true);
//            expandableLayout.expand();
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            int position = getAdapterPosition();
            switch (id) {
                case R.id.linearLayout:
                    ViewHolder holder = (ViewHolder) recyclerView.findViewHolderForAdapterPosition(selectedItem);
                    if (holder != null) {
//                        Log.d("if holder-------", String.valueOf(selectedItem));
                        holder.linearLayout.setSelected(false);
                        holder.expandableLayout.collapse();
                    }
                    if (position == selectedItem) {
//                        Log.d("if selectedItem-------", String.valueOf(position));
                        img_down.setVisibility(View.VISIBLE);
                        img_up.setVisibility(View.GONE);
                        selectedItem = UNSELECTED;
                    } else {
//                        Log.d("else selectedItem----", String.valueOf(position));
                        selectedPos = position;
                        adapterposition = selectedPos;
//                        checklist_id = checklistData.get(position).getId();

                        rangeStatus = SharedPreferencesClass.retriveRangeStatus(context,"rangeStatus");
//                        Log.e("rangeStatus---", String.valueOf(rangeStatus));
                        img_down.setVisibility(View.GONE);
                        img_up.setVisibility(View.VISIBLE);
                        linearLayout.setSelected(true);
                        expandableLayout.expand();

                        if(bitmap == null || encodedImage == null || bitmapImage == null){
                          encodedImage = null;
                        }
                        else {
                            if(checklist_id == checklistData.get(position).getId()) {
                                encodedImages = encodedImage;
                                Picasso.with(context)
                                        .load(bitmap)
                                        .error(R.drawable.ic_camera)
                                        .placeholder(R.drawable.ic_camera)
                                        .resize(600, 600)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                                        .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                                        .into(img_remark);
                            }
                            else {
                                encodedImages = null;
                                img_remark.setImageResource(R.drawable.ic_camera);
                            }
                        }
                        List<RemarksData> remarksDataList = checklistData.get(position).getRemarksData();
                        isDone = checklistData.get(position).getIs_done();

                        if(role.equals("employee")){
                            if(isDone == 1){
                                cardview.setVisibility(View.GONE);
                            }
                            else {
                                cardview.setVisibility(View.VISIBLE);
                            }
                        }
                        else if(role.equals("supervisor")){
                            cardview.setVisibility(View.GONE);
                        }

                        RecycleViewAdapter recycleViewAdapter = new RecycleViewAdapter(remarksDataList, checkedId, isDone, isdone, isCompleted, isClose, R.layout.remark_list, task_name, context);
                        child_recyclerview.setAdapter(recycleViewAdapter);
                        selectedItem = position;
                    }
                    break;

                case R.id.img_remark:
                    selectedPos = position;
                    adapterposition = selectedPos;
                    checklist_id = checklistData.get(position).getId();
//                    DetailFragment.imageClick(context,adapterposition);
                    checkRunTimePermission();
                    break;

                case R.id.linear_updateStatus:
                    if(role.equals("employee")){

                        if(rangeStatus) {
                            if (adapterposition == position) {
                                String comment = edit_comment.getText().toString();
                                int remarkId = 0;
//                                if (comment == null || comment.trim().length() == 0) {
//                                    Toast.makeText(context, context.getText(R.string.label_comment), Toast.LENGTH_SHORT).show();
//                                }
//                                else if (encodedImage == null || encodedImage.trim().length() == 0
//                                        || encodedImages == null || encodedImages.trim().length() == 0) {
////                                    Toast.makeText(context, context.getText(R.string.label_image), Toast.LENGTH_SHORT).show();
//                                    encodedImage = "";
//                                }
//                                else if(!checkbox.isChecked()){
//                                    Toast.makeText(context, "Please checked the checkbox", Toast.LENGTH_SHORT).show();
//                                }
//                                else {

//                                    Log.e("id---", checklistData.get(adapterposition).getId()
//                                            + "\nisdone---" + isdone
//                                            + "\ncomment--" + comment + "\nremarkId--" + remarkId + "\nencode--" + encodedImages == null ? "" : encodedImages);
                                    if (encodedImage == null) {
                                        DetailFragment.onUpdateClick(context, checklistData.get(position).getId(),
                                                isdone, "", comment, "", 0);
                                    }
                                    else {
                                        DetailFragment.onUpdateClick(context, checklistData.get(position).getId(),
                                                isdone, encodedImages, comment, "", 0);
                                    }
//                                    DetailFragment.onUpdateClick(context, checklistData.get(position).getId(),
//                                            isdone, encodedImages, comment, "", 0);

//                                }
                            }
                            else {
                                String comment = edit_comment.getText().toString();
                                int remarkId = 0;
//                                if (comment == null || comment.trim().length() == 0) {
//                                    Toast.makeText(context,  context.getText(R.string.label_comment), Toast.LENGTH_SHORT).show();
//                                }
//                                else if (encodedImage == null || encodedImage.trim().length() == 0
//                                        || encodedImages == null || encodedImages.trim().length() == 0) {
//                                    Toast.makeText(context,  context.getText(R.string.label_image), Toast.LENGTH_SHORT).show();
//                                    encodedImage = "";
//                                }
//                                else {
//                                    Log.e("id---", checklistData.get(adapterposition).getId()
//                                            + "\nisdone---" + isdone
//                                            + "\ncomment--" + comment + "\nremarkId--" + remarkId + "\nencode--" + encodedImages == null ? "" : encodedImages);
                                    if (encodedImage == null) {
                                        DetailFragment.onUpdateClick(context, checklistData.get(position).getId(),
                                                isdone, "", comment, "", 0);
                                    }
                                    else {
                                        DetailFragment.onUpdateClick(context, checklistData.get(position).getId(),
                                                isdone, encodedImages, comment, "", 0);
                                    }
//                                    DetailFragment.onUpdateClick(context, checklistData.get(position).getId(),
//                                            isdone, encodedImages, comment, "", 0);

//                                }
                            }
                        }
                        else {
                            Toast.makeText(context, context.getText(R.string.update_task_location), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else if(role.equals("supervisor")){
                        String comment = edit_comment.getText().toString();
                        int remarkId = 0;
//                        if (comment == null || comment.trim().length() == 0) {
//                            Toast.makeText(context,  context.getText(R.string.label_comment), Toast.LENGTH_SHORT).show();
//                        }
//                        else {

//                            Log.e("id---", checklistData.get(position).getId()
//                                    +"\nisdone---"+ isdone
//                                    +"\ncomment--"+ comment +"\nremarkId--"+ remarkId +"\nencode--"+ encodedImage );

                            DetailFragment.onUpdateClick(context,checklistData.get(position).getId(),
                                    isdone,encodedImage, comment, "",0);

//                        }
                    }
                    break;
            }

        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {

//            Log.d("ExpandableLayout", "getAdapterPosition: " + getAdapterPosition());
            if (state == ExpandableLayout.State.EXPANDING) {
                recyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }
    }

    private void checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions((Activity) context,permissionArrays, 11111);
            selectImage();
        } else {
            // if already permition granted
            selectImage();
        }
    }

    // Select image from camera and gallery
    private void selectImage() {
        try {
            final CharSequence[] options = {context.getText(R.string.take_photo), context.getText(R.string.choose_gallery), context.getText(R.string.cancel)};
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getText(R.string.select_option));
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals(context.getText(R.string.take_photo))) {
                        dialog.dismiss();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        ((TaskDetailsActivity) context).startActivityForResult(intent, PICK_IMAGE_CAMERA);
                    } else if (options[item].equals(context.getText(R.string.choose_gallery))) {
                        dialog.dismiss();
                        Intent pickPhoto = new Intent();
                        pickPhoto.setType("image/*");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                            pickPhoto.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        }
                        pickPhoto.setAction(Intent.ACTION_GET_CONTENT);
                        ((TaskDetailsActivity)context).startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                    } else if (options[item].equals(context.getText(R.string.cancel))) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            Toast.makeText(context, context.getText(R.string.camera_permision), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public static void imagesList(Bitmap bitmaps,Uri imageList,String images){
        bitmap = imageList;
        encodedImage = images;
        bitmapImage = bitmaps;
        adapterposition = selectedPos;
    }

    public interface Callback {
        void onUpdateClick(int checklistId, int isDone, String images, String comment, int remarkId);
    }

    public void setCallback(Callback callback) {
    }


}
