package eu.spottless.userit.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;
import eu.spottless.userit.R;
import eu.spottless.userit.TinyDB;
import eu.spottless.userit.activities.FullScreenViewActivity;
import eu.spottless.userit.activities.ReplyActivity;
import eu.spottless.userit.activities.TaskDetailsActivity;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.fragments.DetailFragment;
import eu.spottless.userit.responseModel.tasksResponse.RemarksData;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.MyViewHolder>{

    private int rowLayout;
    private Context context;
    private List<RemarksData> remarksDataList;
    boolean isImageFitToScreen;
    int checkListId, isDone;
    private final int PICK_IMAGE_CAMERA = 3, PICK_IMAGE_GALLERY = 4;
    static byte[] bitmap;
    static Bitmap bitmapImg;
    static String encodedImage;
    int isCompleted, isClose,is_validate,isCheckDone;
    static String image,imageuri;
    static byte[] bytes;
    String role, task_name;
    static Uri uriImages;
    static ArrayList<Uri> uris;
    static int selectedPos, adapterposition;
    OnItemClicked onClick;
    static int remark_id;
    static TinyDB tinydb;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        final ImageView img_remark;
        final ImageView img_camera;
        final TextView txt_viewimg;
        final EditText edit_comment;
        final LinearLayout linear_updateStatus;
        final LinearLayout linear_validate;
        final LinearLayout linear_viewRply;
        ProgressBar progressBar;

        MyViewHolder(View v) {
            super(v);
            linear_updateStatus = v.findViewById(R.id.linear_updateStatus);
            linear_validate = v.findViewById(R.id.linear_validate);
            linear_viewRply = v.findViewById(R.id.linear_viewRply);
//            progressBar = itemView.findViewById(R.id.progressBar);
            img_remark = v.findViewById(R.id.img_remark);
            img_camera = v.findViewById(R.id.img_camera);
            txt_viewimg = v.findViewById(R.id.txt_viewimg);
            SpannableString content = new SpannableString(context.getString(R.string.view_img));
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            txt_viewimg.setText(content);
            edit_comment = v.findViewById(R.id.edit_comment);

            role = SharedPreferencesClass.retriveRole(context,"role");
        }
    }

    public RecycleViewAdapter(List<RemarksData> remarksDataList, int checkListId, int isCheckDone,int isDone, int isCompleted,int isClose,int rowLayout, String task_name, Context context) {
        this.remarksDataList = remarksDataList;
        this.rowLayout = rowLayout;
        this.context = context;
        this.checkListId = checkListId;
        this.isDone = isDone;
        this.isCompleted = isCompleted;
        this.isClose = isClose;
        this.isCheckDone = isCheckDone;
        this.task_name = task_name;
        tinydb = new TinyDB(context);
    }

    public RecycleViewAdapter(){

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.edit_comment.setText(remarksDataList.get(position).getRemark());
        image = remarksDataList.get(position).getImg();

        if(image == null){
            holder.txt_viewimg.setVisibility(View.GONE);
        }
        else {
            holder.txt_viewimg.setVisibility(View.VISIBLE);
        }

        is_validate = remarksDataList.get(position).getIs_validate();
//        Log.d("onBindViewHolder------", String.valueOf(isDone));
//        Log.e("onBindViewHolder()","checkListId---"+checkListId);
        if(role.equals("employee")){

            if(isCompleted == 1){
                if(isClose == 1) {
                    holder.linear_updateStatus.setVisibility(View.GONE);
                    holder.edit_comment.setEnabled(false);
                    holder.edit_comment.setFocusable(false);
                    holder.img_camera.setVisibility(View.GONE);
                }
                else {
                    holder.linear_updateStatus.setVisibility(View.VISIBLE);
                    holder.edit_comment.setEnabled(true);
                    holder.edit_comment.setFocusable(true);
                    holder.img_camera.setVisibility(View.VISIBLE);
                }
            }else {
                holder.linear_updateStatus.setVisibility(View.VISIBLE);
                holder.edit_comment.setEnabled(true);
                holder.edit_comment.setFocusable(true);
                holder.img_camera.setVisibility(View.VISIBLE);
            }

        }
        else if(role.equals("supervisor")){
            if(task_name.equals("declined")){
                holder.linear_updateStatus.setVisibility(View.GONE);
                holder.linear_validate.setVisibility(View.GONE);
                holder.edit_comment.setEnabled(false);
                holder.edit_comment.setFocusable(false);
                holder.img_camera.setVisibility(View.GONE);
            }
            else {
                if (isCompleted == 1 || isCheckDone == 1) {
                    if (isClose == 1) {
                        holder.linear_updateStatus.setVisibility(View.GONE);
                        holder.linear_validate.setVisibility(View.GONE);
                        holder.edit_comment.setEnabled(false);
                        holder.edit_comment.setFocusable(false);
                        holder.img_camera.setVisibility(View.GONE);
                    } else {
                        if (isDone == 1 && is_validate == 0) {
                            holder.linear_validate.setVisibility(View.VISIBLE);
                            holder.linear_updateStatus.setVisibility(View.GONE);
                            holder.edit_comment.setEnabled(false);
                            holder.edit_comment.setFocusable(false);
                            holder.img_camera.setVisibility(View.GONE);
                        } else if (isDone == 1 && is_validate == 1) {
                            holder.linear_validate.setVisibility(View.GONE);
                            holder.linear_updateStatus.setVisibility(View.GONE);
                            holder.edit_comment.setEnabled(false);
                            holder.edit_comment.setFocusable(false);
                            holder.img_camera.setVisibility(View.GONE);
                        } else {
                            holder.linear_updateStatus.setVisibility(View.VISIBLE);
                            holder.linear_validate.setVisibility(View.GONE);
                            holder.edit_comment.setEnabled(true);
                            holder.edit_comment.setFocusable(true);
                            holder.img_camera.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    holder.linear_updateStatus.setVisibility(View.VISIBLE);
                    holder.linear_validate.setVisibility(View.GONE);
                    holder.edit_comment.setEnabled(true);
                    holder.edit_comment.setFocusable(true);
                    holder.img_camera.setVisibility(View.VISIBLE);
                }
            }
        }

        if(remarksDataList.get(position).getReply().size() != 0){
            holder.linear_viewRply.setVisibility(View.VISIBLE);
        }
        else {
            holder.linear_viewRply.setVisibility(View.GONE);
        }

        if(encodedImage == null || bitmapImg == null || bitmap.length == 0 || uriImages == null){
            image = remarksDataList.get(position).getImg();
//            Log.d("RecycleViewAdapter", "image: " + image);

            Picasso.with(context)
                    .load(image)
                    .error(R.mipmap.ic_launcher)
                    .placeholder(R.mipmap.ic_launcher)
                    .resize(200,200)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .networkPolicy(NetworkPolicy.NO_CACHE,NetworkPolicy.NO_STORE)
                    .into(holder.img_remark);

           if(role.equals("supervisor")) {
               holder.linear_updateStatus.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                       String comment = holder.edit_comment.getText().toString();
                       int remarkId = remarksDataList.get(position).getId();
//                       if (comment == null || comment.trim().length() == 0) {
//                           Toast.makeText(context, context.getText(R.string.label_comment), Toast.LENGTH_SHORT).show();
//                       } else {
                           if (encodedImage == null) {
                               DetailFragment.onUpdateClick(context, remarksDataList.get(position).getCheck_sheet_list_id(),
                                       isDone, "", comment, String.valueOf(remarkId), 0);
                           } else {
                               DetailFragment.onUpdateClick(context, remarksDataList.get(position).getCheck_sheet_list_id(),
                                       isDone, encodedImage, comment, String.valueOf(remarkId), 0);
                           }
//                       }
                   }
               });
           }
           else if(role.equals("employee")){
               holder.linear_updateStatus.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                       String comment = holder.edit_comment.getText().toString();
                       int remarkId = remarksDataList.get(position).getId();
//                       if (comment == null || comment.trim().length() == 0) {
//                           Toast.makeText(context, context.getText(R.string.label_comment), Toast.LENGTH_SHORT).show();
//                       } else {
                           if (encodedImage == null) {
                               DetailFragment.onUpdateClick(context, remarksDataList.get(position).getCheck_sheet_list_id(),
                                       1, "", comment, String.valueOf(remarkId), 0);
                           } else {
                               DetailFragment.onUpdateClick(context, remarksDataList.get(position).getCheck_sheet_list_id(),
                                       1, encodedImage, comment, String.valueOf(remarkId), 0);
                           }
//                       }
                   }
               });
            }
        }
        else {
            bytes = bitmap;
//            Log.d("RecycleViewAdapter", "bytes: " + bytes);
            Log.e("selectedPos..", String.valueOf(selectedPos));
            Log.d("position..", String.valueOf(position));
            if(remark_id == remarksDataList.get(position).getId()){
                Picasso.with(context)
                        .load(uriImages)
                        .resize(200,200)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .networkPolicy(NetworkPolicy.NO_CACHE,NetworkPolicy.NO_STORE)
                        .into(holder.img_remark);

                if(role.equals("supervisor")) {
                    holder.linear_updateStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String comment = holder.edit_comment.getText().toString();
                            int remarkId = remarksDataList.get(position).getId();
//                            if (comment == null || comment.trim().length() == 0) {
//                                Toast.makeText(context, context.getText(R.string.label_comment), Toast.LENGTH_SHORT).show();
//                            } else {
                                if (encodedImage == null) {
                                    DetailFragment.onUpdateClick(context, remarksDataList.get(position).getCheck_sheet_list_id(),
                                            isDone, "", comment, String.valueOf(remarkId), 0);
                                } else {
                                    DetailFragment.onUpdateClick(context, remarksDataList.get(position).getCheck_sheet_list_id(),
                                            isDone, encodedImage, comment, String.valueOf(remarkId), 0);
                                }
//                            }
                        }
                    });
                }
                else if(role.equals("employee")) {
                    holder.linear_updateStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String comment = holder.edit_comment.getText().toString();
                            int remarkId = remarksDataList.get(position).getId();
//                            if (comment == null || comment.trim().length() == 0) {
//                                Toast.makeText(context, context.getText(R.string.label_comment), Toast.LENGTH_SHORT).show();
//                            } else {
                                if (encodedImage == null) {
                                    DetailFragment.onUpdateClick(context, remarksDataList.get(position).getCheck_sheet_list_id(),
                                            1, "", comment, String.valueOf(remarkId), 0);
                                } else {
                                    DetailFragment.onUpdateClick(context, remarksDataList.get(position).getCheck_sheet_list_id(),
                                            1, encodedImage, comment, String.valueOf(remarkId), 0);
                                }
//                            }
                        }
                    });
                }
            }
            else {
                Picasso.with(context)
                        .load(remarksDataList.get(position).getImg())
                        .resize(200, 200)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                        .into(holder.img_remark);

                if(role.equals("supervisor")) {
                    holder.linear_updateStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String comment = holder.edit_comment.getText().toString();
                            int remarkId = remarksDataList.get(position).getId();
//                            if (comment == null || comment.trim().length() == 0) {
//                                Toast.makeText(context, context.getText(R.string.label_comment), Toast.LENGTH_SHORT).show();
//                            } else {
                                if (encodedImage == null) {
                                    DetailFragment.onUpdateClick(context, remarksDataList.get(position).getCheck_sheet_list_id(),
                                            isDone, "", comment, String.valueOf(remarkId), 0);
                                } else {
                                    DetailFragment.onUpdateClick(context, remarksDataList.get(position).getCheck_sheet_list_id(),
                                            isDone, encodedImage, comment, String.valueOf(remarkId), 0);
                                }
//                            }
                        }
                    });
                }
                else if(role.equals("employee")) {
                    holder.linear_updateStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String comment = holder.edit_comment.getText().toString();
                            int remarkId = remarksDataList.get(position).getId();
//                            if (comment == null || comment.trim().length() == 0) {
//                                Toast.makeText(context, context.getText(R.string.label_comment), Toast.LENGTH_SHORT).show();
//                            } else {
                                if (encodedImage == null) {
                                    DetailFragment.onUpdateClick(context, remarksDataList.get(position).getCheck_sheet_list_id(),
                                            1, "", comment, String.valueOf(remarkId), 0);
                                } else {
                                    DetailFragment.onUpdateClick(context, remarksDataList.get(position).getCheck_sheet_list_id(),
                                            1, "", comment, String.valueOf(remarkId), 0);
                                }
//                            }
                        }
                    });
                }
            }
        }

        holder.linear_viewRply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ReplyActivity.class);
                i.putExtra("reply", (Serializable) remarksDataList.get(position).getReply());
                context.startActivity(i);
            }
        });

        holder.txt_viewimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(encodedImage != null){
                    if(remark_id == remarksDataList.get(position).getId()){
//                        Log.d("txt_viewimg Image", "image: " + image);
                        Intent i = new Intent(context, FullScreenViewActivity.class);
                        i.putExtra("imageuri", bytes);
                        context.startActivity(i);
                    }
                    else {
//                        Log.d("txt_viewimg", "image: " + image);
                        Intent i = new Intent(context, FullScreenViewActivity.class);
                        i.putExtra("img", remarksDataList.get(position).getImg());
                        context.startActivity(i);
                    }
                }
                else{
//                    Log.d("txt_viewimg", "image: " + image);
                    Intent i = new Intent(context, FullScreenViewActivity.class);
                    i.putExtra("img", remarksDataList.get(position).getImg());
                    context.startActivity(i);
                }


            }
        });

        holder.img_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.e("Recycleviewadapter..", String.valueOf(position));
                adapterposition = position;
                selectedPos = adapterposition;
                remark_id = remarksDataList.get(position).getId();
//                Log.e("Recycleviewadapter..", String.valueOf(remark_id));
//                DetailFragment.imageClick(context,adapterposition);
                checkRunTimePermission(String.valueOf(position));
            }
        });

        holder.linear_validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comment = holder.edit_comment.getText().toString();
                int remarkId = remarksDataList.get(position).getId();
//                if (comment == null || comment.trim().length() == 0) {
//                    Toast.makeText(context, context.getText(R.string.label_comment), Toast.LENGTH_SHORT).show();
//                }
//                else {

                    DetailFragment.onUpdateClick(context,remarksDataList.get(position).getCheck_sheet_list_id(),
                            1, "", comment, String.valueOf(remarkId),1);
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return remarksDataList.size();
    }

    private void checkRunTimePermission(String position) {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions((Activity) context,permissionArrays, 11111);
            selectImage(position);
        } else {
            // if already permition granted
            selectImage(position);
        }
    }

    // Select image from camera and gallery
    private void selectImage(final String position) {
        try {
            final CharSequence[] options = {context.getText(R.string.take_photo), context.getText(R.string.choose_gallery), context.getText(R.string.cancel)};
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getText(R.string.select_option));
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals(context.getText(R.string.take_photo))) {
                        dialog.dismiss();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        ((TaskDetailsActivity) context).startActivityForResult(intent, PICK_IMAGE_CAMERA);
                    } else if (options[item].equals(context.getText(R.string.choose_gallery))) {
                        dialog.dismiss();
                        Intent pickPhoto = new Intent();
                        pickPhoto.setType("image/*");
                        pickPhoto.putExtra("position",position);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                            pickPhoto.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                            pickPhoto.putExtra("position",position);
                        }
                        pickPhoto.setAction(Intent.ACTION_GET_CONTENT);
                        ((TaskDetailsActivity)context).startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                    } else if (options[item].equals(context.getText(R.string.cancel))) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            Toast.makeText(context, context.getText(R.string.camera_permision), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public static void imagesList(Bitmap bitmaps, byte[] imageList, String images, Uri uriImage, ArrayList<Uri> urii){
        bitmap = imageList;
        bitmapImg = bitmaps;
        encodedImage = images;
        uriImages = uriImage;
        uris = urii;
        selectedPos = adapterposition;
    }

    public interface OnItemClicked {
        void onItemClicked(int position);
    }

    public void setOnClick(OnItemClicked onClick) {
        this.onClick = onClick;
    }
}
