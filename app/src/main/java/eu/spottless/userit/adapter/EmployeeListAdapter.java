package eu.spottless.userit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import eu.spottless.userit.R;
import eu.spottless.userit.responseModel.employeeResponse.EmployeeList;

@SuppressWarnings("unchecked")
public class EmployeeListAdapter extends RecyclerView.Adapter<EmployeeListAdapter.EmployeeListHolder> implements Filterable {

    private EmployeeListItemClickListener employeeListItemClickListener;
    List<EmployeeList> employeeLists;
    List<EmployeeList> filteredemployeeLists;
    private final int rowLayout;
    private final Context context;

    public class EmployeeListHolder extends RecyclerView.ViewHolder{
        final LinearLayout linear_line;
        final ImageView img_profile;
        final TextView txt_companyname;
        final TextView txt_designation;
        final TextView txt_name;
        final TextView txt_status;
        String upcomingcount, overduecount, pendingcount, completedcount;

        EmployeeListHolder(View v) {
            super(v);
            linear_line = v.findViewById(R.id.linear_line);

            img_profile = v.findViewById(R.id.img_profile);
            txt_companyname = v.findViewById(R.id.txt_companyname);
            txt_designation = v.findViewById(R.id.txt_designation);
            txt_name = v.findViewById(R.id.txt_name);
            txt_status = v.findViewById(R.id.txt_status);

//            upcomingcount = filteredemployeeLists.get(getAdapterPosition()).getTask_count().getUpcomming_task();
//            overduecount = filteredemployeeLists.get(getAdapterPosition()).getTask_count().getOverdue_task();
//            pendingcount = filteredemployeeLists.get(getAdapterPosition()).getTask_count().getPending_task();
//            completedcount = filteredemployeeLists.get(getAdapterPosition()).getTask_count().getComplete_task();

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (employeeListItemClickListener != null) {
                        employeeListItemClickListener.onClick(view, EmployeeListHolder.this.getAdapterPosition());
                    }
                }
            });
        }
    }

    public EmployeeListAdapter(List<EmployeeList> employeeLists, int rowLayout, Context context) {
        this.employeeLists = employeeLists;
        this.filteredemployeeLists = employeeLists;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @NonNull
    @Override
    public EmployeeListAdapter.EmployeeListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new EmployeeListHolder(view);
    }


    @Override
    public void onBindViewHolder(EmployeeListHolder holder, int position) {
        holder.txt_companyname.setText(filteredemployeeLists.get(position).getCompany_name().substring(0, 1).toUpperCase()+ filteredemployeeLists.get(position).getCompany_name().substring(1));
        holder.txt_name.setText(filteredemployeeLists.get(position).getName().substring(0, 1).toUpperCase()+ filteredemployeeLists.get(position).getName().substring(1));

       if(filteredemployeeLists.get(position).getDesignation_name().equals("")){
           holder.txt_designation.setText(filteredemployeeLists.get(position).getDesignation_name());
       }
       else {
           holder.txt_designation.setText(filteredemployeeLists.get(position).getDesignation_name().substring(0, 1).toUpperCase()+ filteredemployeeLists.get(position).getDesignation_name().substring(1));
       }
        holder.txt_status.setText(filteredemployeeLists.get(position).getStatus().substring(0, 1).toUpperCase()+ filteredemployeeLists.get(position).getStatus().substring(1));

        Picasso.with(context)
                .load(filteredemployeeLists.get(position).getAvatar_url())
                .into(holder.img_profile);

        if(filteredemployeeLists.get(position).getStatus().equals("Active")){
            holder.txt_status.setTextColor(context.getResources().getColor(R.color.parrotgreen));
            holder.linear_line.setBackgroundColor(context.getResources().getColor(R.color.parrotgreen));
        }
        if(filteredemployeeLists.get(position).getStatus().equals("Inactive")){
            holder.txt_status.setTextColor(context.getResources().getColor(R.color.red));
            holder.linear_line.setBackgroundColor(context.getResources().getColor(R.color.red));
        }
    }

    @Override
    public int getItemCount() {
        return filteredemployeeLists.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    filteredemployeeLists = employeeLists;
                } else {

                    List<EmployeeList> filteredemployeeList = new ArrayList<>();

                    for (EmployeeList employeeList : employeeLists) {

                        if (employeeList.getCompany_name().toLowerCase().contains(charString)
                                || employeeList.getName().toLowerCase().contains(charString)
                                || employeeList.getDesignation_name().toLowerCase().contains(charString)
                                || employeeList.getStatus().toLowerCase().contains(charString)) {

                            filteredemployeeList.add(employeeList);
                        }
                    }

                    filteredemployeeLists = filteredemployeeList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredemployeeLists;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredemployeeLists = (List<EmployeeList>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public void setOnItemClicklListener(final EmployeeListItemClickListener employeeListItemClickListener) {
        this.employeeListItemClickListener = employeeListItemClickListener;
    }

    public interface EmployeeListItemClickListener {
        void onClick(View view, int position);

    }
}
