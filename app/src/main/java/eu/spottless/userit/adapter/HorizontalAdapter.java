package eu.spottless.userit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import eu.spottless.userit.R;

public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.HorizontalHolder> {

    private final int rowLayout;
    private final List<String> list_tool;

    public class HorizontalHolder extends RecyclerView.ViewHolder{
        final LinearLayout linearLayout;
        final TextView txt_toolname;

        HorizontalHolder(View v) {
            super(v);
            linearLayout = v.findViewById(R.id.linearLayout);
            txt_toolname = v.findViewById(R.id.txt_toolname);

        }

    }

    public HorizontalAdapter(List<String> list_tool, int rowLayout, Context context) {
        this.list_tool = list_tool;
        this.rowLayout = rowLayout;
    }

    @NonNull
    @Override
    public HorizontalAdapter.HorizontalHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new HorizontalHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull HorizontalHolder holder, int position) {
//        Log.e("Horizontal listtool---", String.valueOf(list_tool));
        if (list_tool.get(position) != null) {
            holder.linearLayout.setVisibility(View.VISIBLE);
            holder.txt_toolname.setText(list_tool.get(position).substring(0, 1).toUpperCase() + list_tool.get(position).substring(1));
        } else{
            holder.linearLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if(list_tool != null){
            return list_tool.size();
        }
        return 0;
//        return list_tool.size();
    }

}
