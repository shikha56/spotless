package eu.spottless.userit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import eu.spottless.userit.R;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.LanguageListHolder> {
        private LangItemClickListener langItemClickListener;
        Context context;
        int flags[];
        String[] languageNames;
        LayoutInflater inflter;

    public CustomAdapter(Context applicationContext, int[] flags, String[] languageNames) {
            this.context = applicationContext;
            this.flags = flags;
            this.languageNames = languageNames;
            inflter = (LayoutInflater.from(applicationContext));
        }

    public class LanguageListHolder extends RecyclerView.ViewHolder{
        final ImageView icon;
        final TextView names;

        LanguageListHolder(View view) {
            super(view);

            icon = view.findViewById(R.id.img_langicon);
            names = view.findViewById(R.id.txt_language);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (langItemClickListener != null) {
                        langItemClickListener.onClick(view, getAdapterPosition());
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public LanguageListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_spinner_items, parent, false);
        return new LanguageListHolder(view);
    }

    @Override
    public int getItemCount() {
        return languageNames.length;
    }

    @Override
    public void onBindViewHolder(LanguageListHolder holder, int position) {
        holder.icon.setImageResource(flags[position]);
        holder.names.setText(languageNames[position]);
    }

    public void setOnItemClicklListener(final LangItemClickListener langItemClickListener) {
        this.langItemClickListener = langItemClickListener;
    }

    public interface LangItemClickListener {
        void onClick(View view, int position);

    }
}
