package eu.spottless.userit.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import eu.spottless.userit.R;
import eu.spottless.userit.responseModel.tasksResponse.ReplyData;
import eu.spottless.userit.utils.TouchImageView;

public class FullScreenImageAdapter extends PagerAdapter {

	private Activity _activity;
	private ArrayList<String> _imagePaths;

	List<ReplyData> replyData;
	// constructor
	public FullScreenImageAdapter(Activity activity, List<ReplyData> replyData) {
		this._activity = activity;
		this.replyData = replyData;
	}

	@Override
	public int getCount() {
		return replyData.size();
	}

	@Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
	
	@Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;
        TextView txt_comment,txt_user;
        Button btnClose;

		LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);
 
        imgDisplay = viewLayout.findViewById(R.id.imgDisplay);
        btnClose = viewLayout.findViewById(R.id.btnClose);
		txt_comment = viewLayout.findViewById(R.id.txt_comment);
		txt_user = viewLayout.findViewById(R.id.txt_user);
        txt_comment.setText(replyData.get(position).getRemark());
		txt_user.setText(replyData.get(position).getUser_type()+": ");

		Picasso.with(_activity)
				.load(replyData.get(position).getImg())
				.error(R.mipmap.ic_launcher)
				.placeholder(R.mipmap.ic_launcher)
				.resize(600,400)
				.memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
				.networkPolicy(NetworkPolicy.NO_CACHE,NetworkPolicy.NO_STORE)
				.into(imgDisplay);

        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				_activity.finish();
			}
		}); 

        container.addView(viewLayout);
 
        return viewLayout;
	}
	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
 
    }

}
