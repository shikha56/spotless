package eu.spottless.userit.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import eu.spottless.userit.R;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.fragments.CompletedTask;
import eu.spottless.userit.fragments.DeclinedTask;
import eu.spottless.userit.fragments.OverdueTask;
import eu.spottless.userit.fragments.PendingTask;
import eu.spottless.userit.fragments.UpcomingTask;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int upcomingcount, overduecount, pendingcount, completedcount, empId, declinedcount, value;
    FragmentManager fm;
    Context context;
    String role;

    public PagerAdapter(FragmentManager fm, Context context,int upcomingcount, int overduecount, int pendingcount, int completedcount, int empId, int declinedcount) {
        super(fm);
        this.upcomingcount = upcomingcount;
        this.overduecount = overduecount;
        this.pendingcount = pendingcount;
        this.completedcount = completedcount;
        this.declinedcount = declinedcount;
        this.empId = empId;
        this.fm = fm;
        this.context = context;
        this.role = SharedPreferencesClass.retriveRole(context,"role");
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
//        Log.e("getItem position----", String.valueOf(position));
        switch (position) {
            case 0:
                return PendingTask.newInstance("2",empId);
            case 1:
                return CompletedTask.newInstance("1",empId);
            case 2:
                return OverdueTask.newInstance("4",empId);
            case 3:
                return UpcomingTask.newInstance("3",empId);
            case 4:
//                if(role.equals("employee")){
//                    return null;
//                }
//                else if(role.equals("supervisor")){
                    return DeclinedTask.newInstance("5",empId);
//                }

//            default:
//                return null;
        }
        return null;
    }

    @Override
    public int getCount() {
        if(role.equals("employee")){
            value = 4;
        }
        else if(role.equals("supervisor")){
            value = 5;
        }
        return value;
    }

    @Override
    public CharSequence getPageTitle(int position) {
//        Log.e("getPageTitle----", String.valueOf(position));
        switch (position) {
            case 0:
                return context.getText(R.string.pending) + " ("+pendingcount+")";
            case 1:
                return context.getText(R.string.completed)+ " ("+completedcount+")";
            case 2:
                return context.getText(R.string.overdue)+ " ("+overduecount+")";
            case 3:
                return context.getText(R.string.upcoming)+ " ("+upcomingcount+")";
            case 4:
//                if(role.equals("employee")){
//                    return null;
//                }
//                else if(role.equals("supervisor")){
                    return context.getText(R.string.declined)+ " ("+declinedcount+")";
//                }
        }
        return null;
    }

    public  void updateTitle(int position, int numItems){

        notifyDataSetChanged();
    }

}
