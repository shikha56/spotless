package eu.spottless.userit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import eu.spottless.userit.R;
import eu.spottless.userit.responseModel.Security.QuesData;

public class SecurityQuesAdapter extends BaseAdapter {
        Context context;
        List<QuesData> quesDataList;
        LayoutInflater inflter;

    public SecurityQuesAdapter(Context applicationContext, List<QuesData> quesDataList) {
            this.context = applicationContext;
            this.quesDataList = quesDataList;
            inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return quesDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.ques_list, null);
        TextView txt_name = view.findViewById(R.id.txt_name);
        TextView txt_answer = view.findViewById(R.id.txt_answer);
        txt_name.setText("Ques. "+quesDataList.get(position).getName());
        txt_answer.setText("Ans. "+quesDataList.get(position).getAnswer());
        return view;
    }

    public class QuesHolder extends RecyclerView.ViewHolder{
        final TextView txt_name;
        final TextView txt_answer;

        QuesHolder(View view) {
            super(view);

            txt_name = view.findViewById(R.id.txt_name);
            txt_answer = view.findViewById(R.id.txt_answer);
        }
    }
}
