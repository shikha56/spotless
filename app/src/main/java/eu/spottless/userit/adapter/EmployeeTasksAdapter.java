package eu.spottless.userit.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import eu.spottless.userit.R;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.fragments.CompletedTask;
import eu.spottless.userit.fragments.DeclinedTask;
import eu.spottless.userit.fragments.OverdueTask;
import eu.spottless.userit.fragments.PendingTask;
import eu.spottless.userit.fragments.UpcomingTask;
import eu.spottless.userit.responseModel.tasksResponse.TasksList;

public class EmployeeTasksAdapter extends RecyclerView.Adapter<EmployeeTasksAdapter.EmployeeTasksHolder> implements Filterable {

    private ListItemClickListener listItemClickListener;
    List<TasksList> tasksLists;
    List<TasksList> filteredtasksLists;
    private final int rowLayout;
    private final Context context;
    String fragmentType,status;
    boolean matched = true;

    public class EmployeeTasksHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final LinearLayout linear_line;
        final LinearLayout linearLayoutButton;
//        final LinearLayout linear_norecord;
        final TextView txt_companyname;
        final TextView txt_address;
        final TextView txt_name;
        final TextView txt_time;
        final TextView txt_locate;
        final TextView txt_accept;
        final TextView txt_reject;
        final TextView txt_send;
        final TextView txt_status;
        final TextView txt_refno;
        String destintationLatitude;
        String destintationLongitude;
        int list_id;
        String role, isAccept;
        int isClose;

        EmployeeTasksHolder(View v) {
            super(v);
            linear_line = v.findViewById(R.id.linear_line);
            linearLayoutButton = v.findViewById(R.id.linearLayoutButton);
//            linear_norecord = v.findViewById(R.id.linear_norecord);

            txt_companyname = v.findViewById(R.id.txt_companyname);
            txt_address = v.findViewById(R.id.txt_address);
            txt_name = v.findViewById(R.id.txt_name);
            txt_time = v.findViewById(R.id.txt_time);

            txt_locate = v.findViewById(R.id.txt_locate);
            txt_accept = v.findViewById(R.id.txt_accept);
            txt_reject = v.findViewById(R.id.txt_reject);
            txt_send = v.findViewById(R.id.txt_send);
            txt_status = v.findViewById(R.id.txt_status);
            txt_refno = v.findViewById(R.id.txt_refno);

            role = SharedPreferencesClass.retriveRole(context,"role");
//            Log.e("fragmentType----",fragmentType);

            txt_locate.setOnClickListener(this);
            txt_accept.setOnClickListener(this);
            txt_reject.setOnClickListener(this);
            txt_send.setOnClickListener(this);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listItemClickListener != null) {
                        listItemClickListener.onClick(view, EmployeeTasksHolder.this.getAdapterPosition());
                    }
                }
            });

        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            int adapterPosition = getAdapterPosition();
            list_id = filteredtasksLists.get(adapterPosition).getId();
            destintationLatitude = filteredtasksLists.get(adapterPosition).getClientData().getAddress().getLat();
            destintationLongitude = filteredtasksLists.get(adapterPosition).getClientData().getAddress().getLongitude();

            switch (id) {
                case R.id.txt_locate:
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + destintationLatitude + "," + destintationLongitude);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    view.getContext().startActivity(mapIntent);
                    break;

                case R.id.txt_accept:
                    if(fragmentType.equals("upcoming")){
                        UpcomingTask.getAcceptData(context, String.valueOf(filteredtasksLists.get(getAdapterPosition()).getId()),"1","");
                    }
                    else if(fragmentType.equals("overdue")){
                        OverdueTask.getAcceptData(context, String.valueOf(filteredtasksLists.get(getAdapterPosition()).getId()),"1","");
                    }
                    else if(fragmentType.equals("pending")){
                        PendingTask.getAcceptData(context, String.valueOf(filteredtasksLists.get(getAdapterPosition()).getId()),"1","");
                    }
                    else if(fragmentType.equals("completed")){
                        CompletedTask.getAcceptData(context, String.valueOf(filteredtasksLists.get(getAdapterPosition()).getId()),"1","");
                    }
                    else if(fragmentType.equals("declined")){
                        DeclinedTask.getAcceptData(context, String.valueOf(filteredtasksLists.get(getAdapterPosition()).getId()),"1","");
                    }
                    break;

                case R.id.txt_reject:
                    // custom dialog
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setContentView(R.layout.dialog_box);
                    dialog.setCancelable(false);
                    // set the custom dialog components - text, image and button
                    TextView text = dialog.findViewById(R.id.label_popup);
                    TextView txt_reject = dialog.findViewById(R.id.label_reject);
                    text.setText(filteredtasksLists.get(adapterPosition).getClientData().getName());
                    txt_reject.setText(R.string.label_reject);

                    ImageView img_close = dialog.findViewById(R.id.img_close);
                    final EditText editText = dialog.findViewById(R.id.edtpin);
                    TextView btn_reject = dialog.findViewById(R.id.btn_reject);
                    btn_reject.setVisibility(View.VISIBLE);
                    btn_reject.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view1) {
                            String reason = editText.getText().toString();
                            if (!TextUtils.isEmpty(reason)) {
                            if(fragmentType.equals("upcoming")){
                                UpcomingTask.getAcceptData(context, String.valueOf(filteredtasksLists.get(getAdapterPosition()).getId()),"2",reason);
                                dialog.dismiss();
                            }
                            else if(fragmentType.equals("overdue")){
                                OverdueTask.getAcceptData(context, String.valueOf(filteredtasksLists.get(getAdapterPosition()).getId()),"2",reason);
                                dialog.dismiss();
                            }
                            else if(fragmentType.equals("pending")){
                                PendingTask.getAcceptData(context, String.valueOf(filteredtasksLists.get(getAdapterPosition()).getId()),"2",reason);
                                dialog.dismiss();
                            }
                            else if(fragmentType.equals("completed")){
                                CompletedTask.getAcceptData(context, String.valueOf(filteredtasksLists.get(getAdapterPosition()).getId()),"2",reason);
                                dialog.dismiss();
                            }
                            else if(fragmentType.equals("declined")){
                                DeclinedTask.getAcceptData(context, String.valueOf(filteredtasksLists.get(getAdapterPosition()).getId()),"2",reason);
                                dialog.dismiss();
                            }
                            } else {
                                Toast.makeText(view1.getContext(), "Please enter reason", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    img_close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view12) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    break;

                case R.id.txt_send:
                    // custom dialog
                    final Dialog dialog1 = new Dialog(context);
                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog1.setContentView(R.layout.dialog_box);
                    dialog1.setCancelable(false);
                    // set the custom dialog components - text, image and button
                    TextView txt_name = dialog1.findViewById(R.id.label_popup);
                    TextView txt_send = dialog1.findViewById(R.id.label_reject);
                    txt_send.setText(R.string.label_message);
                    txt_name.setText(filteredtasksLists.get(adapterPosition).getClientData().getName());

                    ImageView img_cancle = dialog1.findViewById(R.id.img_close);
                    final EditText edit_message = dialog1.findViewById(R.id.edtpin);
                    TextView btn_send = dialog1.findViewById(R.id.btn_send);
                    btn_send.setVisibility(View.VISIBLE);

                    btn_send.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view1) {
                            String reason = edit_message.getText().toString();
                            String employeeId = filteredtasksLists.get(getAdapterPosition()).getAssignData().getEmployee_id();

                            if (!TextUtils.isEmpty(reason)) {
                                if(fragmentType.equals("upcoming")){
                                    UpcomingTask.getSendData(context, employeeId,"Spottless Notifications",reason);
                                    dialog1.dismiss();
                                }
                                else if(fragmentType.equals("overdue")){
                                    OverdueTask.getSendData(context, employeeId,"Spottless Notifications",reason);
                                    dialog1.dismiss();
                                }
                                else if(fragmentType.equals("pending")){
                                    PendingTask.getSendData(context, employeeId,"Spottless Notifications",reason);
                                    dialog1.dismiss();
                                }
                                else if(fragmentType.equals("completed")){
                                    CompletedTask.getSendData(context, employeeId,"Spottless Notifications",reason);
                                    dialog1.dismiss();
                                }
                                else if(fragmentType.equals("declined")){
                                    DeclinedTask.getSendData(context, employeeId,"Spottless Notifications",reason);
                                    dialog1.dismiss();
                                }
                            } else {
                                Toast.makeText(view1.getContext(), "Please enter message", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    img_cancle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view12) {
                            dialog1.dismiss();
                        }
                    });
                    dialog1.show();
                    break;
            }
        }
    }

    public EmployeeTasksAdapter(List<TasksList> tasksLists, int rowLayout, Context context, String fragmentType, String status) {
        this.tasksLists = tasksLists;
        this.filteredtasksLists = tasksLists;
        this.rowLayout = rowLayout;
        this.context = context;
        this.fragmentType = fragmentType;
        this.status = status;
    }

    @NonNull
    @Override
    public EmployeeTasksAdapter.EmployeeTasksHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new EmployeeTasksHolder(view);
    }


    @Override
    public void onBindViewHolder(EmployeeTasksHolder holder, final int position) {
//        Log.d("filteredtasksLists---",new Gson().toJson(filteredtasksLists));
        holder.txt_name.setText(filteredtasksLists.get(position).getClientData().getName().substring(0, 1).toUpperCase()+ filteredtasksLists.get(position).getClientData().getName().substring(1));
        holder.txt_companyname.setText(filteredtasksLists.get(position).getName().substring(0, 1).toUpperCase()+ filteredtasksLists.get(position).getName().substring(1));
        holder.txt_address.setText(filteredtasksLists.get(position).getClientData().getAddress().getAddress());

        Date oneWayTripDate = null;
        String str = null;

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy HH:mm aa", Locale.US);
        try {
            oneWayTripDate = df.parse(filteredtasksLists.get(position).getStart_date());
            str = outputFormat.format(oneWayTripDate);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        holder.txt_time.setText(str);
        holder.txt_status.setText(filteredtasksLists.get(position).getStatus());

        if(filteredtasksLists.get(position).getCase_number() != null){
            holder.txt_refno.setVisibility(View.VISIBLE);
            holder.txt_refno.setText("Ref No: "+filteredtasksLists.get(position).getCase_number());
        }
        else if(filteredtasksLists.get(position).getCase_number() == null){
            holder.txt_refno.setVisibility(View.GONE);
        }

            if(fragmentType.equals("upcoming")){
                holder.linear_line.setBackgroundColor(context.getResources().getColor(R.color.grey));
            }
            else if(fragmentType.equals("overdue")){
                holder.linear_line.setBackgroundColor(context.getResources().getColor(R.color.red));
            }
            else if(fragmentType.equals("pending")){
                holder.linear_line.setBackgroundColor(context.getResources().getColor(R.color.darkyellow));
            }
            else if(fragmentType.equals("completed")){
                holder.linear_line.setBackgroundColor(context.getResources().getColor(R.color.parrotgreen));
            }
            else if(fragmentType.equals("declined")){
                holder.linear_line.setBackgroundColor(context.getResources().getColor(R.color.red));
            }
            if(filteredtasksLists.get(position).getStatus().equals("Active")){
                holder.txt_status.setTextColor(context.getResources().getColor(R.color.parrotgreen));
            }
            if(filteredtasksLists.get(position).getStatus().equals("Inactive")){
                holder.txt_status.setTextColor(context.getResources().getColor(R.color.red));
            }

        holder.isAccept = String.valueOf(filteredtasksLists.get(position).getIs_accept());
        holder.isClose = filteredtasksLists.get(position).getIs_close();

        if( holder.role.equals("employee")){
            if( holder.isClose == 1){
                holder.txt_locate.setVisibility(View.GONE);
                holder.txt_accept.setVisibility(View.GONE);
                holder.txt_reject.setVisibility(View.GONE);
                holder.linearLayoutButton.setPadding(0,50,0,0);
            }
            else {
                if (holder.isAccept.equals("2")) {
                    holder.txt_locate.setVisibility(View.GONE);
                    holder.txt_accept.setVisibility(View.GONE);
                    holder.txt_reject.setVisibility(View.GONE);
                    holder.txt_status.setVisibility(View.VISIBLE);
                    holder.txt_status.setText("Rejected");
                    holder.txt_status.setTextColor(context.getResources().getColor(R.color.red));
                } else if (holder.isAccept.equals("1")) {
                    holder.txt_accept.setVisibility(View.GONE);
                    holder.txt_reject.setVisibility(View.GONE);
                    holder.txt_status.setVisibility(View.VISIBLE);
                    holder.txt_status.setText("Accepted");
                    holder.txt_status.setTextColor(context.getResources().getColor(R.color.parrotgreen));
                } else {
                    holder.txt_accept.setVisibility(View.VISIBLE);
                    holder.txt_reject.setVisibility(View.VISIBLE);
                }
            }
        }
        else if(holder.role.equals("supervisor")){
            if(fragmentType.equals("declined")){
                if( holder.isClose == 1){
                    holder.txt_locate.setVisibility(View.GONE);
                    holder.txt_send.setVisibility(View.GONE);
                    holder.txt_status.setVisibility(View.VISIBLE);
                    holder.txt_status.setText("Rejected");
                    holder.txt_status.setTextColor(context.getResources().getColor(R.color.red));
                }
                else {
                    holder.txt_locate.setVisibility(View.GONE);
                    holder.txt_send.setVisibility(View.GONE);
                    holder.txt_status.setVisibility(View.VISIBLE);
                    holder.txt_status.setText("Rejected");
                    holder.txt_status.setTextColor(context.getResources().getColor(R.color.red));
                }
            }
            else{
                if( holder.isClose == 1){
                    holder.txt_locate.setVisibility(View.GONE);
                    holder.txt_send.setVisibility(View.GONE);
                    holder.txt_status.setVisibility(View.VISIBLE);
                }
                else {
                    holder.txt_locate.setVisibility(View.VISIBLE);
                    holder.txt_send.setVisibility(View.VISIBLE);
                    holder.txt_status.setVisibility(View.VISIBLE);
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        if(filteredtasksLists != null){
            return filteredtasksLists.size();
        }
        return 0;
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty() || charSequence.length() == 0) {
                    filteredtasksLists = tasksLists;
                } else {

                    List<TasksList> filteredtaskeList = new ArrayList<>();
                    for (TasksList taskList : tasksLists) {

                        if (taskList.getClientData().getName().toLowerCase().contains(charString)
                                || taskList.getName().toLowerCase().contains(charString)
                                || taskList.getStatus().toLowerCase().contains(charString)) {

                            filteredtaskeList.add(taskList);
                        }
//                        else {
//                            Toast.makeText(context, "No Record Found", Toast.LENGTH_SHORT).show();
//                        }
                    }
                    filteredtasksLists = filteredtaskeList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredtasksLists;
                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredtasksLists = (List<TasksList>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public void setOnItemClicklListener(final ListItemClickListener mItemClickListener) {
        this.listItemClickListener = mItemClickListener;
    }

    public interface ListItemClickListener {
        void onClick(View view, int position);
    }
}
