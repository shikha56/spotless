package eu.spottless.userit.callBacks;

public interface MainCallbacks {

    public void onMsgFromFragToMain (String sender);
}
