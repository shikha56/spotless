package eu.spottless.userit.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import eu.spottless.userit.liveData.EmployeeTaskRepository;
import eu.spottless.userit.liveData.NotificationRepository;
import eu.spottless.userit.liveData.TaskListRepository;
import eu.spottless.userit.responseModel.notificationResponse.MainData;
import eu.spottless.userit.responseModel.tasksResponse.TasksListData;

public class TaskViewModel extends AndroidViewModel {
    private TaskListRepository taskListRepository;
    private EmployeeTaskRepository employeeTaskRepository;
    private NotificationRepository notificationRepository;

    public TaskViewModel(@NonNull Application application) {
        super(application);
        taskListRepository = new TaskListRepository(application);
        employeeTaskRepository = new EmployeeTaskRepository(application);
        notificationRepository = new NotificationRepository(application);
    }

    public LiveData<TasksListData> getAllTasks(String frag_id, String token) {
        return taskListRepository.getMutableLiveData(token,frag_id);
    }

    public LiveData<TasksListData> getAllEmployeeTasks(String frag_id, String empId, String token) {
        return employeeTaskRepository.getMutableLiveData(token,frag_id,empId);
    }

    public LiveData<MainData> getNotifications(String token) {
        return notificationRepository.getMutableLiveData(token);
    }
}
