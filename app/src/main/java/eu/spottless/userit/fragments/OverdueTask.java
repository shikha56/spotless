package eu.spottless.userit.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.List;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import eu.spottless.userit.R;
import eu.spottless.userit.TinyDB;
import eu.spottless.userit.activities.Dashboard;
import eu.spottless.userit.activities.TaskDetailsActivity;
import eu.spottless.userit.adapter.EmployeeTasksAdapter;
import eu.spottless.userit.callBacks.MainCallbacks;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;
import eu.spottless.userit.responseModel.loginResponse.LoginResponse;
import eu.spottless.userit.responseModel.tasksResponse.TasksList;
import eu.spottless.userit.responseModel.tasksResponse.TasksListData;
import eu.spottless.userit.utils.NetworkUtils;
import eu.spottless.userit.utils.UpdateCountEvent;
import eu.spottless.userit.viewmodel.TaskViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OverdueTask extends Fragment {
    final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefresh;
    private TaskViewModel taskViewModel;
    private EmployeeTasksAdapter employeeTasksAdapter;
    private static ProgressDialog progressDialog;
    private String status, authtoken, role;
    private static String fragment_id;
    private static String reason;
    private static String token;
    private static int empId;
    private Context context;
    static FragmentTransaction fragmentTransaction;
    static FragmentActivity activity;
    List<TasksList> taskList;
    static TinyDB tinydb;
    TasksListData tasksData;
    TasksListData emptasksData;
    static Dashboard main;

    private static boolean firstConnect = true;

    public OverdueTask() {
        // Required empty public constructor
    }

    public static OverdueTask getAcceptData(Context context, String taskid, String acceptid, String reasons) {
        reason = reasons;
        token = "Bearer" + SharedPreferencesClass.retriveToken(context, "token");
        if (!NetworkUtils.getConnectivityStatusString(context).equals(context.getText(R.string.not_internet))) {
            acceptTasksApiCall(context, token, taskid, acceptid, reason);
        } else {
            tinydb.putInt("idTask", Integer.parseInt(taskid)); //saves data in TinyDB
            tinydb.putString("tokens", token); //saves data in TinyDB
            tinydb.putInt("acceptId", Integer.parseInt(acceptid)); //saves data in TinyDB
            tinydb.putString("reason", reason); //saves data in TinyDB
            Toast.makeText(context, context.getText(R.string.save_data), Toast.LENGTH_LONG).show();
        }
        return new OverdueTask();
    }

    public static OverdueTask getSendData(Context context, String employeeId, String titles, String body) {
        reason = body;
        token = "Bearer" + SharedPreferencesClass.retriveToken(context, "token");
        notificationSendApiCall(context, token, employeeId, titles, reason);
        return new OverdueTask();
    }

    public static OverdueTask newInstance(String fragmentid, int empid) {
        fragment_id = fragmentid;
        empId = empid;
        return new OverdueTask();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!(getActivity() instanceof MainCallbacks)) {
            throw new IllegalStateException( " Activity must implement MainCallbacks");
        }
        main = (Dashboard) getActivity(); // use this reference reference to invoke main callbacks

        if (getArguments() != null) {
            fragment_id = getArguments().getString("fragment_id");
            empId = getArguments().getInt("empId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_overdue_task, container, false);
        context = getContext();
        activity = (FragmentActivity) root.getContext();

        authtoken = "Bearer" + SharedPreferencesClass.retriveToken(getContext(), "token");
        role = SharedPreferencesClass.retriveRole(getContext(), "role");
        taskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
        swipeRefresh = root.findViewById(R.id.swiperefresh);
        recyclerView = root.findViewById(R.id.recycler_view);

//         Log.e("overdue fragment_id---",fragment_id == null ? "" : fragment_id);
//         Log.e("overdue empId---", String.valueOf(empId));

        tinydb = new TinyDB(context);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!NetworkUtils.getConnectivityStatusString(getContext()).equals(context.getText(R.string.not_internet))) {
                    if (empId != 0) {
                        getEmployeeTask(fragment_id, empId, authtoken);
                    } else {
                        getPopularBlog(fragment_id, authtoken);
                    }
                } else {
                    getTasks();
                }
            }
        });

        return root;
    }


    public void getPopularBlog(String frag_id, String token) {
        swipeRefresh.setRefreshing(true);
        taskViewModel.getAllTasks(frag_id, token).observe(this, new Observer<TasksListData>() {
            @Override
            public void onChanged(@Nullable TasksListData tasksListData) {
                swipeRefresh.setRefreshing(false);
                UpdateCountEvent.OVERDUE_COUNT = tasksListData.getData().size();
                tinydb.putObject("overdue", tasksListData); //saves the object
//                main.onMsgFromFragToMain("OVERDUE‐FRAG", tasksListData.getData().size());
                prepareRecyclerView(tasksListData);
            }
        });
    }

    public void getEmployeeTask(String frag_id, int empId, String token) {
        swipeRefresh.setRefreshing(true);
        taskViewModel.getAllEmployeeTasks(frag_id, String.valueOf(empId), token).observe(this, new Observer<TasksListData>() {
            @Override
            public void onChanged(@Nullable TasksListData tasksListData) {
                swipeRefresh.setRefreshing(false);
                UpdateCountEvent.OVERDUE_COUNT = tasksListData.getData().size();
//                main.onMsgFromFragToMain("OVERDUE‐FRAG", tasksListData.getData().size());
                prepareRecyclerView(tasksListData);
            }
        });
    }

    private void prepareRecyclerView(final TasksListData tasksListData) {
        taskList = tasksListData.getData();

        employeeTasksAdapter = new EmployeeTasksAdapter(taskList, R.layout.list_items, getContext(), "overdue", status);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 4));

        }
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(employeeTasksAdapter);
        employeeTasksAdapter.setOnItemClicklListener(new EmployeeTasksAdapter.ListItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                int task_id = taskList.get(position).getId();
                SharedPreferencesClass.insertTaskId(context, "task_id", task_id);

                if (role.equals("employee")) {
                    String isAccept = taskList.get(position).getAssignData().getIs_accept();
                    if (isAccept.equals("2")) {
                        Toast.makeText(getContext(), context.getText(R.string.not_see_task), Toast.LENGTH_SHORT).show();
                    }
                    if (isAccept.equals("1")) {
                        Intent intent = new Intent(view.getContext(), TaskDetailsActivity.class);
                        intent.putExtra("companyName", taskList.get(position).getClientData().getName());
                        intent.putExtra("clientId", String.valueOf(taskList.get(position).getClient_id()));
                        intent.putExtra("clientLat", taskList.get(position).getClientData().getAddress().getLat());
                        intent.putExtra("clientLong", taskList.get(position).getClientData().getAddress().getLongitude());
                        intent.putExtra("range", taskList.get(position).getClientData().getAddress().getRange());
                        intent.putExtra("checksheetData", taskList.get(position).getChecksheetData());
                        intent.putExtra("isCompleted", String.valueOf(taskList.get(position).getIs_complete()));
                        intent.putExtra("isClose", String.valueOf(taskList.get(position).getIs_close()));
                        intent.putExtra("is_accept", String.valueOf(taskList.get(position).getIs_accept()));
                        intent.putExtra("task_name", "Overdue");
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        view.getContext().startActivity(intent);
                    }
                    if (isAccept.equals("0")) {
                        Intent intent = new Intent(view.getContext(), TaskDetailsActivity.class);
                        intent.putExtra("companyName", taskList.get(position).getClientData().getName());
                        intent.putExtra("clientId", String.valueOf(taskList.get(position).getClient_id()));
                        intent.putExtra("clientLat", taskList.get(position).getClientData().getAddress().getLat());
                        intent.putExtra("clientLong", taskList.get(position).getClientData().getAddress().getLongitude());
                        intent.putExtra("range", taskList.get(position).getClientData().getAddress().getRange());
                        intent.putExtra("checksheetData", taskList.get(position).getChecksheetData());
                        intent.putExtra("isCompleted", String.valueOf(taskList.get(position).getIs_complete()));
                        intent.putExtra("isClose", String.valueOf(taskList.get(position).getIs_close()));
                        intent.putExtra("is_accept", String.valueOf(taskList.get(position).getIs_accept()));
                        intent.putExtra("task_name", "only_view");
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        view.getContext().startActivity(intent);
                    }
                } else if (role.equals("supervisor")) {
                    Intent intent = new Intent(view.getContext(), TaskDetailsActivity.class);
                    intent.putExtra("companyName", taskList.get(position).getClientData().getName());
                    intent.putExtra("clientId", String.valueOf(taskList.get(position).getClient_id()));
                    intent.putExtra("clientLat", taskList.get(position).getClientData().getAddress().getLat());
                    intent.putExtra("clientLong", taskList.get(position).getClientData().getAddress().getLongitude());
                    intent.putExtra("range", taskList.get(position).getClientData().getAddress().getRange());
                    intent.putExtra("checksheetData", taskList.get(position).getChecksheetData());
                    intent.putExtra("isCompleted", String.valueOf(taskList.get(position).getIs_complete()));
                    intent.putExtra("isClose", String.valueOf(taskList.get(position).getIs_close()));
                    intent.putExtra("is_accept", String.valueOf(taskList.get(position).getIs_accept()));
                    intent.putExtra("task_name", "Overdue");
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    view.getContext().startActivity(intent);
                }
            }
        });
        employeeTasksAdapter.notifyDataSetChanged();
    }

    private void getTasks() {
        if (tinydb.getObject("overdue", TasksListData.class) != null) {
            tasksData = tinydb.getObject("overdue", TasksListData.class); // retrieves the object from storage
//            Log.e("overdue tasksData---", String.valueOf(tasksData.getData().size()));
            prepareRecyclerView(tasksData);
        } else if (tinydb.getObject("empOverdue", TasksListData.class) != null) {
            emptasksData = tinydb.getObject("empOverdue", TasksListData.class); // retrieves the object from storage
            prepareRecyclerView(emptasksData);
        }
    }

    private static void acceptTasksApiCall(final Context context, String token, String task_id, String isaccept, String reason) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
        Call<LoginResponse> call = apiService.acceptTask(token, task_id, isaccept, reason);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    if (loginResponse.getStatus().equals("success")) {
                        String message = loginResponse.getMessage();

                        tinydb.remove("idTask");
                        tinydb.remove("tokens");
                        tinydb.remove("acceptId");
                        tinydb.remove("reason");

                        main.onMsgFromFragToMain("OVERDUE‐FRAG");
                        fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.frameLayoutOverdue, new OverdueTask());
                        fragmentTransaction.commit();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else {
                        String message = loginResponse.getMessage();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, context.getText(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("OverdueTask error---", t.toString());
                progressDialog.dismiss();
            }
        });
    }


    private static void notificationSendApiCall(final Context context, String token, String emp_id, String title, String body) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
        Call<LoginResponse> call = apiService.notificationSend(token, emp_id, title, body);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    if (loginResponse.getStatus().equals("success")) {
                        String message = loginResponse.getMessage();
                        Log.d("notificationSend---", new Gson().toJson(loginResponse));

                        fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.frameLayoutOverdue, new OverdueTask());
                        fragmentTransaction.commit();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else {
                        String message = loginResponse.getMessage();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, context.getText(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    public void beginSearch(String query) {
        Log.e("Overdue beginSearch---", query);
        if (employeeTasksAdapter != null) {
            employeeTasksAdapter.getFilter().filter(query);
        }
    }

//    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
//    public void onResultReceived(String result) {
//         Log.e("onResultReceived--",result);
//         getPopularBlog(fragment_id, authtoken);
//    }

    @Override
    public void onStart() {
        super.onStart();
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        getActivity().registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(networkChangeReceiver);
//        EventBus.getDefault().unregister(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (tinydb.getInt("idTask") != 0 && tinydb.getString("tokens") != null
                    && tinydb.getInt("acceptId") != 0 && tinydb.getString("reason") != null) {
                String netstatus = NetworkUtils.getConnectivityStatusString(context);

                if (!netstatus.equals(context.getText(R.string.not_internet))) {
                    if (firstConnect) {
                        int taskid = tinydb.getInt("idTask"); // retrieves the object from storage
                        String token = tinydb.getString("tokens"); // retrieves the object from storage
                        int acceptId = tinydb.getInt("acceptId"); // retrieves the object from storage
                        String reason = tinydb.getString("reason"); // retrieves the object from storage
                        acceptTasksApiCall(context, token, String.valueOf(taskid), String.valueOf(acceptId), reason);
                        progressDialog.dismiss();

                        firstConnect = false;
                    }
                } else {
                    firstConnect = true;
                }
            }
            if (!NetworkUtils.getConnectivityStatusString(getContext()).equals(context.getText(R.string.not_internet))) {
                if (empId != 0) {
                    getEmployeeTask(fragment_id, empId, authtoken);
                } else {
                    getPopularBlog(fragment_id, authtoken);
                }
            } else {
                getTasks();
            }
        }
    };

}
