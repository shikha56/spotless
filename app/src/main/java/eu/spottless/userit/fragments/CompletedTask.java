package eu.spottless.userit.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import eu.spottless.userit.R;
import eu.spottless.userit.TinyDB;
import eu.spottless.userit.activities.Dashboard;
import eu.spottless.userit.activities.TaskDetailsActivity;
import eu.spottless.userit.adapter.EmployeeTasksAdapter;
import eu.spottless.userit.callBacks.MainCallbacks;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;
import eu.spottless.userit.responseModel.loginResponse.LoginResponse;
import eu.spottless.userit.responseModel.tasksResponse.TasksList;
import eu.spottless.userit.responseModel.tasksResponse.TasksListData;
import eu.spottless.userit.utils.NetworkUtils;
import eu.spottless.userit.utils.UpdateCountEvent;
import eu.spottless.userit.viewmodel.TaskViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CompletedTask extends Fragment implements Serializable {
    final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefresh;
    private TaskViewModel taskViewModel;
    private EmployeeTasksAdapter employeeTasksAdapter;
    private static ProgressDialog progressDialog;
    private String status, authtoken, role;
    private static String fragment_id;
    private static String reason;
    private static String token;
    private static int empId;
    private Context context;
    static FragmentTransaction fragmentTransaction;
    static FragmentActivity activity;
    TasksListData tasksData;
    TasksListData emptasksData;
    List<TasksList> taskList;
    TinyDB tinydb;
    private static boolean firstConnect = true;
    static Dashboard main;

    public CompletedTask() {
        // Required empty public constructor
    }

    public static CompletedTask getAcceptData(Context context, String taskid, String acceptid, String reasons) {
        reason = reasons;
        token = "Bearer" + SharedPreferencesClass.retriveToken(context, "token");
        acceptTasksApiCall(context, token, taskid, acceptid, reason);
        return new CompletedTask();
    }

    public static CompletedTask getSendData(Context context, String employeeId, String titles, String body) {
        reason = body;
        token = "Bearer" + SharedPreferencesClass.retriveToken(context, "token");
        notificationSendApiCall(context, token, employeeId, titles, reason);
        return new CompletedTask();
    }

    public static CompletedTask newInstance(String fragmentid, int empid) {
        fragment_id = fragmentid;
        empId = empid;
        return new CompletedTask();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!(getActivity() instanceof MainCallbacks)) {
            throw new IllegalStateException( " Activity must implement MainCallbacks");
        }
        main = (Dashboard) getActivity(); // use this reference reference to invoke main callbacks

        if (getArguments() != null) {
            fragment_id = getArguments().getString("fragment_id");
            empId = getArguments().getInt("empId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_completed_task, container, false);
        context = getContext();
        activity = (FragmentActivity) root.getContext();
        authtoken = "Bearer" + SharedPreferencesClass.retriveToken(getContext(), "token");
        role = SharedPreferencesClass.retriveRole(getContext(), "role");
        taskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
        swipeRefresh = root.findViewById(R.id.swiperefresh);
        recyclerView = root.findViewById(R.id.recycler_view);

        tinydb = new TinyDB(context);

//        Log.e("complete fragment_id---",fragment_id == null ? "" : fragment_id);
//        Log.e("complete empId---", String.valueOf(empId));

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!NetworkUtils.getConnectivityStatusString(getContext()).equals(context.getText(R.string.not_internet))) {
                    if (empId != 0) {
                        getEmployeeTask(fragment_id, empId, authtoken);
                    } else {
                        getPopularBlog(fragment_id, authtoken);
                    }
                } else {
                    getTasks();
                }
            }
        });
        return root;
    }

    public void getPopularBlog(String frag_id, String token) {
        swipeRefresh.setRefreshing(true);
        taskViewModel.getAllTasks(frag_id, token).observe(this, new Observer<TasksListData>() {
            @Override
            public void onChanged(@Nullable TasksListData blogList) {
                swipeRefresh.setRefreshing(false);
                tinydb.putObject("completed", blogList); //saves the object
                UpdateCountEvent.COMPLETED_COUNT = blogList.getData().size();
//                main.onMsgFromFragToMain("COMPLETED‐FRAG", blogList.getData().size());
                prepareRecyclerView(blogList);
            }
        });
    }

    public void getEmployeeTask(String frag_id, int empId, String token) {
        swipeRefresh.setRefreshing(true);
        taskViewModel.getAllEmployeeTasks(frag_id, String.valueOf(empId), token).observe(this, new Observer<TasksListData>() {
            @Override
            public void onChanged(@Nullable TasksListData tasksListData) {
                swipeRefresh.setRefreshing(false);
                tinydb.putObject("empCompleted", tasksListData); //saves the object
                UpdateCountEvent.COMPLETED_COUNT = tasksListData.getData().size();
//                main.onMsgFromFragToMain("COMPLETED‐FRAG", tasksListData.getData().size());
                prepareRecyclerView(tasksListData);
            }
        });
    }

    private void prepareRecyclerView(final TasksListData tasksListData) {
        taskList = tasksListData.getData();

        employeeTasksAdapter = new EmployeeTasksAdapter(taskList, R.layout.list_items, getContext(), "completed", status);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 4));

        }
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(employeeTasksAdapter);
        employeeTasksAdapter.setOnItemClicklListener(new EmployeeTasksAdapter.ListItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                int task_id = taskList.get(position).getId();
                SharedPreferencesClass.insertTaskId(context, "task_id", task_id);

                Log.e("isCompleted", String.valueOf(taskList.get(position).getIs_complete()));

                if (role.equals("employee")) {
                    String isAccept = taskList.get(position).getAssignData().getIs_accept();
                    if (isAccept.equals("2")) {
                        Toast.makeText(getContext(), context.getText(R.string.not_see_task), Toast.LENGTH_SHORT).show();
                    }
                    if (isAccept.equals("1")) {
                        Intent intent = new Intent(view.getContext(), TaskDetailsActivity.class);
                        intent.putExtra("companyName", taskList.get(position).getClientData().getName());
                        intent.putExtra("clientId", String.valueOf(taskList.get(position).getClient_id()));
                        intent.putExtra("clientLat", taskList.get(position).getClientData().getAddress().getLat());
                        intent.putExtra("clientLong", taskList.get(position).getClientData().getAddress().getLongitude());
                        intent.putExtra("range", taskList.get(position).getClientData().getAddress().getRange());
                        intent.putExtra("checksheetData", taskList.get(position).getChecksheetData());
                        intent.putExtra("isCompleted", String.valueOf(taskList.get(position).getIs_complete()));
                        intent.putExtra("isClose", String.valueOf(taskList.get(position).getIs_close()));
                        intent.putExtra("is_accept", String.valueOf(taskList.get(position).getIs_accept()));
                        intent.putExtra("task_name", "Completed");
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        view.getContext().startActivity(intent);
                    }
                    if (isAccept.equals("0")) {
                        Intent intent = new Intent(view.getContext(), TaskDetailsActivity.class);
                        intent.putExtra("companyName", taskList.get(position).getClientData().getName());
                        intent.putExtra("clientId", String.valueOf(taskList.get(position).getClient_id()));
                        intent.putExtra("clientLat", taskList.get(position).getClientData().getAddress().getLat());
                        intent.putExtra("clientLong", taskList.get(position).getClientData().getAddress().getLongitude());
                        intent.putExtra("range", taskList.get(position).getClientData().getAddress().getRange());
                        intent.putExtra("checksheetData", taskList.get(position).getChecksheetData());
                        intent.putExtra("isCompleted", String.valueOf(taskList.get(position).getIs_complete()));
                        intent.putExtra("isClose", String.valueOf(taskList.get(position).getIs_close()));
                        intent.putExtra("is_accept", String.valueOf(taskList.get(position).getIs_accept()));
                        intent.putExtra("task_name", "only_view");
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        view.getContext().startActivity(intent);
                    }
                } else if (role.equals("supervisor")) {
                    Intent intent = new Intent(view.getContext(), TaskDetailsActivity.class);
                    intent.putExtra("companyName", taskList.get(position).getClientData().getName());
                    intent.putExtra("clientId", String.valueOf(taskList.get(position).getClient_id()));
                    intent.putExtra("clientLat", taskList.get(position).getClientData().getAddress().getLat());
                    intent.putExtra("clientLong", taskList.get(position).getClientData().getAddress().getLongitude());
                    intent.putExtra("range", taskList.get(position).getClientData().getAddress().getRange());
                    intent.putExtra("checksheetData", taskList.get(position).getChecksheetData());
                    intent.putExtra("isCompleted", String.valueOf(taskList.get(position).getIs_complete()));
                    intent.putExtra("isClose", String.valueOf(taskList.get(position).getIs_close()));
                    intent.putExtra("is_accept", String.valueOf(taskList.get(position).getIs_accept()));
                    intent.putExtra("task_name", "Completed");
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    view.getContext().startActivity(intent);
                }
            }
        });
        employeeTasksAdapter.notifyDataSetChanged();
    }

    private void getTasks() {

        if (tinydb.getObject("completed", TasksListData.class) != null) {
            tasksData = tinydb.getObject("completed", TasksListData.class); // retrieves the object from storage
//            Log.e("completed tasksData---", String.valueOf(tasksData.getData().size()));
            prepareRecyclerView(tasksData);
        } else if (tinydb.getObject("empCompleted", TasksListData.class) != null) {
            emptasksData = tinydb.getObject("empCompleted", TasksListData.class); // retrieves the object from storage
            prepareRecyclerView(emptasksData);
        }
    }

    private static void acceptTasksApiCall(final Context context, String token, String task_id, String isaccept, String reason) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
        Call<LoginResponse> call = apiService.acceptTask(token, task_id, isaccept, reason);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    String message = loginResponse.getMessage();

                    main.onMsgFromFragToMain("COMPLETED‐FRAG");
                    fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frameLayoutCompleted, new CompletedTask());
                    fragmentTransaction.commit();
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, context.getText(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("CompletedTask error---", t.toString());
                progressDialog.dismiss();
            }
        });
    }

    private static void notificationSendApiCall(final Context context, String token, String emp_id, String title, String body) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
        Call<LoginResponse> call = apiService.notificationSend(token, emp_id, title, body);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    String message = loginResponse.getMessage();

                    fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frameLayoutCompleted, new CompletedTask());
                    fragmentTransaction.commit();
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, context.getText(R.string.error_message), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    public void beginSearch(String query) {
        if (employeeTasksAdapter != null) {
            employeeTasksAdapter.getFilter().filter(query);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        getActivity().registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(networkChangeReceiver);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String netstatus = NetworkUtils.getConnectivityStatusString(context);
            if (!NetworkUtils.getConnectivityStatusString(getContext()).equals(context.getText(R.string.not_internet))) {
                if (empId != 0) {
                    getEmployeeTask(fragment_id, empId, authtoken);
                } else {
                    getPopularBlog(fragment_id, authtoken);
                }

            } else {
                getTasks();
            }
        }
    };
}
