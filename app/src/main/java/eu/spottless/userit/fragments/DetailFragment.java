package eu.spottless.userit.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import eu.spottless.userit.R;
import eu.spottless.userit.TinyDB;
import eu.spottless.userit.adapter.AyalmaExpandAdapter;
import eu.spottless.userit.adapter.HorizontalAdapter;
import eu.spottless.userit.adapter.RecycleViewAdapter;
import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;
import eu.spottless.userit.responseModel.tasksResponse.ChecklistData;
import eu.spottless.userit.responseModel.tasksResponse.ChecksheetData;
import eu.spottless.userit.responseModel.tasksResponse.HeadingData;
import eu.spottless.userit.responseModel.tasksResponse.TasksList;
import eu.spottless.userit.responseModel.tasksResponse.TasksListData;
import eu.spottless.userit.responseModel.tasksResponse.TasksResponse;
import eu.spottless.userit.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class DetailFragment extends Fragment {
    final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    static ChecksheetData checksheetData;
    private static ProgressDialog progressDialog;
    private static RecyclerView recyclerToolView;
    private static RecyclerView expandable_recyclerview;
    private static HorizontalAdapter horizontalAdapter;
    private static AyalmaExpandAdapter ayalmaExpandAdapter;
    private  static  EditText edit_comment;
    private  static TextView txt_remark;

    private static List<String> ListElementsArrayList;
    private static List<HeadingData> headingData;
    private static List<ChecklistData> checklistData;
    private static List<HeadingData> updateheadingData;
    private static List<ChecklistData> updatechecklistData;

    static FragmentTransaction fragmentTransaction;
    static FragmentActivity activity;
    static TinyDB tinydb;

    static String token, task_name;
    private static int tab_id, task_id, isCompleted, isClose, checksheetId;
    String netstatus;
    private static boolean firstConnect = true;
    static TasksListData tasksData;
    static List<TasksList> taskList;
//    private static boolean visible = false;
    static Context contextfrag;

    public DetailFragment() {
        // Required empty public constructor
    }

    public static DetailFragment newInstance(int tabid, ChecksheetData checksheetDatas) {
        tab_id = tabid;
//            Log.e("checksheetData---", "checksheetData");
            checksheetData = checksheetDatas;
        return new DetailFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tab_id = getArguments().getInt("tabid");

//                Log.e("checksheetData---", new Gson().toJson(checksheetData));
                isCompleted = getArguments().getInt("isCompleted");
                isClose = getArguments().getInt("isClose");
                checksheetData = (ChecksheetData) getArguments().getSerializable("checksheetData");
                task_name = getArguments().getString("task_name");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_detail, container, false);
        getActivity().setResult(RESULT_OK);
        token = "Bearer " + SharedPreferencesClass.retriveToken(getContext(), "token");

        Log.e("token-----", token);

        task_id = SharedPreferencesClass.retriveTaskId(getContext(), "task_id");

        contextfrag = getContext();

        Context context = getContext();
        activity = (FragmentActivity) root.getContext();
        tinydb = new TinyDB(context);

//        Log.e("checksheetData---", new Gson().toJson(checksheetData));

        if (tinydb.getObject("taskLists", TasksListData.class) == null) {
            Log.e("if tasksData---", "tasksData");
            headingData = checksheetData.getHeadingData();
            checksheetId = checksheetData.getId();
        } else {
            Log.e("tasksData---", "tasksData");
            getTasks();
        }

        recyclerToolView = root.findViewById(R.id.recycler_toolview);
        recyclerToolView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        expandable_recyclerview = root.findViewById(R.id.expandable_recyclerview);
        expandable_recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));

//        txt_remark = root.findViewById(R.id.txt_remark);
//        txt_remark.setPaintFlags(txt_remark.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        SpannableString content = new SpannableString(getContext().getText(R.string.note));
//        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
//        txt_remark.setText(R.string.note);

        edit_comment = root.findViewById(R.id.edit_comment);
        edit_comment.setText(checksheetData.getComment());
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecycleViewAdapter.imagesList(null, null, null, null, null);
        AyalmaExpandAdapter.imagesList(null, null, null);

        if (tinydb.getObject("taskLists", TasksListData.class) == null) {

            if (tab_id == 0) {
//                Log.d("if-------", String.valueOf(tab_id));
                checklistData = checksheetData.getHeadingData().get(0).getChecklistData();
                String toolName = checksheetData.getHeadingData().get(0).getTool();
                if (toolName != null) {
                    String[] tools = toolName.split(",");
                    ListElementsArrayList = new ArrayList<>(Arrays.asList(tools));
                } else {
                    ListElementsArrayList = new ArrayList<>(Arrays.asList(toolName));
                }
            } else {
//                Log.d("else-------", String.valueOf(tab_id));
                for (int i = 0; i < headingData.size(); i++) {
                    int category_id = headingData.get(i).getId();
                    if (category_id == tab_id) {
                        checklistData = headingData.get(i).getChecklistData();
                        String toolName = headingData.get(i).getTool();
                        if (toolName != null) {
                            String[] tools = toolName.split(",");
                            ListElementsArrayList = new ArrayList<>(Arrays.asList(tools));
                        } else {
                            ListElementsArrayList = new ArrayList<>(Arrays.asList(toolName));
                        }
                    }
                }
            }

            horizontalAdapter = new HorizontalAdapter(ListElementsArrayList, R.layout.horizontal_items_list, getContext());
            recyclerToolView.setAdapter(horizontalAdapter);
            ayalmaExpandAdapter = new AyalmaExpandAdapter(null, expandable_recyclerview, checklistData, isCompleted, isClose, task_name, getContext());
            expandable_recyclerview.setAdapter(ayalmaExpandAdapter);
            ayalmaExpandAdapter.notifyDataSetChanged();

        } else {
//            Log.e("else checksheetData---", "checksheetData");
            getTasks();
        }
    }

    private void getTasks() {

        if (tinydb.getObject("taskLists", TasksListData.class) != null) {
            tasksData = tinydb.getObject("taskLists", TasksListData.class); // retrieves the object from storage
            taskList = tasksData.getData();
            for (int i = 0; i < taskList.size(); i++) {
                int taskid = taskList.get(i).getId();
                if (taskid == task_id) {
                    updateheadingData = taskList.get(i).getChecksheetData().getHeadingData();
                    isCompleted = taskList.get(i).getIs_complete();
                    isClose = taskList.get(i).getIs_close();

                    if (tab_id == 0) {
//                        Log.d("if-------", String.valueOf(tab_id));
                        updatechecklistData = updateheadingData.get(0).getChecklistData();
                        String toolName = updateheadingData.get(0).getTool();
                        if (toolName != null) {
                            String[] tools = toolName.split(",");
                            ListElementsArrayList = new ArrayList<>(Arrays.asList(tools));
                        } else {
                            ListElementsArrayList = new ArrayList<>(Arrays.asList(toolName));
                        }
                    } else {
                        for (int j = 0; j < updateheadingData.size(); j++) {
                            int category_id = updateheadingData.get(j).getId();
                            if (category_id == tab_id) {
                                updatechecklistData = updateheadingData.get(j).getChecklistData();
                                String toolName = updateheadingData.get(j).getTool();
                                if (toolName != null) {
                                    String[] tools = toolName.split(",");
                                    ListElementsArrayList = new ArrayList<>(Arrays.asList(tools));
                                } else {
                                    ListElementsArrayList = new ArrayList<>(Arrays.asList(toolName));
                                }
                            }
                        }
                    }
                }
            }
            horizontalAdapter = new HorizontalAdapter(ListElementsArrayList, R.layout.horizontal_items_list, getContext());
            recyclerToolView.setAdapter(horizontalAdapter);
            RecycleViewAdapter.imagesList(null, null, null, null, null);
            AyalmaExpandAdapter.imagesList(null, null, null);
            ayalmaExpandAdapter = new AyalmaExpandAdapter(null, expandable_recyclerview, updatechecklistData, isCompleted, isClose, task_name, getContext());
            expandable_recyclerview.setAdapter(ayalmaExpandAdapter);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        boolean isViewShown = false;
        if (isVisibleToUser) {

            // Refresh tab data:
            if (getFragmentManager() != null) {

                getFragmentManager()
                        .beginTransaction()
                        .detach(this)
                        .attach(this)
                        .addToBackStack(null)
                        .commit();
            }
        }
    }

    @NonNull
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            File destination;
            String encodeString = null;
            byte[] bt;
            ArrayList<String> imagesEncodedList = new ArrayList<>();
            int PICK_IMAGE_GALLERYY = 4;
            int PICK_IMAGE_CAMERAA = 3;
            int PICK_IMAGE_GALLERY = 2;
            int PICK_IMAGE_CAMERA = 1;
            Bitmap bitmap;
            if (requestCode == PICK_IMAGE_CAMERA) {
                try {
                    bitmap = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                    bt = bytes.toByteArray();
                    encodeString = Base64.encodeToString(bt, Base64.DEFAULT);
                    imagesEncodedList.add(encodeString);
//                    Log.e("Activity", "Pick from Camera::>>> " + encodeString);

                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(getContext(), bitmap);
                    getContext().getContentResolver().notifyChange(tempUri, null);

//                    Log.e("Activity", "Pick from Camera::>>> " + tempUri);
                    ArrayList<Uri> arrayList = new ArrayList<>();
                    arrayList.add(tempUri);

                    tinydb.putString("base64",encodeString);

                    if (tinydb.getObject("taskLists", TasksListData.class) == null) {
                        encodeString = String.valueOf(tinydb.getImage("base64"));

                        AyalmaExpandAdapter.imagesList(bitmap, tempUri, encodeString);
                        ayalmaExpandAdapter = new AyalmaExpandAdapter(arrayList, expandable_recyclerview, checklistData, isCompleted, isClose, task_name, getContext());
                        expandable_recyclerview.setAdapter(ayalmaExpandAdapter);
                        ayalmaExpandAdapter.notifyDataSetChanged();
                    }
                    else {
                        AyalmaExpandAdapter.imagesList(bitmap, tempUri, encodeString);
                        ayalmaExpandAdapter = new AyalmaExpandAdapter(arrayList, expandable_recyclerview, updatechecklistData, isCompleted, isClose, task_name, getContext());
                        expandable_recyclerview.setAdapter(ayalmaExpandAdapter);
                        ayalmaExpandAdapter.notifyDataSetChanged();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == PICK_IMAGE_GALLERY) {
                Uri selectedImageUri = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), selectedImageUri);
//                        bitmap = BitmapFactory.decodeFile(bitmap.toString());
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    Bitmap resizedbitmap = Bitmap.createScaledBitmap(bitmap, 200, 200, true);
                    resizedbitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                    byte[] _byteArray = baos.toByteArray();
                    String base64 = Base64.encodeToString(_byteArray, Base64.DEFAULT);

                    getContext().getContentResolver().notifyChange(selectedImageUri, null);
//                        Log.e("Activity", "Pick from Gallery::>>> " + base64);
                    ArrayList<Uri> arrayList = new ArrayList<>();
                    arrayList.add(selectedImageUri);
                        tinydb.putString("base64",base64);

                    if (tinydb.getObject("taskLists", TasksListData.class) == null) {
                        base64 = String.valueOf(tinydb.getImage("base64"));

                        AyalmaExpandAdapter.imagesList(bitmap, selectedImageUri, base64);
                        ayalmaExpandAdapter = new AyalmaExpandAdapter(arrayList, expandable_recyclerview, checklistData, isCompleted, isClose, task_name, getContext());
                        expandable_recyclerview.setAdapter(ayalmaExpandAdapter);
                        ayalmaExpandAdapter.notifyDataSetChanged();
                    }
                    else {
                        AyalmaExpandAdapter.imagesList(bitmap, selectedImageUri, base64);
                        ayalmaExpandAdapter = new AyalmaExpandAdapter(arrayList, expandable_recyclerview, updatechecklistData, isCompleted, isClose, task_name, getContext());
                        expandable_recyclerview.setAdapter(ayalmaExpandAdapter);
                        ayalmaExpandAdapter.notifyDataSetChanged();
                    }

//                    AyalmaExpandAdapter.imagesList(bitmap, selectedImageUri, base64);
//                    ayalmaExpandAdapter = new AyalmaExpandAdapter(arrayList, expandable_recyclerview, checklistData, isCompleted, isClose, getContext());
//                    expandable_recyclerview.setAdapter(ayalmaExpandAdapter);

                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), getText(R.string.image_size_error), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == PICK_IMAGE_CAMERAA) {
                try {
                    bitmap = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                    bt = bytes.toByteArray();
                    encodeString = Base64.encodeToString(bt, Base64.DEFAULT);
                    imagesEncodedList.add(encodeString);

//                    Log.e("Activity", "Pick from Camera::>>> " + bitmap);

                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(getContext(), bitmap);
//                    Log.e("Activity", "Pick from Camera::>>> " + tempUri);

                    ArrayList<Uri> arrayList = new ArrayList<>();
                    arrayList.add(tempUri);

                    if (tinydb.getObject("taskLists", TasksListData.class) == null) {
                        RecycleViewAdapter.imagesList(bitmap, bt, encodeString, tempUri, arrayList);
                        ayalmaExpandAdapter = new AyalmaExpandAdapter(arrayList, expandable_recyclerview, checklistData, isCompleted, isClose, task_name, getContext());
                        expandable_recyclerview.setAdapter(ayalmaExpandAdapter);
                    }
                    else {
                        RecycleViewAdapter.imagesList(bitmap, bt, encodeString, tempUri, arrayList);
                        ayalmaExpandAdapter = new AyalmaExpandAdapter(arrayList, expandable_recyclerview, updatechecklistData, isCompleted, isClose, task_name, getContext());
                        expandable_recyclerview.setAdapter(ayalmaExpandAdapter);
                    }
//                    RecycleViewAdapter.imagesList(bitmap, bt, encodeString, tempUri, arrayList);
//                    ayalmaExpandAdapter = new AyalmaExpandAdapter(arrayList, expandable_recyclerview, checklistData, isCompleted, isClose, getContext());
//                    expandable_recyclerview.setAdapter(ayalmaExpandAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == PICK_IMAGE_GALLERYY) {
                Uri selectedImageUri = data.getData();
//                String position = data.getStringExtra("position");
//                Log.e("Recycleviewadapter:"," position"+ position == null ? "" : position);
                Uri uri = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), selectedImageUri);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    Bitmap resizedbitmap = Bitmap.createScaledBitmap(bitmap, 200, 200, true);
                    resizedbitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                    byte[] _byteArray = baos.toByteArray();
                    String base64 = Base64.encodeToString(_byteArray, Base64.DEFAULT);

//                    Log.e("Activity", "Pick from Gallery::>>> " + selectedImageUri);
//                    Log.e("Activity", "Pick from Gallery::>>> " + bitmap);

                    ArrayList<Uri> arrayList = new ArrayList<>();
                    arrayList.add(selectedImageUri);

                    if (tinydb.getObject("taskLists", TasksListData.class) == null) {
                        RecycleViewAdapter.imagesList(bitmap, _byteArray, base64, selectedImageUri, arrayList);
                        ayalmaExpandAdapter = new AyalmaExpandAdapter(arrayList, expandable_recyclerview, checklistData, isCompleted, isClose, task_name, getContext());
                        expandable_recyclerview.setAdapter(ayalmaExpandAdapter);
                    }
                    else {
                        RecycleViewAdapter.imagesList(bitmap, _byteArray, base64, selectedImageUri, arrayList);
                        ayalmaExpandAdapter = new AyalmaExpandAdapter(arrayList, expandable_recyclerview, updatechecklistData, isCompleted, isClose, task_name, getContext());
                        expandable_recyclerview.setAdapter(ayalmaExpandAdapter);
                    }
//                    RecycleViewAdapter.imagesList(bitmap, _byteArray, base64, selectedImageUri, arrayList);
//                    ayalmaExpandAdapter = new AyalmaExpandAdapter(arrayList, expandable_recyclerview, checklistData, isCompleted, isClose, getContext());
//                    expandable_recyclerview.setAdapter(ayalmaExpandAdapter);

                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), getText(R.string.image_size_error), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
//            Toast.makeText(getContext(), "Something went wrong" + e, Toast.LENGTH_LONG).show();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
//        return inContext.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,values);
    }

    public static void onUpdateClick(Context context, int checklistId, int isDone, String images, String comment, String remarkId, int validateId) {
        token = "Bearer " + SharedPreferencesClass.retriveToken(context, "token");
        if (!NetworkUtils.getConnectivityStatusString(context).equals(context.getText(R.string.not_internet))) {
            updateTasksApiCall(context, token, String.valueOf(task_id), checklistId, isDone, images, comment, remarkId, validateId);
        } else {
            tinydb.putString("taskid", String.valueOf(task_id)); //saves data in TinyDB
            tinydb.putInt("checklistId", checklistId); //saves data in TinyDB
            tinydb.putInt("isDone", isDone); //saves data in TinyDB
            tinydb.putString("images", images); //saves data in TinyDB
            tinydb.putString("comment", comment); //saves data in TinyDB
            tinydb.putString("remarkId", remarkId); //saves data in TinyDB
            tinydb.putInt("validateId", validateId); //saves data in TinyDB
            Toast.makeText(context, context.getText(R.string.update_success), Toast.LENGTH_LONG).show();
        }
    }

    private static void updateTasksApiCall(final Context context, String token, String taskId, int checkedListId, int isdone, String encodedImage, String remark, String remarkId, int validateId) {

        progressDialog = new ProgressDialog(contextfrag);
        progressDialog.setMessage(context.getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
        //Here the json data is add to a hash map with key data
        Map<String, String> params = new HashMap<>();
        params.put("checksheet_id", String.valueOf(checksheetId));
        params.put("checklist_id", String.valueOf(checkedListId));
        params.put("image", encodedImage);
        params.put("remark", remark);
        params.put("is_done", String.valueOf(isdone));
        params.put("parent", remarkId);
        params.put("validate", String.valueOf(validateId));

        Log.d("params data---", new Gson().toJson(params));

        Call<TasksResponse> call = apiService.updateTasks(token, taskId, params);
        call.enqueue(new Callback<TasksResponse>() {
            @Override
            public void onResponse(Call<TasksResponse> call, Response<TasksResponse> response) {
                //hiding progress dialog
                progressDialog.dismiss();
//                Log.d("updateApi response---", new Gson().toJson(response));
                if (response.isSuccessful()) {
                    TasksResponse tasksResponse = response.body();
//                    Log.d("updateApiCall---", new Gson().toJson(tasksResponse));

                    if (tasksResponse.getStatus().equals("success")) {
                        String message = tasksResponse.getMessage();
                        TasksListData tasksListData = tasksResponse.getTasksListData();
                        List<TasksList> tasksLists = tasksResponse.getTasksListData().getData();
                        
                        tinydb.putObject("taskLists", tasksListData); //saves the object
                        checksheetData = null;
                        fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.frameLayout, new DetailFragment());
                        fragmentTransaction.commit();

                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                        tinydb.remove("taskid");
                        tinydb.remove("images");
                        tinydb.remove("comment");
                        tinydb.remove("remarkId");
                        tinydb.remove("checklistId");
                        tinydb.remove("isDone");
                        tinydb.remove("validateId");
                        tinydb.remove("base64");
                    } else {
                        String message = tasksResponse.getMessage();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                progressDialog.dismiss();
                Log.d("updateTasks_error---", t.toString());
//                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        visible = true;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        getActivity().registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
//        visible = false;
        getActivity().unregisterReceiver(networkChangeReceiver);
    }

//    @Override
//    public void onStop() {
//        super.onStop();
//    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (tinydb.getString("taskid") != null && tinydb.getString("images") != null &&
                    tinydb.getString("comment") != null && tinydb.getString("remarkId") != null
                    && tinydb.getInt("checklistId") != 0 && tinydb.getString("authtoken") != null) {
//                netstatus = NetworkUtils.getConnectivityStatusString(context);
//                Log.e("netstatus--",netstatus);
                if (!NetworkUtils.getConnectivityStatusString(context).equals(context.getText(R.string.not_internet))) {
                    if (firstConnect) {
                        // do subroutines here
                        token = "Bearer " + SharedPreferencesClass.retriveToken(context, "token");
                        String taskid = tinydb.getString("taskid"); // retrieves the object from storage
                        String images = tinydb.getString("images"); // retrieves the object from storage
                        String comment = tinydb.getString("comment"); // retrieves the object from storage
                        String remarkId = tinydb.getString("remarkId"); // retrieves the object from storage
                        int checklistId = tinydb.getInt("checklistId"); // retrieves the object from storage
                        int isDone = tinydb.getInt("isDone"); // retrieves the object from storage
                        int validateId = tinydb.getInt("validateId"); // retrieves the object from storage
                        updateTasksApiCall(context, token, taskid, checklistId, isDone, images, comment, remarkId, validateId);
//                        progressDialog.dismiss();

                        firstConnect = false;
                    }
                } else {
                    firstConnect = true;
                }
            }
        }
    };
}
