package eu.spottless.userit.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.List;

import eu.spottless.userit.database.SharedPreferencesClass;
import eu.spottless.userit.remote.ApiService;
import eu.spottless.userit.remote.ServiceGenerator;

import eu.spottless.userit.responseModel.tasksResponse.TasksList;
import eu.spottless.userit.responseModel.tasksResponse.TasksResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BroadcastService extends Service {
    private static final String TAG = "BroadcastService";
    public static final String BROADCAST_ACTION = "eu.spottless.userit.service";
    private final Handler handler = new Handler();
    Intent intent;
    int counter = 0;
    List<TasksList> tasksLists;

    @Override
    public void onCreate() {
        super.onCreate();

        intent = new Intent(BROADCAST_ACTION);
    }

//    @Override
//    public void onStart(Intent intent, int startId) {
//        handler.removeCallbacks(sendUpdatesToUI);
//        handler.postDelayed(sendUpdatesToUI, 1000); // 1 second
//
//    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        handler.removeCallbacks(sendUpdatesToUI);
        handler.postDelayed(sendUpdatesToUI, 1000); // 1 second

        return startId;
    }

    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            DisplayLoggingInfo();
            handler.postDelayed(this, 10000); // 10 seconds
        }
    };

    private void DisplayLoggingInfo() {
        Log.d(TAG, "entered DisplayLoggingInfo");
        String token = "Bearer" + SharedPreferencesClass.retriveToken(this, "token");
        ApiService apiService = ServiceGenerator.createService(ApiService.class, token);
        Call<TasksResponse> call = apiService.tasksListUpdateUI(token);
        call.enqueue(new Callback<TasksResponse>() {
            @Override
            public void onResponse(Call<TasksResponse> call, Response<TasksResponse> response) {
                //hiding progress dialog
                if (response.isSuccessful()) {
                    TasksResponse tasksResponse = response.body();
                    if (tasksResponse != null && tasksResponse.getTasksListData().getData() != null) {
                        tasksLists = tasksResponse.getTasksListData().getData();
                        intent.putExtra("tasksLists", (Serializable) tasksLists);
                        sendBroadcast(intent);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Error! Please try again!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("notification error---", new Gson().toJson(t.getCause()));
            }
        });


    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(sendUpdatesToUI);
        super.onDestroy();
    }
}
