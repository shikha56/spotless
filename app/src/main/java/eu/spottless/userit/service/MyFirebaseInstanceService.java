package eu.spottless.userit.service;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import eu.spottless.userit.R;
import eu.spottless.userit.activities.LoginActivity;
import eu.spottless.userit.activities.MainActivity;

public class MyFirebaseInstanceService extends FirebaseMessagingService {
    String GROUP_KEY_WORK_EMAIL = "eu.spottless.userit";
    RemoteViews contentView;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if(!task.isSuccessful()){
                   Log.w("getInstanceId failed",task.getException());
                   return;
                }

                // Get new Instance ID token
                String token = task.getResult().getToken();
                Log.e("NEW_TOKENS--", token);
            }
        });
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d("Message data payload: " ,remoteMessage.getData().toString());
            Map<String, String> params = remoteMessage.getData();
            String body = params.get("body");
            String title = params.get("title");
                Log.e("JSON_OBJECT", title);

            sendNotification(title,body);
        }
        // Check if message contains a notification payload.
        else if (remoteMessage.getNotification() != null) {
            Log.d("Notification Body: " , remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());
        }
    }

    private void sendNotification(String title,String messageBody) {

        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("msg",messageBody);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID  = "idddd";

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);

            //Configure Notification Channel
            notificationChannel.setDescription("Game Notifications");
            notificationChannel.enableLights(true);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);

            notificationManager.createNotificationChannel(notificationChannel);
        }

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder = new NotificationCompat.Builder(MyFirebaseInstanceService.this,NOTIFICATION_CHANNEL_ID)

                            .setContentTitle(title)
                            .setAutoCancel(true)
//                            .setLargeIcon(((BitmapDrawable)getDrawable(R.drawable.logo)).getBitmap())
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setSound(defaultSoundUri)
                            .setContentText(messageBody)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
//                            .setContentIntent(pendingIntent)
                            .setColor(getResources().getColor(R.color.blue))
                            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                            .setWhen(System.currentTimeMillis())
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setDefaults(NotificationCompat.DEFAULT_ALL)
                            .setFullScreenIntent(pendingIntent, true);
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
