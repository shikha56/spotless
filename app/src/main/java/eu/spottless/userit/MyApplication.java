package eu.spottless.userit;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import eu.spottless.userit.remote.ServiceGenerator;
import eu.spottless.userit.utils.LocaleHelper;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class  MyApplication extends Application {

    private static MyApplication mInstance;
    private ServiceGenerator serviceGenerator;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
//        serviceGenerator = new ServiceGenerator(getApplicationContext());
//        serviceGenerator.clean();
        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("task.realm")
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(3)
                .build();
        Realm.setDefaultConfiguration(realmConfig);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
        Log.e("default language","en");
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

//    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
//        ConnectivityReceiver.connectivityReceiverListener = listener;
//    }
}
