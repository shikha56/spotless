package eu.spottless.userit.utils;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import eu.spottless.userit.R;
import eu.spottless.userit.TinyDB;

public class NetworkUtils {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    public static TinyDB tinydb;

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                NetworkCapabilities capabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        return TYPE_MOBILE;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        return TYPE_WIFI;
                    }
//                    else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)){
//                        return true;
//                    }
                }
            }
            else {
                try {
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    if (null != activeNetwork) {
                        if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                            return TYPE_WIFI;

                        if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                            return TYPE_MOBILE;
                    }
                    return TYPE_NOT_CONNECTED;
                } catch (Exception e) {
//                    Toast.makeText(context, "Network not available", Toast.LENGTH_SHORT).show();
                    Log.i("update_status", "" + e.getMessage());
                }
            }
        }
//        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
//        if (null != activeNetwork) {
//            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
//                return TYPE_WIFI;
//
//            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
//                return TYPE_MOBILE;
//        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        tinydb = new TinyDB(context);
        String languageCode = tinydb.getString("languageCode");
        Context context1 = LocaleHelper.setLocale(context, languageCode);
        Resources resources = context1.getResources();

        int conn = NetworkUtils.getConnectivityStatus(context);
        String status = null;
        if (conn == NetworkUtils.TYPE_WIFI) {
            status = resources.getString(R.string.wifi);;
        } else if (conn == NetworkUtils.TYPE_MOBILE) {
            status = resources.getString(R.string.mobile_data);;
        } else if (conn == NetworkUtils.TYPE_NOT_CONNECTED) {
            status = resources.getString(R.string.not_internet);
        }
        return status;
    }
}
