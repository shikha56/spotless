package eu.spottless.userit.utils;

public class UpdateCountEvent {

    public final int count;
    public static int UPCOMING_COUNT;
    public static int PENDING_COUNT;
    public static int OVERDUE_COUNT;
    public static int COMPLETED_COUNT;
    public static int DECLINED_COUNT;

    public UpdateCountEvent(int count) {
        this.count = count;
    }

}
