package eu.spottless.userit.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.widget.Toast;

import java.util.Objects;

import androidx.annotation.RequiresApi;
import eu.spottless.userit.MyApplication;

public class ConnectivityReceiver extends BroadcastReceiver {

//    public static ConnectivityReceiverListener connectivityReceiverListener;

    @Override
    public void onReceive(final Context context, final Intent intent) {

        String status = NetworkUtils.getConnectivityStatusString(context);
        if(status.equals("Not connected to Internet")){
            Toast.makeText(context, status, Toast.LENGTH_LONG).show();
        }
    }
//    public ConnectivityReceiver() {
//        super();
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//    @Override
//    public void onReceive(Context context, Intent arg1) {
//        //context and database helper object
//
//        ConnectivityManager cm = (ConnectivityManager) context
//                .getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetwork = Objects.requireNonNull(cm).getActiveNetworkInfo();
//        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
//
//        if (connectivityReceiverListener != null) {
//            connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
//        if (NetworkUtils.isNetworkAvailable(context)) {
//                Toast.makeText(context, "Network is available", Toast.LENGTH_SHORT).show();
//            }
//            else {
//                Toast.makeText(context, "Network not available", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
//
    @SuppressWarnings("deprecation")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean isConnected() {
        ConnectivityManager
                connectivityManager = (ConnectivityManager) MyApplication.getInstance().getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = Objects.requireNonNull(connectivityManager).getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();

//        if (connectivityManager == null) {
//            return false;
//        }
//
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//            Network network = connectivityManager.getActiveNetwork();
//            NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(network);
//            if (capabilities == null) {
//                return false;
//            }
//            return capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
//        } else {
//            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
//            if (networkInfo == null) {
//                return false;
//            }
//            return networkInfo.isConnected();
//        }
    }
//
//
//    public interface ConnectivityReceiverListener {
//        void onNetworkConnectionChanged(boolean isConnected);
//    }

}
